<?php

namespace GestionHogar\AdministracionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GestionHogar\AdministracionBundle\Entity\Categoria;

class LoadCategoriaDataTest implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->cargarCategorias($manager);
    }
    
    public function cargarCategorias(ObjectManager $manager){
        
        
        $rubroNombre = 'Alimentos';
        $rubroRepository = $manager->getRepository('AdministracionBundle:Rubro');
        $rubro = $rubroRepository->findOneBy(array('nombre'=> $rubroNombre));
        $categorias [0]['nombre']= 'Verduras';
        $categorias [0]['rubro']= $rubro;
        $categorias [0]['descripcion']= 'Informacion sobre verduras';        
        $categorias [0]['activo']= true;
        
        $categorias [1]['nombre']= 'Lácteos ';
        $categorias [1]['rubro']= $rubro;
        $categorias [1]['descricpion']= 'Informacion sobre Lácteos ';        
        $categorias [1]['activo']= true;
        
        $categorias [2]['nombre']= 'Pastas ';
        $categorias [2]['rubro']= $rubro;
        $categorias [2]['descripcion']= 'Informacion sobre Pastas ';        
        $categorias [2]['activo']= true;
        
        $categorias [3]['nombre']= 'Carnes ';
        $categorias [3]['rubro']= $rubro;
        $categorias [3]['descripcion']= 'Informacion sobre Carnes ';        
        $categorias [3]['activo']= true;
                        
        
        foreach ($categorias as $categoria) {
            $categoriaObjeto = new Categoria();
            $categoriaObjeto->setRubro($categoria['rubro']);
            $categoriaObjeto->setNombre($categoria['nombre']);
            $categoriaObjeto->setDescripcion($categoria['descripcion']);
            $categoriaObjeto->setActivo($categoria['activo']);
            $manager->persist($categoriaObjeto);
            $manager->flush();
        }
    }
            
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 4; 
    }
}