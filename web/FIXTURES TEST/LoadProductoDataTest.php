<?php

namespace GestionHogar\AdministracionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GestionHogar\AdministracionBundle\Entity\Producto;

class LoadProductoDataTest implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarProductos($manager);
    }
    
    public function cargarProductos(ObjectManager $manager){
        $categoriaNombre = "Lácteos";
        $categoriaRepository = $manager->getRepository('AdministracionBundle:Categoria');
        $categoria = $categoriaRepository->findOneBy(array('nombre'=> $categoriaNombre));
        
        $i= 0;
        $productos [$i]['nombre']= 'Queso untable';
        $productos [$i]['descripcion']= 'Queso untable';
        $productos [$i]['unidad']= '1kg';
        $productos [$i]['categoria']= $categoria;
        $i++;
        
        $productos [$i]['nombre']= 'Leche';
        $productos [$i]['descripcion']= 'Queso untable';
        $productos [$i]['unidad']= '1 Lt.';
        $productos [$i]['categoria']= $categoria;
        $i++;
        
        $productos [$i]['nombre']= 'Yogurt';
        $productos [$i]['descripcion']= 'Yogurt';
        $productos [$i]['unidad']= '1 Lt.';
        $productos [$i]['categoria']= $categoria;
        $i++;
        
        
        foreach ($productos as $producto) {
            $productoObjeto = new Producto();
            $productoObjeto->setNombre($producto['nombre']);
            $productoObjeto->setDescripcion($producto['descripcion']);
            $productoObjeto->setUnidadMedida($producto['unidad']);
            $productoObjeto->setCategoria($producto['categoria']);
            $manager->persist($productoObjeto);
            $manager->flush();
        }
    }
            
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 5; 
    }
}