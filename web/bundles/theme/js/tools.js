function resetFormulario(form) {

    form.find('input, textarea, input:not([type="submit"])').removeAttr('value');
    form.find('input, textarea, input:not([type="submit"])').val('');
    form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

    form.find('select option').removeAttr('selected').find('option:first').attr('selected', 'selected');

}

function prevent(e) {
    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
}

function bloquearPantalla(gif) {
    $.blockUI({
        message: '<h1> <img src="' + gif + '"/>Cargando Agenda...</h1>',
        css: {border: 'none', background: ''}
    });

}

function desbloquearPantalla() {
    setTimeout($.unblockUI, 2000); 
}