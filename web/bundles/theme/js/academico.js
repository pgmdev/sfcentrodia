function allowArrastrar() {

    $('#external-events .fc-event').each(function () {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            id: $(this).find('input').val(),
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });


        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });

}

function createDateFormDay(evento) {
    
    dateFull = moment().day(evento.dia);
    //dia = moment(dateFull).format("YYYY-MM-DD");
    //dia = moment(evento.fecha).format("YYYY-MM-DD");
    diaconvertido = evento.fecha + "T" + evento.inicio;
    diaahora = String(diaconvertido);
    //console.log(diaahora);
    return diaahora;
    
}

function createHourEnd(evento) {
    dateFull = moment().day(evento.dia);
    dia = moment(dateFull).format("YYYY-MM-DD");
    diaconvertido = dia + "T" + evento.inicio;
    diaahora = String(diaconvertido);
    returned_endate = moment(diaahora).add(1, 'hours');
    hourEnd = moment(returned_endate).format("HH:mm:ss");
    diaEnd = evento.fecha + "T" + hourEnd;
    dayEnd = String(diaEnd);
    return dayEnd;
}

function showMessageCreate() {
    $('#ventana2').modal({
        show: true
    });
}

function cargarCalendario(elemento, path) {
    //path: {{ path('academico_ajax_horario_semanal')}}

    elemento.fullCalendar({
        events: function (start, end, timezone, callback) {
            $.ajax({
                url: path,
                dataType: 'json',
                method: 'get',
                data: {
                    start: start.format(),
                    end: end.format()
                },
                success: function (response) {
                    var events = [];
                    if (response.status == "ok") {
                        if (!!response) {
                            $(response.results).each(function (index, evento) {
                                fechaInicio = createDateFormDay(evento);
                                fechaFinal = createHourEnd(evento);
                                events.push({
                                    id: evento.id,
                                    title: evento.nombre,
                                    className: 'success',
                                    textColor: 'black',
                                    editable: false,
                                    //url: "{{ path('horario_ver', { id: 1 })}}",
                                    start: fechaInicio,
                                    end: fechaFinal,
                                });
                            });
                        }

                        callback(events);

                    } else {

                        $('.alertas').show();
                    }


                }

            });
        },
    });
}

function armarCalendario(calendario) {
    $(calendario).fullCalendar({
        
    });


}