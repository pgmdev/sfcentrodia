<?php

namespace CentroDia\PersonaBundle\Services;

use CentroDia\PersonaBundle\Entity\Responsable;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PersonaManager
 *
 * @author gabriel
 */
class PersonaManager {
    
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /*
     * Devuelve un listado de  activos 
     */

    public function listarAlumnos($filtros) {
        
        $alumnoRepository = $this->container->get('doctrine')->getRepository('PersonaBundle:Alumno');
        $aAlumnos = $alumnoRepository->getListarAlumnos($filtros);
        return $aAlumnos;
    }
    
    public function listarPadres($filtros) {
        $padreRepository = $this->container->get('doctrine')->getRepository('PersonaBundle:Padre');
        $pPadres = $padreRepository->getListarPadres($filtros);
        return $pPadres;
    }
    
    public function listarPersonales() {
        $personalRepository = $this->container->get('doctrine')->getRepository('PersonaBundle:Personal');
        $pPersonales = $personalRepository->getListarPersonales();
        //$pPersonales = $personalRepository->findAll();
        return $pPersonales;
    }
    
    
    public function listarUsuarios($filtros) {
        $usuarioRepository = $this->container->get('doctrine')->getRepository('UsuarioBundle:Usuario');
        $aUsuarios = $usuarioRepository->getListarUsuarios($filtros);
        return $aUsuarios;
    }
    
    public function getAlumnoByTerm($term) {
        $alumnoRepository = $this->container->get('doctrine')->getRepository('PersonaBundle:Alumno');
        $aAlumno = $alumnoRepository->getAlumnoByTerm($term);
        return $aAlumno;
    }
    
    
    public function crearResponsabilidad($personal){
        if($personal->getResponsable() == null){
            $responsable  = new Responsable();
            $responsable->setPersonal($personal);
        }
        return $responsable;
    }
    
    public function filtrarAlumnos($filtros) {
        $alumnoRepository = $this->container->get('doctrine')->getRepository('PersonaBundle:Alumno');
        $aEquipos = $alumnoRepository->configurarFiltrosAlumnos($filtros);
        return $aEquipos;
    }
}
