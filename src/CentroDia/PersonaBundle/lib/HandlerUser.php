<?php

namespace CentroDia\PersonaBundle\lib;

use CentroDia\UtilBundle\lib\StringTools;
use Symfony\Component\DependencyInjection\Container;

 
 
 /**
  * CLASE QUE MANEJA EL CONTROL SOBRE USUARIOS ASIGNACION DEL MISMO A UNA PERSONA  
  * 
  * @author Juan Gabriel Cabral <jgc1984@gmail.com>
  * 
  */

class HandlerUser {
    
    private $container;
    const ROLE_PROFESOR = "ROLE_PROFESOR",
          EMAIL="@centrodia.com";
    
    
    public function __construct(Container $container) {
        $this->container = $container;
        
    }
            
    
    /**
     * 
     * Devuelve un nuevo usuario asociado a la persona con su persona institucional y configuracion      
     * 
     * @param type $persona
     * @return type
     */
    public function createNewUser($personal){    
            $userManager = $this->container->get('fos_user.user_manager');            
            $usuario = $userManager->createUser();
            $username = StringTools::usernamealize($personal->getNombre().' '.$personal->getApellido());
            $usuario->setUsername($username);
            $usuario->setEmail($username.self::EMAIL);
            $usuario->addRole(self::ROLE_PROFESOR);  
            $usuario->setEnabled(true);     
            
            return $usuario;
    
    }
    
     public function crearNuevoUsuario($personal){
            $userManager = $this->container->get('fos_user.user_manager');
            
            $usuario = $userManager->createUser();
            $username = StringTools::usernamealize($personal->getNombre().' '.$personal->getApellido());
            $usuario->setUsername($username);
            $usuario->setEmail($username.self::EMAIL);
            $usuario->addRole(self::ROLE_PROFESOR);  
            $usuario->setEnabled(true);
         
            return $usuario;
    }
    
    
        
    
    /**
     * 
     * Comprueba el usuario
     * 
     * Metodo que devuelve un usuario asociado a la persona si esta no lo tiene lo crea
     * 
     * @param type $persona
     * @param type $session
     * @param type $em
     * @return type
     * @throws type
     */
    public function checkUser($personal){        
        
        if (NULL === $personal->getUsuario()) {//Si la persona no tiene un usuario asignado se le crea            
            $usuario = $this->createNewUser($personal);                        

        }            
        return $usuario;
    }                                                                                                                                         
        
}
