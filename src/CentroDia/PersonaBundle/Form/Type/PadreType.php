<?php

namespace CentroDia\PersonaBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use CentroDia\PersonaBundle\Entity\Padre;

class PadreType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        
        $builder            
                                                                                      
            ->add('apellido', 'text',array('label'=> 'Apellido:', 'attr'=>array('class'=> 'form-control'),))                     
            ->add('dni', 'text',array('label'=> 'DNI:', 'attr'=>array('class'=> 'form-control'),))                 
                
            ->add('nombre', 'text', array('label'=> 'Nombre:', 'attr'=> array('class'=> 'form-control')))                                                                                                   
            ->add('direccion', 'text', array('label'=> 'Direccion:', 'attr'=> array('class'=> 'form-control')))              
            ->add('cuil', 'text', array('required'=> false,'label'=> 'CUIL:', 'attr'=> array('class'=> 'form-control')))
            ->add('email', 'text', array('required'=> false,'label'=> 'Email:', 'attr'=> array('class'=> 'form-control')))
            ->add('telefono', 'text', array('required'=> false,'label'=> 'Telefono:', 'attr'=> array('class'=> 'form-control')))
            ->add('celular', 'text', array('required'=> false,'label'=> 'Celular:', 'attr'=> array('class'=> 'form-control')))
            ->add('vive', 'checkbox', array('label'=> 'Vive:','required'=> false, 'attr'=> array('class'=> 'form-control')))        
            ->add('sexo', new GeneroType(), array('label'=> 'Sexo','attr'=>array( 'placeholder' => 'Elejir el genero'),
             ))            
            ->add('fechaNacimiento', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd','label'=> 'Fecha nacimiento:', 'attr'=> array('class'=> 'form-control')))
            ->add('ocupacion', 'text', array('required'=> false,'label'=> 'Ocupación:', 'attr'=> array('class'=> 'form-control')))
            ->add('tutor', 'checkbox', array('required'=> false,'label'=> 'Tutor:', 'attr'=> array('class'=> 'form-control')))                
                                                               
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\PersonaBundle\Entity\Padre'
        ));
    }
    
   
    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_personabundle_padre';
    }
}