<?php

namespace CentroDia\PersonaBundle\Form\Type;

use CentroDia\AcademicoBundle\Form\TallerType;
use CentroDia\PersonaBundle\Entity\Personal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PersonalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accept = 'image/*';
        
        $assert = new Assert\File(array(
            'maxSize' => '5120k',
            'mimeTypes' => array(
                'image/png',
                'image/jpeg',
                'image/gif'
            ),
            'mimeTypesMessage' => 'Por favor seleccione una imágen con formato válido',
        ));
        
        
        $builder            
            ->add('nombre', 'text', array('label'=> 'Nombre:', 'attr'=> array('class'=> 'form-control')))                                                                                       
            ->add('apellido', 'text', array('label'=> 'Apellido:', 'attr'=> array('class'=> 'form-control')))   
            ->add('direccion', 'text', array('label'=> 'Direccion:', 'attr'=> array('class'=> 'form-control')))  
            ->add('dni', 'text', array('label'=> 'DNI:', 'attr'=> array('class'=> 'form-control')))  
            ->add('cuil', 'text', array('label'=> 'CUIL:', 'attr'=> array('class'=> 'form-control')))
            ->add('file', 'file', array(
            'required' => false,
            'label' => 'Foto',            
            'mapped'=>false,
            'attr' => array('accept' => $accept),
            'constraints' => array($assert),
            'auto_initialize' => false,
            'attr'=>array('class'=> 'input-file uniform_on')          
             ))                
            ->add('email', 'text', array('label'=> 'Email:', 'attr'=> array('class'=> 'form-control')))
            ->add('telefono', 'text', array('label'=> 'Telefono:', 'attr'=> array('class'=> 'form-control')))                
            ->add('celular', 'text', array('label'=> 'Celular:', 'attr'=> array('class'=> 'form-control')))
            ->add('vive', 'checkbox', array('required'=> false,'label'=> 'Vive:', 'attr'=> array('class'=> 'form-control')))    
            ->add('activo', 'checkbox', array('required'=> false,'label'=> 'Activo:', 'attr'=> array('class'=> 'form-control')))    
            ->add('externo', 'checkbox', array('required'=> false,'label'=> 'Externo:', 'attr'=> array('class'=> 'form-control')))    
            ->add('cargo', 'text', array('required'=> true,'label'=> 'Cargo:', 'attr'=> array('class'=> 'form-control')))    
            ->add('sexo', new GeneroType(), array('label'=> 'Sexo:','attr'=>array( 'placeholder' => 'Elejir el genero'),
             ))
            ->add('fechaNacimiento', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd','label'=> 'Fecha nacimiento:', 'attr'=> array('class'=> 'form-control')))      
            ->add('fechaIngreso', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd','label'=> 'Fecha Ingreso:', 'attr'=> array('class'=> 'form-control')))      
            ->add('fechaEgreso', 'date', array('required'=>false, 'widget' => 'single_text','format' => 'yyyy-MM-dd','label'=> 'Fecha Egreso:', 'attr'=> array('class'=> 'form-control')))      
            
            ->add('talleres', 'entity', array(    
                //'empty_value'=> 'Elejir un taller',
                'multiple' => true,   // Multiple selection allowed            
                //'property' => 'nombre', // Assuming that the entity has a "name" property
                'class'    => 'AcademicoBundle:Taller',
                
            ))
            ->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=> 'form-control')));
                                                               
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\PersonaBundle\Entity\Personal',
            'cascade_validation'=>true  
        ));
    }
    
    
    public static function processImage(UploadedFile $uploaded_file, Personal $personal)
    {
        $path = 'foto/';
     
        $uploaded_file_info = pathinfo($uploaded_file->getClientOriginalName());
        $file_name =            
            $personal->getId() ."_".md5(rand()).
            "." .
            $uploaded_file_info['extension']
            ;

        $uploaded_file->move($path, $file_name);

        return $file_name;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'personal';
    }
}