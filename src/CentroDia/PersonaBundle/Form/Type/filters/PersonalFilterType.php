<?php

namespace CentroDia\PersonaBundle\Form\Type\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('persona', 'text', array('label'=>'Nombre / Apellido:', 'required'=> false))
            ->add('dni', 'text', array('label'=>'Dni:', 'required'=> false))                                    
            ->add('sexo', 'choice', array(
                    'choices'   => array('m' => 'Masculino', 'f' => 'Femenino'),
                    'preferred_choices' => array('m'),
                    'required'  => false,
                    )
                 )
            ->add('activo', 'choice', array(
                    'choices'   => array(1 => 'Activo', 0 => 'Pasivo'),
                    'preferred_choices' => array(1),
                    'required'  => false,
                    )
                 )     
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array(     
            'cascade_validation' => true,
            'csrf_protection'   => false                      
        ));
    }

    public function getName()
    {
        return 'personal_filter';
    }
}
