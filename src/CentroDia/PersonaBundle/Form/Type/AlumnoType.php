<?php

namespace CentroDia\PersonaBundle\Form\Type;

use CentroDia\PersonaBundle\Form\Type\PadreType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use CentroDia\PersonaBundle\Entity\Alumno;


class AlumnoType extends AbstractType {

    private $arrayParams;
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $accept = 'image/*';
        
        $assert = new Assert\File(array(
            'maxSize' => '5120k',
            'mimeTypes' => array(
                'image/png',
                'image/jpeg',
                'image/gif'
            ),
            'mimeTypesMessage' => 'Por favor seleccione una imágen con formato válido',
        ));
        
        $builder
                ->add('nombre', 'text', array('label' => 'Nombre:', 'attr' => array('class' => 'form-control')))
                ->add('apellido', 'text', array('label' => 'Apellido:', 'attr' => array('class' => 'form-control')))
                ->add('direccion', 'text', array('label' => 'Direccion:', 'attr' => array('class' => 'form-control')))
                ->add('dni', 'text', array('label' => 'DNI:', 'attr' => array('class' => 'form-control')))
                ->add('cuil', 'text', array('label' => 'CUIL:', 'attr' => array('class' => 'form-control')))
                ->add('email', 'email', array('label' => 'Email:','required'=>false, 'attr' => array('class' => 'form-control', 'placeholder'=> 'Email')))
                ->add('telefono', 'text', array('label' => 'Telefono:', 'attr' => array('class' => 'form-control')))
                ->add('celular', 'text', array('label' => 'Celular:', 'attr' => array('class' => 'form-control')))
                ->add('vive', 'checkbox', array('required' => false, 'label' => 'Vive:', 'attr' => array('class' => 'form-control')))
                ->add('asiste', 'checkbox', array('required' => false, 'label' => 'Asiste:', 'attr' => array('class' => 'form-control')))
                ->add('sexo', new GeneroType(), array('label'=> 'Sexo', 'attr' => array('placeholder' => 'Elejir el genero'),
                ))
                ->add('file', 'file', array(
            'required' => false,
            'label' => 'Foto',            
            'mapped'=>false,
            'attr' => array('accept' => $accept),
            'constraints' => array($assert),
            'auto_initialize' => false,
            'attr'=>array('class'=> 'input-file uniform_on')          
        ))
                
                /*->add('padres', 'collection', array(
                    'type' => new PadreType(),
                    'allow_add' => true,
                    'prototype' => true,                    
                    'allow_delete' => true,
                ))*/
                ->add('padres','entity', array(    
                'empty_value'=> 'Elejir un padre',
                'multiple' => true,   // Multiple selection allowed            
                //'property' => 'nombre', // Assuming that the entity has a "name" property
                'class'    => 'PersonaBundle:Padre',
                'attr'=> array('class'=> 'padre')    
                ))
                ->add('fechaNacimiento', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha nacimiento:', 'attr' => array('class' => 'form-control')))
                ->add('fechaIngreso', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha Ingreso:', 'attr' => array('class' => 'form-control')))
                ->add('fechaEgreso', 'date', array('required' => false, 'required' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha Egreso:', 'attr' => array('class' => 'form-control')))
                ;

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\PersonaBundle\Entity\Alumno'
        ));
    }
    
    public static function processImage(UploadedFile $uploaded_file, Alumno $alumno)
    {
        $path = 'foto/';
     
        $uploaded_file_info = pathinfo($uploaded_file->getClientOriginalName());
        $file_name =            
            $alumno->getId() ."_".md5(rand()).
            "." .
            $uploaded_file_info['extension']
            ;

        $uploaded_file->move($path, $file_name);

        return $file_name;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'centrodia_personabundle_alumno';
    }

}
