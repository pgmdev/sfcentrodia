<?php

namespace CentroDia\PersonaBundle\Form;

use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResponsableType extends AbstractType
{
    
    private $arrayParams;
    const   entityPersonal = "PersonaBundle:Personal"; 
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $this->arrayParams['container']->get('doctrine')->getManager();
        
        $personalTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['personal'], self::entityPersonal);
        
        $personal = $this->arrayParams['personal'];
        
        /*$builder->add($builder
                    ->create('personal', 'hidden')
                    ->addModelTransformer($personalTransformer));*/    
        //$builder->add('personal');
        
        $builder->add('personal', 'entity', array('class'=> 'PersonaBundle:Personal', 
                                                'label'=> 'Alumno',
                                                'query_builder' => function (EntityRepository $repository) use ($personal){
                                                    $qb = $repository->getPersonalById($personal);                                                                                                                                                                                                      
                                                    return $qb;
                                               }
                    ));
        $builder->add('fechaInicio', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha Control:'))
            ->add('fechaFin', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha Vencimiento:'))
            ->add('descripcion', 'textarea', array('label'=> 'Descripcion'))                                                                                                                                       
            ->add('cargo', 'text', array('label'=> 'Cargo'))                                                                                                                                                   
            ->add('activo', 'checkbox', array('label'=> 'Activo'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\PersonaBundle\Entity\Responsable',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'centrodia_personabundle_responsable';
    }
}
