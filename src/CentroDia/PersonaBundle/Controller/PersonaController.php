<?php

namespace CentroDia\PersonaBundle\Controller;

use CentroDia\PersonaBundle\Entity\Alumno;
use CentroDia\PersonaBundle\Entity\Padre;
use CentroDia\PersonaBundle\Entity\Personal;
use CentroDia\PersonaBundle\Entity\Responsable;
use CentroDia\PersonaBundle\Form\ResponsableType;
use CentroDia\PersonaBundle\Form\Type\AlumnoType;
use CentroDia\PersonaBundle\Form\Type\filters\AlumnoFilterType;
use CentroDia\PersonaBundle\Form\Type\filters\PadreFilterType;
use CentroDia\PersonaBundle\Form\Type\filters\PersonalFilterType;
use CentroDia\PersonaBundle\Form\Type\PadreType;
use CentroDia\PersonaBundle\Form\Type\PersonalType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PersonaController extends Controller {

    /**
     * Lists all Banco entities.
     *
     */
    public function indexAlumnoAction(Request $request) {

        $personaManager = $this->get('persona.manager');

        $alumnoFiltro = new AlumnoFilterType();

        $aAlumnos = $personaManager->listarAlumnos(array());

        $formFiltro = $this->createForm($alumnoFiltro);

        $formFiltro->handleRequest($request);

        if ($formFiltro->isValid()) {

            $filtros = $formFiltro->getData();
            $aAlumnos = $personaManager->listarAlumnos($filtros);
        }


        return $this->render('PersonaBundle:Alumno:index.html.twig', array(
                    'entities' => $aAlumnos,
                    'formFilter' => $formFiltro->createView()
        ));
    }

    public function newAlumnoAction() {

        $alumno = new Alumno();

        $form = $this->createForm(new AlumnoType(), $alumno);

        return $this->render('PersonaBundle:Alumno:alumno.html.twig', array(
                    'alumno' => $alumno,
                    'form' => $form->createView(),
        ));
    }

    public function createAlumnoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $alumno = new Alumno();

        $form = $this->createForm(new AlumnoType(), $alumno);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $uploaded_file = $form['file']->getData();

            if ($uploaded_file) {
                //Sube la imagen
                $picture = AlumnoType::processImage($uploaded_file, $alumno);

                //setea el tamaño del recorte
                $targ_w = $request->request->get('w');
                $targ_h = $request->request->get('h');

                if ($targ_h && $targ_w) {
                    if (null !== $alumno->getFoto()) {
                        $file = $alumno->getFoto();
                        @unlink($file);
                    } else {
                        $file = $picture;
                    }
                    $file = $this->get('kernel')->getRootDir() . '/../web/foto/' . $picture;

                    $img_r = imagecreatefromjpeg($file);
                    $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                    imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                    imagejpeg($dst_r, $file, 90);
                }


                $alumno->setFoto($picture);
            }

            $em->persist($alumno);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el alumno .' . $alumno->getNombre() . " - " . $alumno->getApellido()
            );

            return $this->redirect($this->generateUrl('alumnos'));
        }

        return $this->render('PersonaBundle:Alumno:alumno.html.twig', array(
                    'alumno' => $alumno,
                    'form' => $form->createView(),
        ));
    }

    public function editAlumnoAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $alumno = $em->getRepository('PersonaBundle:Alumno')->find($id);

        if (!$alumno) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }

        $form = $this->createForm(new AlumnoType(), $alumno);


        return $this->render('PersonaBundle:Alumno:alumno.html.twig', array(
                    'edicion' => true,
                    'alumno' => $alumno,
                    'form' => $form->createView()
        ));
    }

    public function updateAlumnoAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $alumno = $em->getRepository('PersonaBundle:Alumno')->find($id);

        $form = $this->createForm(new AlumnoType(), $alumno);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $uploaded_file = $form['file']->getData();

            if ($uploaded_file) {
                //Sube la imagen
                $picture = AlumnoType::processImage($uploaded_file, $alumno);

                //setea el tamaño del recorte
                $targ_w = $request->request->get('w');
                $targ_h = $request->request->get('h');

                if ($targ_h && $targ_w) {
                    if (null !== $alumno->getFoto()) {
                        $file = $alumno->getFoto();
                        @unlink($file);
                    } else {
                        $file = $picture;
                    }
                    $file = $this->get('kernel')->getRootDir() . '/../web/foto/' . $picture;

                    $img_r = imagecreatefromjpeg($file);
                    $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                    imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                    imagejpeg($dst_r, $file, 90);
                }


                $alumno->setFoto($picture);
            }

            $em->persist($alumno);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el alumno .' . $alumno->getNombre() . " - " . $alumno->getApellido()
            );

            return $this->redirect($this->generateUrl('alumnos'));
        }

        return $this->render('PersonaBundle:Alumno:alumno.html.twig', array(
                    'alumno' => $alumno,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Lists all Banco entities.
     *
     */
    public function indexPadreAction(Request $request) {
        $personaManager = $this->get('persona.manager');
        
        $padreFiltro = new PadreFilterType();

        $aPadres = $personaManager->listarPadres(array());

        $formFiltro = $this->createForm($padreFiltro);

        $formFiltro->handleRequest($request);

        if ($formFiltro->isValid()) {

            $filtros = $formFiltro->getData();
            $aPadres = $personaManager->listarPadres($filtros);
        }
               

        return $this->render('PersonaBundle:Padre:index.html.twig', array(
                            'entities' => $aPadres,
                            'formFilter'=>$formFiltro->createView()
                        )
        );
    }

    public function newPadreAction() {

        $padre = new Padre();

        $form = $this->createForm(new PadreType(), $padre);

        return $this->render('PersonaBundle:Padre:padre.html.twig', array(
                    'padre' => $padre,
                    'form' => $form->createView(),
        ));
    }

    public function createPadreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $padre = new Padre();

        $form = $this->createForm(new PadreType(), $padre);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($padre);
            $em->flush();

            return $this->redirect($this->generateUrl('padres'));
        }

        return $this->render('PersonaBundle:Padre:padre.html.twig', array(
                    'padre' => $padre,
                    'form' => $form->createView(),
        ));
    }
    
    
    public function editPadreAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $padre = $em->getRepository('PersonaBundle:Padre')->find($id);

        if (!$padre) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }

        $form = $this->createForm(new PadreType(), $padre);


        return $this->render('PersonaBundle:Padre:padre.html.twig', array(
                    'edicion' => true,
                    'padre' => $padre,
                    'form' => $form->createView()
        ));
    }

    public function updatePadreAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $padre = $em->getRepository('PersonaBundle:Padre')->find($id);

        $form = $this->createForm(new PadreType(), $padre);

        $form->handleRequest($request);

        if ($form->isValid()) {
                    
            $em->persist($padre);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el padre .' . $padre->getNombre() . " - " . $padre->getApellido()
            );

            return $this->redirect($this->generateUrl('padres'));
        }

        return $this->render('PersonaBundle:Padre:padre.html.twig', array(
                    'padre' => $padre,
                    'form' => $form->createView(),
        ));
    }
    
    
    

    public function indexPersonalAction(Request $request) {

        $personaManager = $this->get('persona.manager');

        $personalFiltro = new PersonalFilterType();

        $aPersonales = $personaManager->listarPersonales(array());

        $formFiltro = $this->createForm($personalFiltro);

        $formFiltro->handleRequest($request);

        if ($formFiltro->isValid()) {

            $filtros = $formFiltro->getData();

            $personaManager->listarPersonales($aPersonales, $filtros);
        }

        return $this->render('PersonaBundle:Personal:index.html.twig', array(
                    'entities' => $aPersonales,
                    'formFilter' => $formFiltro->createView()
        ));
        
    }

    public function newPersonalAction() {

        $personal = new Personal();

        $form = $this->createForm(new PersonalType(), $personal);

        return $this->render('PersonaBundle:Personal:personal.html.twig', array(
                    'personal' => $personal,
                    'form' => $form->createView(),
        ));
    }

    public function createPersonalAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $personal = new Personal();

        $form = $this->createForm(new PersonalType(), $personal);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($personal);
            $em->flush();

            return $this->redirect($this->generateUrl('personal'));
        }

        return $this->render('PersonaBundle:Personal:personal.html.twig', array(
                    'personal' => $personal,
                    'form' => $form->createView(),
        ));
    }

    public function editPersonalAction($id) {
        $em = $this->getDoctrine()->getManager();

        $personal = $em->getRepository('PersonaBundle:Personal')->find($id);

        $form = $this->createForm(new PersonalType(), $personal);


        return $this->render('PersonaBundle:Personal:personal.html.twig', array(
                    'personal' => $personal,
                    'edicion' => true,
                    'form' => $form->createView(),
        ));
    }

    public function updatePersonalAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $personal = $em->getRepository('PersonaBundle:Personal')->find($id);

        $form = $this->createForm(new PersonalType(), $personal);

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($personal);
            $em->flush();


            return $this->redirect($this->generateUrl('personal'));
        }


        return $this->render('PersonaBundle:Personal:personal.html.twig', array(
                    'personal' => $personal,
                    'form' => $form->createView(),
        ));
    }

    public function indexResponsableAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PersonaBundle:Responsable')->findAll();

        $paginador = $this->get('knp_paginator');

        $entities = $paginador->paginate(
                $entities, $this->get('request')->query->get('page', 1), 10
        );



        return $this->render('PersonaBundle:Responsable:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function createResponsableAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $personaManager = $this->get('persona.manager');
        
        $arrayParams = array('personal' => $idPersonal, 'container' => $this->container);

        $personal = $em->getRepository('PersonaBundle:Personal')->find($id);

        $responsable = $em->getRepository('PersonaBundle:Responsable')->findOneByPersonal($id);

        if (!$responsable) {
            $responsable = $personaManager->crearResponsabilidad($personal);
        }

        $form = $this->createForm(new ResponsableType($arrayParams), $responsable);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            echo $responsable->getPersonal();
            $em->persist($responsable);
            $em->flush();


            return $this->redirect($this->generateUrl('responsable', array('id' => $responsable->getId())));
        }


        return $this->render('PersonaBundle:Responsable:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Responsable entity.
     *

     */
    public function newResponsableAction($idPersonal) {
        $entity = new Responsable();

        $arrayParams = array('personal' => $idPersonal, 'container' => $this->container);

        $form = $this->createForm(new ResponsableType($arrayParams), $entity);


        return $this->render('PersonaBundle:Responsable:new.html.twig', array(
                    'entity' => $entity,
                    'personal' => $idPersonal,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Responsable entity.
     *
     */
    public function editResponsableAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PersonaBundle:Responsable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Responsable entity.');
        }

        $editForm = $this->createForm(new ResponsableType(), $entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render('PersonaBundle:Responsable:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Responsable entity.
     *

     */
    public function updateResponsableAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PersonaBundle:Responsable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Responsable entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ResponsableType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('responsable_edit', array('id' => $id)));
        }


        return $this->render('PersonaBundle:Responsable:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function dashboardAction() {
        return $this->render('PersonaBundle:Persona:indexDashboardSuperAdmin.html.twig');
    }

    public function getAlumnosAjaxAction(Request $request) {

        $personaManager = $this->get('persona.manager');

        $alumnos = $personaManager->getAlumnoByTerm($request->get('mt_filter'));        
        
        if($request->get('mt_selected')) {
            $selectedItemsIds = $request->get('mt_selected');
        }else{
            $selectedItemsIds = array();
        }
                              
        if (count($alumnos) > 0) {
            foreach($alumnos as $alumno){
                $jsonAlumnos[] = array(
                    'id'=> ''.$alumno["id"].'',
                    'name'=> $alumno["nombre"],
                    'description'=> $alumno["dni"],
                    'picture_path'=> '../../web/avatar.png'
                ); 
            }

            foreach ($alumnos as $key => $r) {                
                if (in_array($r['id'], $selectedItemsIds) || (stripos($r['dni'], $request->get('mt_filter')) === false && stripos($r['nombre'], $request->get('mt_filter')) === false))
                    unset($alumnos[$key]);
            }
        }else{
            $alumnos[] = 
                    array(
                        'status' => 'empty',
                        'message' => 'No Results available'
                    );
            
        }
        
        $jsonResponse = array(
            'status' => 'ok',
            'results'=> $jsonAlumnos
            
        );
        $response = new Response(json_encode($jsonResponse));

        return $response;
    }
    
    
    
    public function getAlumnoByDataAjaxAction(Request $request) {
        $value = strtoupper($request->get('term'));
        
        $class = $request->get('class');
        $property = $request->get('property');
        $searchMethod = $request->get('search_method');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository($class)->$searchMethod($value);

        $json = array();

        if (!count($entities)) {
            $json[] = array(
                'label' => 'No se encontraron coincidencias',
                'value' => ''
            );
        } else {

            foreach ($entities as $entity) {
                $json[] = array(
                    'id' => $entity['id'],                    
                    'value' => $entity[$property]                    
                );
            }
        }

        $response = new Response();
        $response->setContent(json_encode($json));

        return $response;
    }
    
    
    public function getAsistenciasByAlumnoAjaxAction(Request $request) {
                
        $class = $request->get('class');
                                        
        $em = $this->getDoctrine()->getManager();

        $asistencias = $em->getRepository($class)->getAsistenciasByAlumno($request->get('parentID'));
                
        return $this->render('AdministracionBundle:Facturacion:asistencias.html.twig', array(
                    'asistencias' => $asistencias,
                    
        ));
    }
    
    public function printPersonalAction(Request $request) {

        $personaManager = $this->get('persona.manager');
       

        $aPersonales = $personaManager->listarPersonales(array());

        $html = $this->renderView('PersonaBundle:Personal:listado.html.twig', array(
            'entities' => $aPersonales
        ));


        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
                            'Content-Type' => 'application/pdf',
                            'Content-Disposition' => 'attachment; filename="ficha.pdf"'
                )
        );
    }

}
