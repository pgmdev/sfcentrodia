<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\PersonaBundle\Entity\Alumno;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAlumnoData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarAlumnos($manager);
    }
    
    public function cargarAlumnos(ObjectManager $manager){                                
        $i= 0;
        
        /* ID 0 */
        $alumnos[$i]['apellido'] = 'Tapia';
        $alumnos[$i]['nombre'] = 'Paula Analía';
        $alumnos[$i]['dni'] = '32914818';
        $alumnos[$i]['fechaNacimiento'] = '28/01/1987';
        $alumnos[$i]['cuil'] = '27329148187';
        $alumnos[$i]['sexo'] = 'f';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '30/09/2012';
        $alumnos[$i]['direccion'] = 'Necochea 1368 PB "A"';
        $alumnos[$i]['telefono'] = '4483794';
        $alumnos[$i]['celular'] = '15718489';                
        $i++;
        
        
        /* ID 1 */
        $alumnos[$i]['apellido'] = 'Cabral';
        $alumnos[$i]['nombre'] = 'Juan Gabriel';
        $alumnos[$i]['dni'] = '30935287';
        $alumnos[$i]['fechaNacimiento'] = '21/04/1984';
        $alumnos[$i]['cuil'] = '20309352875';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '01/01/2014';
        $alumnos[$i]['direccion'] = 'Necochea 1368 PB "A"';
        $alumnos[$i]['telefono'] = '4483794';
        $alumnos[$i]['celular'] = '15936655';                
        $i++;
        
        
        /* ID 2 */
        $alumnos[$i]['apellido'] = 'Maidana';
        $alumnos[$i]['nombre'] = 'Juan Manuel';
        $alumnos[$i]['dni'] = '20935287';
        $alumnos[$i]['fechaNacimiento'] = '20/01/1977';
        $alumnos[$i]['cuil'] = '20209352875';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = false;
        $alumnos[$i]['fechaIngreso'] = '01/01/2013';
        $alumnos[$i]['fechaEgreso'] = '01/01/2014';
        $alumnos[$i]['direccion'] = 'Santa Catalina 3848';
        $alumnos[$i]['telefono'] = '4483794';
        $alumnos[$i]['celular'] = '15936655';                
        $i++;
        
        /* ID 3 */                
        $alumnos[$i]['apellido'] = 'Parada';
        $alumnos[$i]['nombre'] = 'Paula';
        $alumnos[$i]['dni'] = '10935287';
        $alumnos[$i]['fechaNacimiento'] = '20/01/1965';
        $alumnos[$i]['cuil'] = '20109352875';
        $alumnos[$i]['sexo'] = 'f';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = false;
        $alumnos[$i]['fechaIngreso'] = '01/01/2001';
        $alumnos[$i]['fechaEgreso'] = '01/01/2015';
        $alumnos[$i]['direccion'] = 'Machaca Guemes';
        $alumnos[$i]['telefono'] = '0114483794';
        $alumnos[$i]['celular'] = '01115936655';                
        $i++;
        
        
        /* ID 4 */
        $alumnos[$i]['apellido'] = 'Tapia1';
        $alumnos[$i]['nombre'] = 'Maria del Huerto';
        $alumnos[$i]['dni'] = '30935288';
        $alumnos[$i]['fechaNacimiento'] = '16/10/1983';
        $alumnos[$i]['cuil'] = '27109352885';
        $alumnos[$i]['sexo'] = 'f';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = false;
        $alumnos[$i]['fechaIngreso'] = '01/01/2013';
        $alumnos[$i]['fechaEgreso'] = '01/01/2015';
        $alumnos[$i]['direccion'] = 'Santa Catalina 3848';
        $alumnos[$i]['telefono'] = '4483794';
        $alumnos[$i]['celular'] = '15936655';                
        $i++;
        
        
        /* ID 5 */
        $alumnos[$i]['apellido'] = 'Verón';
        $alumnos[$i]['nombre'] = 'Sergio Ramón';
        $alumnos[$i]['dni'] = '25935288';
        $alumnos[$i]['fechaNacimiento'] = '30/09/1976';
        $alumnos[$i]['cuil'] = '20259352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = false;
        $alumnos[$i]['fechaIngreso'] = '01/01/2001';
        $alumnos[$i]['fechaEgreso'] = '18/06/2013';
        $alumnos[$i]['direccion'] = 'Santa Catalina 4070';
        $alumnos[$i]['telefono'] = '4483794';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 6 */
        $alumnos[$i]['apellido'] = 'Boero';
        $alumnos[$i]['nombre'] = 'Michel';
        $alumnos[$i]['dni'] = '38935288';
        $alumnos[$i]['fechaNacimiento'] = '08/02/1993';
        $alumnos[$i]['cuil'] = '20389352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '01/01/2010';
        $alumnos[$i]['direccion'] = 'Donado 262';
        $alumnos[$i]['telefono'] = '4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 7 */
        $alumnos[$i]['apellido'] = 'Boero1';
        $alumnos[$i]['nombre'] = 'Alexis';
        $alumnos[$i]['dni'] = '31935288';
        $alumnos[$i]['fechaNacimiento'] = '01/01/1985';
        $alumnos[$i]['cuil'] = '20319352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '01/02/2010';
        $alumnos[$i]['direccion'] = 'Donado 262';
        $alumnos[$i]['telefono'] = '4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 8 */
        $alumnos[$i]['apellido'] = 'Cuevas';
        $alumnos[$i]['nombre'] = 'Marcelo';
        $alumnos[$i]['dni'] = '40935281';
        $alumnos[$i]['fechaNacimiento'] = '16/03/1998';
        $alumnos[$i]['cuil'] = '20409352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2009';
        $alumnos[$i]['direccion'] = 'Santa Catalina 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        
        /* ID 9 */
        $alumnos[$i]['apellido'] = 'Cuevas1';
        $alumnos[$i]['nombre'] = 'Ramon';
        $alumnos[$i]['dni'] = '39935237';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Santa Catalina 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 10 Alfredo nuñez */
        $alumnos[$i]['apellido'] = 'Nunez';
        $alumnos[$i]['nombre'] = 'Alfredo';
        $alumnos[$i]['dni'] = '39935286';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Poujade 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 11 Roman nuñez */
        $alumnos[$i]['apellido'] = 'Nunez1';
        $alumnos[$i]['nombre'] = 'Roman';
        $alumnos[$i]['dni'] = '39935285';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Poujade 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 12 Alfredo nuñez */
        $alumnos[$i]['apellido'] = 'Pro';
        $alumnos[$i]['nombre'] = 'Roman';
        $alumnos[$i]['dni'] = '39935267';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Poujade 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 13 Lucas Cabral */
        $alumnos[$i]['apellido'] = 'Cabral1';
        $alumnos[$i]['nombre'] = 'Lucas';
        $alumnos[$i]['dni'] = '39935268';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Fatima 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        
        /* ID 14 Mercedes Cabral */
        $alumnos[$i]['apellido'] = 'Cabral2';
        $alumnos[$i]['nombre'] = 'Mercedez';
        $alumnos[$i]['dni'] = '39935269';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Fatima 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 15 Belen Cabral */
        $alumnos[$i]['apellido'] = 'Cabral3';
        $alumnos[$i]['nombre'] = 'Belen';
        $alumnos[$i]['dni'] = '39935249';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Fatima 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 16 Soledad Cabral */
        $alumnos[$i]['apellido'] = 'Cabral4';
        $alumnos[$i]['nombre'] = 'Soledad';
        $alumnos[$i]['dni'] = '39935248';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Fatima 3920';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        /* ID 17  */
        $alumnos[$i]['apellido'] = 'Perez';
        $alumnos[$i]['nombre'] = 'Soledad';
        $alumnos[$i]['dni'] = '39935247';
        $alumnos[$i]['fechaNacimiento'] = '23/08/1997';
        $alumnos[$i]['cuil'] = '20399352885';
        $alumnos[$i]['sexo'] = 'm';
        $alumnos[$i]['vive'] = true;
        $alumnos[$i]['asiste'] = true;
        $alumnos[$i]['fechaIngreso'] = '22/02/2011';
        $alumnos[$i]['direccion'] = 'Barrio Las Marias';
        $alumnos[$i]['telefono'] = '3764 - 4517987';
        $alumnos[$i]['celular'] = '15936655';
        $i++;
        
        
        
                               
        foreach ($alumnos as $alumno) {
            $alumnosObjeto = new Alumno();
            $alumnosObjeto->setApellido($alumno['apellido']);
            $alumnosObjeto->setNombre($alumno['nombre']);
            $alumnosObjeto->setDni($alumno['dni']);
            
            $worldend = \DateTime::createFromFormat('Y-m-d', '2012-12-21');
            $alumnosObjeto->setFechaNacimiento($worldend);
            $alumnosObjeto->setCuil($alumno['cuil']);
            $alumnosObjeto->setSexo($alumno['sexo']);
            $alumnosObjeto->setVive($alumno['vive']);
            $alumnosObjeto->setAsiste($alumno['asiste']);
            
            $alumnosObjeto->setFechaIngreso($worldend);
            
            $alumnosObjeto->setDireccion($alumno['direccion']);
            $alumnosObjeto->setTelefono($alumno['telefono']);
            $alumnosObjeto->setCelular($alumno['celular']);
            
            $manager->persist($alumnosObjeto);
            
            $this->addReference(strtolower($alumno['apellido']), $alumnosObjeto);
            $manager->flush();
        }                   
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 3; 
    }
}