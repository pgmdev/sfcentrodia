<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\PersonaBundle\Entity\Personal;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPersonalData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarPersonales($manager);
    }
    
    public function cargarPersonales(ObjectManager $manager){                                
        $i= 0;
        /* ID: 1 */
        $personales[$i]['apellido'] = 'Tapia';
        $personales[$i]['nombre'] = 'Paulina';
        $personales[$i]['dni'] = '32914817';
        $personales[$i]['fechaNacimiento'] = '1987-01-28';
        $personales[$i]['cuil'] = '27329148186';
        $personales[$i]['sexo'] = 'f';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2012-09-30';
        $personales[$i]['direccion'] = 'Necochea 1368 PB "A"';
        $personales[$i]['telefono'] = '4483794';
        $personales[$i]['celular'] = '15718489';
        $personales[$i]['cargo'] = 'Analista de sistemas';
        $personales[$i]['externo'] = true;        
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 2 */
        $personales[$i]['apellido'] = 'Cabral';
        $personales[$i]['nombre'] = 'John Gabriel';
        $personales[$i]['dni'] = '30935286';
        $personales[$i]['fechaNacimiento'] = '1984-04-21';
        $personales[$i]['cuil'] = '20309352875';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '01/01/2014';
        $personales[$i]['direccion'] = 'Necochea 1368 PB "A"';
        $personales[$i]['telefono'] = '4483794';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Analista de sistemas';
        $personales[$i]['externo'] = true;        
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 3 */
        $personales[$i]['apellido'] = 'Maidana';
        $personales[$i]['nombre'] = 'Alberto';
        $personales[$i]['dni'] = '20935286';
        $personales[$i]['fechaNacimiento'] = '1977-01-20';
        $personales[$i]['cuil'] = '20209352875';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = false;
        $personales[$i]['fechaIngreso'] = '2013-01-01';
        $personales[$i]['fechaEgreso'] = '2014-01-01';
        $personales[$i]['direccion'] = 'Santa Catalina 3848';
        $personales[$i]['telefono'] = '4483794';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Psicólogo';
        $personales[$i]['externo'] = true;     
        $personales[$i]['salud'] = true;      
        $i++;
        
        /* ID: 4 */        
        $personales[$i]['apellido'] = 'Terminal';
        $personales[$i]['nombre'] = 'Paula';
        $personales[$i]['dni'] = '11935287';
        $personales[$i]['fechaNacimiento'] = '1965-01-20';
        $personales[$i]['cuil'] = '20109352875';
        $personales[$i]['sexo'] = 'f';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = false;
        $personales[$i]['fechaIngreso'] = '2001-01-01';
        $personales[$i]['fechaEgreso'] = '2015-01-01';
        $personales[$i]['direccion'] = 'Machaca Guemes';
        $personales[$i]['telefono'] = '0114483794';
        $personales[$i]['celular'] = '01115936655';
        $personales[$i]['cargo'] = 'Profesor idiomas';
        $personales[$i]['externo'] = true;  
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 5 */
        $personales[$i]['apellido'] = 'Tapia';
        $personales[$i]['nombre'] = 'Ana Maria';
        $personales[$i]['dni'] = '30935284';
        $personales[$i]['fechaNacimiento'] = '1983-10-16';
        $personales[$i]['cuil'] = '27109352885';
        $personales[$i]['sexo'] = 'f';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = false;
        $personales[$i]['fechaIngreso'] = '2013-01-01';
        $personales[$i]['fechaEgreso'] = '2015-01-01';
        $personales[$i]['direccion'] = 'Santa Catalina 3848';
        $personales[$i]['telefono'] = '4483794';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Diseñadora Grafica';
        $personales[$i]['externo'] = false;     
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 6 */
        $personales[$i]['apellido'] = 'Tomaso';
        $personales[$i]['nombre'] = 'Sergio Ramón';
        $personales[$i]['dni'] = '25935281';
        $personales[$i]['fechaNacimiento'] = '1976-09-30';
        $personales[$i]['cuil'] = '20259352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = false;
        $personales[$i]['fechaIngreso'] = '2001-01-01';
        $personales[$i]['fechaEgreso'] = '2013-06-18';
        $personales[$i]['direccion'] = 'Santa Catalina 4070';
        $personales[$i]['telefono'] = '4483794';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Autor de sistemas';
        $personales[$i]['externo'] = false;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 7 */
        $personales[$i]['apellido'] = 'Boero';
        $personales[$i]['nombre'] = 'Michel';
        $personales[$i]['dni'] = '311111111';
        $personales[$i]['fechaNacimiento'] = '1993-02-08';
        $personales[$i]['cuil'] = '20389352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2010-01-01';
        $personales[$i]['direccion'] = 'Donado 262';
        $personales[$i]['telefono'] = '4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Secretario';
        $personales[$i]['externo'] = false;
        $personales[$i]['salud'] = false;      
        $i++;                
        
        /* ID: 8 */
        $personales[$i]['apellido'] = 'Boero';
        $personales[$i]['nombre'] = 'Alexis';
        $personales[$i]['dni'] = '34935288';
        $personales[$i]['fechaNacimiento'] = '1985-01-01';
        $personales[$i]['cuil'] = '20319352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2010-02-01';
        $personales[$i]['direccion'] = 'Donado 262';
        $personales[$i]['telefono'] = '4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Secretario';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 9 */
        $personales[$i]['apellido'] = 'Picarino';
        $personales[$i]['nombre'] = 'Silvia';
        $personales[$i]['dni'] = '38935283';
        $personales[$i]['fechaNacimiento'] = '1993-02-08';
        $personales[$i]['cuil'] = '20389352885';
        $personales[$i]['sexo'] = 'f';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2010-01-01';
        $personales[$i]['direccion'] = 'Donado 262';
        $personales[$i]['telefono'] = '4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Directora';
        $personales[$i]['externo'] = false;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 10 */
        $personales[$i]['apellido'] = 'Papa';
        $personales[$i]['nombre'] = 'Marcelo';
        $personales[$i]['dni'] = '40935288';
        $personales[$i]['fechaNacimiento'] = '1998-03-16';
        $personales[$i]['cuil'] = '20409352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2009-02-22';
        $personales[$i]['direccion'] = 'Santa Catalina 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Portero';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 11 */
        $personales[$i]['apellido'] = 'Pavón';
        $personales[$i]['nombre'] = 'Ramon';
        $personales[$i]['dni'] = '39935288';
        $personales[$i]['fechaNacimiento'] = '1997-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Santa Catalina 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Profesor de Golf';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 12 */
        $personales[$i]['apellido'] = 'Cabral Sosa';
        $personales[$i]['nombre'] = 'Dacio Agustin';
        $personales[$i]['dni'] = '39935281';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Santa Catalina 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Electricista';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 13 */
        $personales[$i]['apellido'] = 'Ojeda';
        $personales[$i]['nombre'] = 'Matias Hernan';
        $personales[$i]['dni'] = '39935283';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Martin Fierro 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Mecanico';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 14 */
        $personales[$i]['apellido'] = 'Ojeda';
        $personales[$i]['nombre'] = 'Ramon';
        $personales[$i]['dni'] = '39935223';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Martin Fierro 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Mecanico';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID 15 - Claudio Maidana*/
        $personales[$i]['apellido'] = 'Maidana';
        $personales[$i]['nombre'] = 'Claudio Raul';
        $personales[$i]['dni'] = '39935289';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Lavalle 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Profesor Ciencias Sociales';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID 16 - ORLANDO MEDINA*/
        $personales[$i]['apellido'] = 'Medina';
        $personales[$i]['nombre'] = 'Orlando';
        $personales[$i]['dni'] = '39935210';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Lavalle 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Panadero';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID 16 - LUIS OMAR CABRAL*/
        $personales[$i]['apellido'] = 'Cabral';
        $personales[$i]['nombre'] = 'Luis Omar';
        $personales[$i]['dni'] = '39935221';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Lavalle 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Albañil';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID 15 - Geremias Kuchera*/
        $personales[$i]['apellido'] = 'Kuchera';
        $personales[$i]['nombre'] = 'Geremias Ruben';
        $personales[$i]['dni'] = '49935289';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Lavalle 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Profesor Ciencias Sociales';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        $personales[$i]['apellido'] = 'Fleitas';
        $personales[$i]['nombre'] = 'Laura Gisselle';
        $personales[$i]['dni'] = '59935289';
        $personales[$i]['fechaNacimiento'] = '1977-08-23';
        $personales[$i]['cuil'] = '20399352885';
        $personales[$i]['sexo'] = 'm';
        $personales[$i]['vive'] = true;
        $personales[$i]['activo'] = true;
        $personales[$i]['fechaIngreso'] = '2011-02-22';
        $personales[$i]['direccion'] = 'Lavalle 3920';
        $personales[$i]['telefono'] = '3764 - 4517987';
        $personales[$i]['celular'] = '15936655';
        $personales[$i]['cargo'] = 'Profesor Geografia';
        $personales[$i]['externo'] = true;
        $personales[$i]['salud'] = false;      
        $i++;
        
        /* ID: 8 */
//        $personales[$i]['apellido'] = 'Boero';
//        $personales[$i]['nombre'] = 'Daniel';
//        $personales[$i]['dni'] = '34935288';
//        $personales[$i]['fechaNacimiento'] = '1985-01-01';
//        $personales[$i]['cuil'] = '20319352885';
//        $personales[$i]['sexo'] = 'm';
//        $personales[$i]['vive'] = true;
//        $personales[$i]['activo'] = true;
//        $personales[$i]['fechaIngreso'] = '2010-02-01';
//        $personales[$i]['direccion'] = 'Donado 262';
//        $personales[$i]['telefono'] = '4517987';
//        $personales[$i]['celular'] = '15936655';
//        $personales[$i]['cargo'] = 'Transportista';
//        $personales[$i]['externo'] = true;
//        $personales[$i]['salud'] = false;      
//        $i++;
        
        
        
        $i = 0;
               
        foreach ($personales as $personal ) {
            
            $personalesObjeto = new Personal();
            $personalesObjeto->setApellido($personal['apellido']);
            $personalesObjeto->setNombre($personal['nombre']);
            $personalesObjeto->setDni($personal['dni']);     
            $fechaVencimiento = DateTime::createFromFormat('Y-m-d', $personal['fechaNacimiento']);
            $fechaIngreso = DateTime::createFromFormat('Y-m-d', $personal['fechaIngreso']);
            //$personalesObjeto->setFechaIngreso($fechaIngreso);
            $personalesObjeto->setFechaIngreso(DateTime::createFromFormat('Y-m-d', '2013-02-03'));
            $personalesObjeto->setFechaEgreso(null);
            /*if(isset($personal['fechaEgreso'])){
                $fechaEgreso = DateTime::createFromFormat('Y-m-d', $personal['fechaEgreso']);
                $personalesObjeto->setFechaEgreso($fechaEgreso);    
            }*/
            
            $personalesObjeto->setFechaNacimiento($fechaVencimiento);
            $personalesObjeto->setCuil($personal['cuil']);
            $personalesObjeto->setSexo($personal['sexo']);
            $personalesObjeto->setVive($personal['vive']);
            $personalesObjeto->setActivo($personal['activo']);
            //$personalesObjeto->setFechaIngreso($fechaIngreso);
            
            $personalesObjeto->setDireccion($personal['direccion']);
            $personalesObjeto->setTelefono($personal['telefono']);
            $personalesObjeto->setCelular($personal['celular']);
            $personalesObjeto->setCargo($personal['cargo']);
            $personalesObjeto->setExterno($personal['externo']);
            $personalesObjeto->setSalud($personal['salud']);
            $manager->persist($personalesObjeto);
            $manager->flush();
            
            if($i == 0){
                $this->addReference('paula', $personalesObjeto);
            }
            
            if($i == 1){
                $this->addReference('gabriel', $personalesObjeto);
            }                        
            
            /*if($i == 4){
                $this->addReference('maria', $personalesObjeto);
            }*/
            if($personal['nombre'] =="Ana Maria" && $personal['apellido']=="Tapia"){
                $this->addReference('maria', $personalesObjeto);
            }
            
            if($personal['nombre'] =="Michel" && $personal['apellido']=="Boero"){
                $this->addReference('michel', $personalesObjeto);
            }
            
            if($personal['nombre'] =="Alexis" && $personal['apellido']=="Boero"){
                $this->addReference('alexis', $personalesObjeto);
            }
            
            if($personal['nombre'] =="Claudio Raul" && $personal['apellido']=="Maidana"){
                $this->addReference('claudio', $personalesObjeto);
            }
            
            if($personal['nombre'] =="Geremias Ruben" && $personal['apellido']=="Kuchera"){
                $this->addReference('geremias', $personalesObjeto);
            }
            
            if($personal['nombre'] =="Laura Gisselle" && $personal['apellido']=="Fleitas"){
                $this->addReference('fleitas', $personalesObjeto);
            }
            
            /*if($i == 8){
                $this->addReference('michel', $personalesObjeto);
            }
            
            if($i == 9){
                $this->addReference('alexis', $personalesObjeto);
            }*/
            
            if($i == 10){
                $this->addReference('silvia', $personalesObjeto);
            }
            
            if($i == 11){
                $this->addReference('dacio', $personalesObjeto);
            }
            if($personal['nombre'] == "Daniel"){
                $this->addReference('daniel', $personalesObjeto);
            }
            
            
            $i++;
            
        }                   
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 1; 
    }
}