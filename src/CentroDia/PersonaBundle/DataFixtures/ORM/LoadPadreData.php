<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\PersonaBundle\Entity\Padre;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPadreData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarPadres($manager);
    }
    
                
    public function cargarPadres(ObjectManager $manager){                                
        $i= 0;
        
        $padres[$i]['apellido'] = 'Tapia';
        $padres[$i]['nombre'] = 'Hugo Orlando';
        $padres[$i]['dni'] = '12914818';
        $padres[$i]['fechaNacimiento'] = '27/01/1959';
        $padres[$i]['cuil'] = '27129148187';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Electromecánico';
        $padres[$i]['fechaIngreso'] = '30/09/2012';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'hugot@hotmail.com';
        $padres[$i]['direccion'] = 'Santa Catalina 3848';
        $padres[$i]['telefono'] = '4483794';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        $padres[$i]['apellido'] = 'Zárate';
        $padres[$i]['nombre'] = 'Matilde de Jesús ';
        $padres[$i]['dni'] = '14914818';
        $padres[$i]['fechaNacimiento'] = '26/06/1960';
        $padres[$i]['cuil'] = '27149148187';
        $padres[$i]['sexo'] = 'f';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Ama de casa';
        $padres[$i]['fechaIngreso'] = '16/08/2012';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'matilde@hotmail.com';
        $padres[$i]['direccion'] = 'Santa Catalina 3848';
        $padres[$i]['telefono'] = '4483794';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
                       
        $padres[$i]['apellido'] = 'Tapia';
        $padres[$i]['nombre'] = 'Clara Genoveva';
        $padres[$i]['dni'] = '14914819';
        $padres[$i]['fechaNacimiento'] = '07/06/1960';
        $padres[$i]['cuil'] = '27149148197';
        $padres[$i]['sexo'] = 'f';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Serigrafista';
        $padres[$i]['fechaIngreso'] = '16/08/2009';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'claragenoveva@hotmail.com';
        $padres[$i]['direccion'] = 'Pasaje 1654';
        $padres[$i]['telefono'] = '4483794';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        $padres[$i]['apellido'] = 'Boero';
        $padres[$i]['nombre'] = 'Hugo Daniel';
        $padres[$i]['dni'] = '10914819';
        $padres[$i]['fechaNacimiento'] = '21/09/1955';
        $padres[$i]['cuil'] = '27109148197';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Transportista';
        $padres[$i]['fechaIngreso'] = '16/08/2001';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'hugodaniel@hotmail.com';
        $padres[$i]['direccion'] = 'Donado 262';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        $padres[$i]['apellido'] = 'Pistarino';
        $padres[$i]['nombre'] = 'Silvia';
        $padres[$i]['dni'] = '10914919';
        $padres[$i]['fechaNacimiento'] = '08/03/1956';
        $padres[$i]['cuil'] = '27109149197';
        $padres[$i]['sexo'] = 'f';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Profesora';
        $padres[$i]['fechaIngreso'] = '16/08/2001';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'silvia_@hotmail.com';
        $padres[$i]['direccion'] = 'Donado 262';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        
        $padres[$i]['apellido'] = 'Cuevas';
        $padres[$i]['nombre'] = 'Marcelino';
        $padres[$i]['dni'] = '11914919';
        $padres[$i]['fechaNacimiento'] = '08/03/1963';
        $padres[$i]['cuil'] = '27119149197';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Chef';
        $padres[$i]['fechaIngreso'] = '22/05/2007';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'marcelino@hotmail.com';
        $padres[$i]['direccion'] = 'Santa Catalina 3920';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        
        $padres[$i]['apellido'] = 'Verón';
        $padres[$i]['nombre'] = 'Ramón';
        $padres[$i]['dni'] = '10214919';
        $padres[$i]['fechaNacimiento'] = '08/03/1954';
        $padres[$i]['cuil'] = '27102149197';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Albañil';
        $padres[$i]['fechaIngreso'] = '22/05/2005';
        $padres[$i]['tutor'] = true;
        $padres[$i]['direccion'] = 'Santa Catalina 4070';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        $padres[$i]['apellido'] = 'Paiva';
        $padres[$i]['nombre'] = 'Hugo';
        $padres[$i]['dni'] = '11925919';
        $padres[$i]['fechaNacimiento'] = '11/03/1966';
        $padres[$i]['cuil'] = '27119259197';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Locutor';
        $padres[$i]['fechaIngreso'] = '22/05/2001';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'paiva@hotmail.com';
        $padres[$i]['direccion'] = 'FM STREET';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        
        $padres[$i]['apellido'] = 'Operuk';
        $padres[$i]['nombre'] = 'Diego';
        $padres[$i]['dni'] = '26956175';
        $padres[$i]['fechaNacimiento'] = '26/04/1981';
        $padres[$i]['cuil'] = '27269561757';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Seguridad';
        $padres[$i]['fechaIngreso'] = '22/05/2011';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'diegoope@hotmail.com';
        $padres[$i]['direccion'] = 'Montevideo 1654 PB A';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $i++;
        
        
        $padres[$i]['apellido'] = 'Kuchera';
        $padres[$i]['nombre'] = 'Jeremías';
        $padres[$i]['dni'] = '25956175';
        $padres[$i]['fechaNacimiento'] = '25/01/1981';
        $padres[$i]['cuil'] = '27259561757';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Profesor';
        $padres[$i]['fechaIngreso'] = '22/05/2012';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'jeremias@hotmail.com';
        $padres[$i]['direccion'] = 'Calle S/N';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        $padres[$i]['apellido'] = 'Kuchera';
        $padres[$i]['nombre'] = 'Jeremías';
        $padres[$i]['dni'] = '25956175';
        $padres[$i]['fechaNacimiento'] = '25/01/1981';
        $padres[$i]['cuil'] = '27259561757';
        $padres[$i]['sexo'] = 'm';
        $padres[$i]['vive'] = true;
        $padres[$i]['ocupacion'] = 'Profesor';
        $padres[$i]['fechaIngreso'] = '22/05/2012';
        $padres[$i]['tutor'] = true;
        $padres[$i]['email'] = 'jeremias@hotmail.com';
        $padres[$i]['direccion'] = 'Calle S/N';
        $padres[$i]['telefono'] = '4517987';
        $padres[$i]['celular'] = '15718489';
        
        
        
        
                      
        foreach ($padres as $padre) {
            $padresObjeto = new Padre();
            $padresObjeto->setApellido($padre['apellido']);
            $padresObjeto->setNombre($padre['nombre']);
            $padresObjeto->setDni($padre['dni']);
            //$date = new DateTime($padre['fechaNacimiento']);
            //$date = new DateTime('2012-12-05');            
            //$date = $date->format('Y-m-d');
            $worldend = \DateTime::createFromFormat('Y-m-d', '2012-12-21');
            $padresObjeto->setFechaNacimiento($worldend);            
            $padresObjeto->setCuil($padre['cuil']);
            $padresObjeto->setSexo($padre['sexo']);
            $padresObjeto->setVive($padre['vive']);                        
            $padresObjeto->setOcupacion($padre['ocupacion']);            
            $padresObjeto->setTutor($padre['tutor']);            
            $padresObjeto->setDireccion($padre['direccion']);
            $padresObjeto->setTelefono($padre['telefono']);
            $padresObjeto->setCelular($padre['celular']);
            //$this->addReference(strtolower($padre['nombre']), $padresObjeto);
            $manager->persist($padresObjeto);
            $manager->flush();
        }                   
        
    }
    
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 2; 
    }
}