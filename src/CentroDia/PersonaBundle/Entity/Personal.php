<?php

namespace CentroDia\PersonaBundle\Entity;

use CentroDia\PersonaBundle\Entity\Persona;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personal
 */
class Personal extends Persona
{
    
    /**
     * @var \DateTime
     */
    private $fechaIngreso;

    /**
     * @var \DateTime
     */
    private $fechaEgreso;

    /**
     * @var boolean
     */
    private $activo;
    
    /**
     * @var string
     */
    private $cargo;
    
    /**
     * @var boolean
     */
    private $externo;
    
    
    /**
     * @var string
     */
    private $usuario;
    

       
    public function __construct() {
        $this->setVive(true);
        $this->setActivo(true);
        $this->talleres = new \Doctrine\Common\Collections\ArrayCollection();
        
    }        
    
  
    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Personal
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaEgreso
     *
     * @param \DateTime $fechaEgreso
     * @return Personal
     */
    public function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    /**
     * Get fechaEgreso
     *
     * @return \DateTime 
     */
    public function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Personal
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Personal
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;

        return $this;
    }

    /**
     * Get profesion
     *
     * @return string 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set externo
     *
     * @param boolean $externo
     * @return Personal
     */
    public function setExterno($externo)
    {
        $this->externo = $externo;

        return $this;
    }

    /**
     * Get externo
     *
     * @return boolean 
     */
    public function getExterno()
    {
        return $this->externo;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     * @return Personal
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string 
     */
    public function getCargo()
    {
        return $this->cargo;
    }
    

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Personal
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    
    
    public function __toString() {
        return (string)$this->getApellido()." - ".$this->getNombre();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $talleres;
    
    
    
    public function setTalleres(\Doctrine\Common\Collections\Collection $talleres)
    {
        $this->talleres = $talleres;
        foreach ($talleres as $taller) {
            $taller->setPersonal($this);
        }
    }

    /**
     * Get talleres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTalleres()
    {
        return $this->talleres;
    }
    /**
     * @var \CentroDia\PersonaBundle\Entity\Responsable
     */
    private $responsable;


    /**
     * Set responsable
     *
     * @param \CentroDia\PersonaBundle\Entity\Responsable $responsable
     * @return Personal
     */
    public function setResponsable(\CentroDia\PersonaBundle\Entity\Responsable $responsable = null)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return \CentroDia\PersonaBundle\Entity\Responsable 
     */
    public function getResponsable()
    {
        return $this->responsable;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $informes;


    /**
     * Add informes
     *
     * @param \CentroDia\AcademicoBundle\Entity\Informe $informes
     * @return Personal
     */
    public function addInforme(\CentroDia\AcademicoBundle\Entity\Informe $informes)
    {
        $this->informes[] = $informes;

        return $this;
    }

    /**
     * Remove informes
     *
     * @param \CentroDia\AcademicoBundle\Entity\Informe $informes
     */
    public function removeInforme(\CentroDia\AcademicoBundle\Entity\Informe $informes)
    {
        $this->informes->removeElement($informes);
    }

    /**
     * Get informes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformes()
    {
        return $this->informes;
    }

    /**
     * Add talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     * @return Personal
     */
    public function addTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres[] = $talleres;

        return $this;
    }

    /**
     * Remove talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     */
    public function removeTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres->removeElement($talleres);
    }
    /**
     * @var boolean
     */
    private $salud;


    /**
     * Set salud
     *
     * @param boolean $salud
     * @return Personal
     */
    public function setSalud($salud)
    {
        $this->salud = $salud;

        return $this;
    }

    /**
     * Get salud
     *
     * @return boolean 
     */
    public function getSalud()
    {
        return $this->salud;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $turnos;


    /**
     * Add turnos
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Turno $turnos
     * @return Personal
     */
    public function addTurno(\CentroDia\ExpedienteBundle\Entity\Turno $turnos)
    {
        $this->turnos[] = $turnos;

        return $this;
    }

    /**
     * Remove turnos
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Turno $turnos
     */
    public function removeTurno(\CentroDia\ExpedienteBundle\Entity\Turno $turnos)
    {
        $this->turnos->removeElement($turnos);
    }

    /**
     * Get turnos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTurnos()
    {
        return $this->turnos;
    }
}
