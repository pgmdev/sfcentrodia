<?php

namespace CentroDia\PersonaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use CentroDia\PersonaBundle\Entity\Persona;

/**
 * Alumno
 */
class Alumno extends Persona
{
    
    /**
     * @var boolean
     */
    private $asiste;

    /**
     * @var \DateTime
     */
    private $fechaIngreso;

    /**
     * @var \DateTime
     */
    private $fechaEgreso;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $padres;
    
            
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->padres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setVive(true); 
    }

    public function __toString() {
        return $this->getApellido()." - ".$this->getNombre();
    }

    
    /**
     * Set asiste
     *
     * @param boolean $asiste
     * @return Alumno
     */
    public function setAsiste($asiste)
    {
        $this->asiste = $asiste;

        return $this;
    }

    /**
     * Get asiste
     *
     * @return boolean 
     */
    public function getAsiste()
    {
        return $this->asiste;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Alumno
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaEgreso
     *
     * @param \DateTime $fechaEgreso
     * @return Alumno
     */
    public function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    /**
     * Get fechaEgreso
     *
     * @return \DateTime 
     */
    public function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }

    /**
     * Add padres
     *
     * @param \CentroDia\PersonaBundle\Entity\Padre $padres
     * @return Alumno
     */
    public function addPadre(\CentroDia\PersonaBundle\Entity\Padre $padres)
    {
        $this->padres[] = $padres;

        return $this;
    }

    /**
     * Remove padres
     *
     * @param \CentroDia\PersonaBundle\Entity\Padre $padres
     */
    public function removePadre(\CentroDia\PersonaBundle\Entity\Padre $padres)
    {
        $this->padres->removeElement($padres);
    }

    /**
     * Get padres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPadres()
    {
        return $this->padres;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Alumno
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }
    /**
     * @var \CentroDia\ExpedienteBundle\Entity\Certificado
     */
    private $certificado;


    /**
     * Set certificado
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Certificado $certificado
     * @return Alumno
     */
    public function setCertificado(\CentroDia\ExpedienteBundle\Entity\Certificado $certificado = null)
    {
        $this->certificado = $certificado;

        return $this;
    }

    /**
     * Get certificado
     *
     * @return \CentroDia\ExpedienteBundle\Entity\Certificado 
     */
    public function getCertificado()
    {
        return $this->certificado;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $certificados;


    /**
     * Add certificados
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Certificado $certificados
     * @return Alumno
     */
    public function addCertificado(\CentroDia\ExpedienteBundle\Entity\Certificado $certificados)
    {
        $this->certificados[] = $certificados;

        return $this;
    }

    /**
     * Remove certificados
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Certificado $certificados
     */
    public function removeCertificado(\CentroDia\ExpedienteBundle\Entity\Certificado $certificados)
    {
        $this->certificados->removeElement($certificados);
    }

    /**
     * Get certificados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCertificados()
    {
        return $this->certificados;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $talleres;


    

    /**
     * Add talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     * @return Alumno
     */
    public function addTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres[] = $talleres;

        return $this;
    }

    /**
     * Remove talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     */
    public function removeTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres->removeElement($talleres);
    }

    /**
     * Get talleres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTalleres()
    {
        return $this->talleres;
    }
    /**
     * @var \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle
     */
    private $asistenciaDetalle;


    /**
     * Set asistenciaDetalle
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle
     * @return Alumno
     */
    public function setAsistenciaDetalle(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle = null)
    {
        $this->asistenciaDetalle = $asistenciaDetalle;

        return $this;
    }

    /**
     * Get asistenciaDetalle
     *
     * @return \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle 
     */
    public function getAsistenciaDetalle()
    {
        return $this->asistenciaDetalle;
    }
    /**
     * @var \CentroDia\ExpedienteBundle\Entity\Turno
     */
    private $turno;


    /**
     * Set turno
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Turno $turno
     * @return Alumno
     */
    public function setTurno(\CentroDia\ExpedienteBundle\Entity\Turno $turno = null)
    {
        $this->turno = $turno;

        return $this;
    }

    /**
     * Get turno
     *
     * @return \CentroDia\ExpedienteBundle\Entity\Turno 
     */
    public function getTurno()
    {
        return $this->turno;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $asistencias;


    /**
     * Add asistencias
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistencias
     * @return Alumno
     */
    public function addAsistencia(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistencias)
    {
        $this->asistencias[] = $asistencias;

        return $this;
    }

    /**
     * Remove asistencias
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistencias
     */
    public function removeAsistencia(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistencias)
    {
        $this->asistencias->removeElement($asistencias);
    }

    /**
     * Get asistencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsistencias()
    {
        return $this->asistencias;
    }
}
