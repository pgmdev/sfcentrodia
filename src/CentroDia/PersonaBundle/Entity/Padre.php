<?php

namespace CentroDia\PersonaBundle\Entity;

use CentroDia\PersonaBundle\Entity\Persona;
/**
 * Padre
 */
class Padre  extends Persona
{
    
    /**
     * @var string
     */
    private $ocupacion;

    /**
     * @var boolean
     */
    private $tutor;

      
    public function __toString() {
        return $this->getNombre();
    }
    /**
     * Set ocupacion
     *
     * @param string $ocupacion
     * @return Padre
     */
    public function setOcupacion($ocupacion)
    {
        $this->ocupacion = $ocupacion;

        return $this;
    }

    /**
     * Get ocupacion
     *
     * @return string 
     */
    public function getOcupacion()
    {
        return $this->ocupacion;
    }

    /**
     * Set tutor
     *
     * @param boolean $tutor
     * @return Padre
     */
    public function setTutor($tutor)
    {
        $this->tutor = $tutor;

        return $this;
    }

    /**
     * Get tutor
     *
     * @return boolean 
     */
    public function getTutor()
    {
        return $this->tutor;
    }
}
