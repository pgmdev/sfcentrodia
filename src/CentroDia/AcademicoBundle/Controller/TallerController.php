<?php

namespace CentroDia\AcademicoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CentroDia\AcademicoBundle\Entity\Taller;
use CentroDia\AcademicoBundle\Form\TallerType;

/**
 * Taller controller.
 *
 */
class TallerController extends Controller
{

    /**
     * Lists all Taller entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();       
        
        $academicoManager = $this->get('academico.manager');
        
        $aTalleres = $academicoManager->listarTalleres();

        return $this->render('AcademicoBundle:Taller:index.html.twig', array(
            'entities' => $aTalleres,
        ));
    }
    /**
     * Creates a new Taller entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = new Taller();                                
                                
        $form = $this->createCreateForm($entity);
        
        $form->submit($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'El taller '.$entity->getNombre()." se ha guardado correctamente"
            );

            return $this->redirect($this->generateUrl('taller'));
        }

        return $this->render('AcademicoBundle:Taller:taller.html.twig', array(
            'entity' => $entity,            
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Taller entity.
     *
     * @param Taller $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Taller $entity)
    {
                
        $form = $this->createForm(new TallerType(), $entity );    

        return $form;
    }

    /**
     * Displays a form to create a new Taller entity.
     *
     */
    public function newAction()
    {
        
        
        $entity = new Taller();                       
        
        $form   = $this->createCreateForm($entity);

        return $this->render('AcademicoBundle:Taller:taller.html.twig', array(
            'entity' => $entity,                    
            'form'   => $form->createView(),
        ));
    }

   

    /**
     * Displays a form to edit an existing Taller entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcademicoBundle:Taller')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Taller entity.');
        }

        $editForm = $this->createEditForm($entity);        

        return $this->render('AcademicoBundle:Taller:taller.html.twig', array(
            'edicion'     => true,
            'taller'      => $entity,
            'form'   => $editForm->createView()            
        ));
    }

    /**
    * Creates a form to edit a Taller entity.
    *
    * @param Taller $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Taller $entity)
    {
        $form = $this->createForm(new TallerType(), $entity );       

        return $form;
    }
    /**
     * Edits an existing Taller entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcademicoBundle:Taller')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Taller entity.');
        }

        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'El taller '.$entity->getNombre()." se ha guardado correctamente"
            );
            return $this->redirect($this->generateUrl('taller'));
        }

        return $this->render('AcademicoBundle:Taller:taller.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            
        ));
    }
    

   
}
