<?php

namespace CentroDia\AcademicoBundle\Controller;

use CentroDia\AcademicoBundle\Entity\AsistenciaCabecera;
use CentroDia\AcademicoBundle\Entity\AsistenciaDetalle;
use CentroDia\AcademicoBundle\Entity\Horario;
use CentroDia\AcademicoBundle\Form\AsistenciaCabeceraType;
use CentroDia\AcademicoBundle\Form\HorarioType;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\Exception;

/**
 * Horario controller.
 *

 */
class HorarioController extends Controller {

    /**
     * Lists all Horario entities.
     *

     */
    public function indexAction() {

        $academicoManager = $this->get('academico.manager');

        $aTalleres = $academicoManager->getTalleresByPersonal($this->getUser()->getPersonal()->getId());

        $horarioInicial = $academicoManager->hasHorarioInicial($this->getUser()->getPersonal()->getId());


        return $this->render('AcademicoBundle:Horario:horarioSemanal.html.twig', array(
                    'talleres' => $aTalleres,
                    'horarioInicial' => $horarioInicial
        ));
    }

    public function indexTestAction() {

        $academicoManager = $this->get('academico.manager');

        $horarioInicial = $academicoManager->hasHorarioInicial($this->getUser()->getPersonal()->getId());

        $aTalleres = $academicoManager->getTalleresByPersonal($this->getUser()->getPersonal()->getId());

        return $this->render('AcademicoBundle:Horario:horarioSemanalTest.html.twig', array(
                    'talleres' => $aTalleres,
                    'horarioInicial' => $horarioInicial
        ));
    }

    public function createAction(Request $request) {
        $entity = new Horario();
        $form = $this->createForm(new HorarioType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('taller'));
        }


        return $this->render('AcademicoBundle:Horario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Horario entity.
     *

     */
    public function newAction() {
        $entity = new Horario();
        $form = $this->createForm(new HorarioType(), $entity);


        return $this->render('AcademicoBundle:Horario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Horario entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcademicoBundle:Horario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcademicoBundle:Horario:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Horario entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcademicoBundle:Horario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horario entity.');
        }

        $editForm = $this->createForm(new HorarioType(), $entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render('AcademicoBundle:Horario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Horario entity.
     *

     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcademicoBundle:Horario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new HorarioType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('horario_edit', array('id' => $id)));
        }


        return $this->render('AcademicoBundle:Horario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Horario entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcademicoBundle:Horario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Horario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('horario'));
    }

    /**
     * Creates a form to delete a Horario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    public function createAjaxAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $horariosReq = stripslashes($request->get('data'));

        ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

        $horariosEventos = json_decode($horariosReq, true);

        if (count($horariosEventos) > 0) {
            try {
                foreach ($horariosEventos as $horario) {
                    $horarioObject = new Horario();
                    $user_tz = 'America/Argentina/Buenos_Aires';
                    $horaInicial = new DateTime($horario['horaInicio'], new DateTimeZone($user_tz));
                    $tallerNew = $em->getRepository('AcademicoBundle:Taller')->find($horario['tallerID']);
                    $horarioObject->addTaller($tallerNew);
                    $horarioObject->setHoraInicial($horaInicial);

                    if (isset($horario['horaFin']) && $horario['horaFin'] != null) {
                        $horaFinal = new DateTime($horario['horaFin'], new DateTimeZone($user_tz));
                        $horarioObject->setHoraFinal($horaFinal);
                    }
                    if ($request->get('isConfiguracionInicial') == false || $request->get('isConfiguracionInicial') != true) {
                        $dia = date('l', strtotime($horario['fecha']));
                        switch ($dia) {
                            case 'Monday':
                                $dia = 1;
                                $horarioObject->setDia($dia);
                                break;
                            case 'Tuesday':
                                $dia = 2;
                                $horarioObject->setDia($dia);
                                break;

                            case 'Wednesday':
                                $dia = 3;
                                $horarioObject->setDia($dia);
                                break;

                            case 'Thursday':
                                $dia = 4;
                                $horarioObject->setDia($dia);
                                break;

                            case 'Friday':
                                $dia = 5;
                                $horarioObject->setDia($dia);
                                break;
                        }
                    } else {
                        $fecha = DateTime::createFromFormat('Y-m-d', $horario['fecha']);
                        $horarioObject->setFecha($fecha);
                    }

                    $em->persist($horarioObject);
                }
                $em->flush();
                $respuesta = array('status' => 'ok', 'message' => 'Se han cargado los horarios correctamente');
            } catch (Exception $e) {
                $respuesta = array('status' => 'fail', 'message' => $e->getMessage());
            }
        }



        $response = new Response(json_encode($respuesta));

        return $response;
    }

    /**
     * Displays a form to edit an existing Horario entity.
     *
     */
    public function verAction($id) {
        $em = $this->getDoctrine()->getManager();

        $asistenciaCabecera = new AsistenciaCabecera();

        $horario = $em->getRepository('AcademicoBundle:Horario')->find($id);

        $asistenciaCabecera->setHorario($horario);

        $params = array('container' => $this->container, 'horario' => $id);
        
        //var_dump($horario);

        $talleres = $horario->getTalleres();
        foreach ($talleres as $taller) {
            foreach ($taller->getAlumnos() as $alumno) {
                $params['alumno'] = $alumno->getId();
                $asistenciaDetalle = new AsistenciaDetalle();
                $asistenciaDetalle->setAlumno($alumno);
                $asistenciaDetalle->setAsistio(true);
                //$asistenciaDetalle->setAsistenciaCabecera($asistenciaCabecera);
                $asistenciaCabecera->addAsistenciaDetalle($asistenciaDetalle);
            }
        }




        $form = $this->createForm(new AsistenciaCabeceraType($params), $asistenciaCabecera);

        return $this->render('AcademicoBundle:Horario:verHorario.html.twig', array(
                    'horario' => $horario,
                    'entity' => $asistenciaCabecera,
                    'form' => $form->createView(),
        ));
    }

    public function getAjaxHorarioSemanalAction(Request $request) {
        $academicoManager = $this->get('academico.manager');

        //$pars = array('desde'=> $request->get('start'), 'hasta'=> $request->get('end'));

        $respuesta = array();

        if (($request->get('inicial'))) {
            $horarioSemanal = $academicoManager->getHorarioInicial($request->get('idHorario'));
        } else {
            //$horarioSemanal = $academicoManager->getHorarioSemanalByFechaActual($request->get('start'), $request->get('end'));         
            $horarioSemanal = $academicoManager->getHorarioSemanalByPersonal($this->getUser()->getPersonal()->getId());
        }

        if (count($horarioSemanal) > 0 || !empty($horarioSemanal)) {
            $respuesta['message'] = 'Se ';
            $respuesta['status'] = 'ok';
            $respuesta['results'] = $horarioSemanal;
        } else {
            $respuesta['message'] = 'No tiene horarios cargados para esta semana';
            $respuesta['status'] = 'fail';
        }


        return new Response(json_encode($respuesta));
    }

    public function getAjaxHorarioSemanalTestAction(Request $request) {
        $academicoManager = $this->get('academico.manager');

        $params = array('desde' => $request->get('start'), 'hasta' => $request->get('end'));

        $respuesta = array();

        $horarioSemanal = $academicoManager->getHorariosByFecha($params);


        if (count($horarioSemanal) > 0 || !empty($horarioSemanal)) {
            $respuesta['message'] = 'Se ';
            $respuesta['status'] = 'ok';
            $respuesta['results'] = $horarioSemanal;
        } else {
            $respuesta['message'] = 'No tiene horarios cargados para esta semana';
            $respuesta['status'] = 'fail';
        }


        return new Response(json_encode($respuesta));
    }

    private function control() {
        
    }

    public function marcarAsistenciaAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $asistenciaCabecera = $em->getRepository('AcademicoBundle:AsistenciaCabecera')->findByHorario($id);
        $horario = $em->getRepository('AcademicoBundle:Horario')->find($id);
        

        if (!$asistenciaCabecera) {            
            $asistenciaCabecera = new AsistenciaCabecera();
            $asistenciaCabecera->setHorario($horario);
        }
                
        $params = array('container' => $this->container, 'horario' => $id);

        $talleres = $horario->getTalleres();
        foreach ($talleres as $taller) {
            foreach ($taller->getAlumnos() as $alumno) {
                $params['alumno'] = $alumno->getId();
                $asistenciaDetalle = new AsistenciaDetalle();
                $asistenciaDetalle->setAlumno($alumno);                
                $asistenciaDetalle->setAsistenciaCabecera($asistenciaCabecera);
                $asistenciaCabecera->addAsistenciaDetalle($asistenciaDetalle);
            }
        }

        $editForm = $this->createForm(new AsistenciaCabeceraType($params), $asistenciaCabecera);


        $editForm->submit($request);

        if ('POST' == $request->getMethod()) {
            if ($editForm->isValid()) {
                $em->persist($asistenciaCabecera);
                $em->flush();
        
                return $this->redirect($this->generateUrl('horario_test'));
            }                        
        }
        
    }

}
