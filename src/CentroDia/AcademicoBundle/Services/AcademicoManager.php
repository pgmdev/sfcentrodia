<?php

namespace CentroDia\AcademicoBundle\Services;

use DateTime;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PersonaManager
 *
 * @author gabriel
 */
class AcademicoManager {
    
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /*
     * Devuelve un listado de  activos 
     */

     public function listarTalleres() {
        
        $tallerRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Taller');
        $aTalleres = $tallerRepository->getListarTalleres();
        return $aTalleres;
    }
    // Obtiene todos los talleres del personal logueado
    public function getTalleresByPersonal($params) {
        
        $horarioRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Taller');
        $aHorarios = $horarioRepository->getTalleresByPersonal($params);
        return $aHorarios;
    }
    
    public function hasHorarioInicial($personal){
        $horarioRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Horario');
        $aHorario = $horarioRepository->hasHorarioInicial($personal);
        
        if(!empty($aHorario)){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function getHorarioInicial($horario){
        $horarioRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Horario');
        $horarioInicial = $horarioRepository->getHorarioInicial($horario);
        //var_dump($hora);
        /*if(!empty($hora)){
            return array('horario'=> true,'id'=> $hora[0]['id']);
        }else{
            return array('horario'=> false);
        }*/
        return $horarioInicial;
        
    }
    
    public function getHorariosByFecha($params){
        
        $horarioRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Horario');
        $aHorarios = $horarioRepository->getHorariosByFecha($params);
        
        return $aHorarios;
    }
    
    public function getHorarioSemanalByPersonal($personal){
        $horarioRepository = $this->container->get('doctrine')->getRepository('AcademicoBundle:Horario');
        $horarioSemanal = $horarioRepository->getHorarioSemanalByPersonal($personal);
        
        return $horarioSemanal;
        
    }
    
    
}
