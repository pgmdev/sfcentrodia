<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Informe
 */
class Informe
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $observaciones;

    /**
     * @var string
     */
    private $archivoAdjunto;
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Informe
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Informe
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return Informe
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set archivoAdjunto
     *
     * @param string $archivoAdjunto
     * @return Informe
     */
    public function setArchivoAdjunto(UploadedFile $archivoAdjunto = null)
    {
        $this->archivoAdjunto = $archivoAdjunto;
        
    }

    /**
     * Get archivoAdjunto
     *
     * @return string 
     */
    public function getArchivoAdjunto()
    {
        return $this->archivoAdjunto;
    }
    /**
     * @var \CentroDia\PersonaBundle\Entity\Personal
     */
    private $personal;


    /**
     * Set personal
     *
     * @param \CentroDia\PersonaBundle\Entity\Personal $personal
     * @return Informe
     */
    public function setPersonal(\CentroDia\PersonaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \CentroDia\PersonaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    
    
    
    
    
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }
    
    public function upload()
{
    // the file property can be empty if the field is not required
    if (null === $this->getFile()) {
        return;
    }

    // use the original file name here but you should
    // sanitize it at least to avoid any security issues

    // move takes the target directory and then the
    // target filename to move to
    $this->getFile()->move(
        $this->getUploadRootDir(),
        $this->getFile()->getClientOriginalName()
    );

    // set the path property to the filename where you've saved the file
    $this->path = $this->getFile()->getClientOriginalName();

    // clean up the file property as you won't need it anymore
    $this->file = null;
}
    /**
     * @var string
     */
    private $nombreAdjunto;

    /**
     * @var string
     */
    private $pathAdjunto;


    /**
     * Set nombreAdjunto
     *
     * @param string $nombreAdjunto
     * @return Informe
     */
    public function setNombreAdjunto($nombreAdjunto)
    {
        $this->nombreAdjunto = $nombreAdjunto;

        return $this;
    }

    /**
     * Get nombreAdjunto
     *
     * @return string 
     */
    public function getNombreAdjunto()
    {
        return $this->nombreAdjunto;
    }

    /**
     * Set pathAdjunto
     *
     * @param string $pathAdjunto
     * @return Informe
     */
    public function setPathAdjunto($pathAdjunto)
    {
        $this->pathAdjunto = $pathAdjunto;

        return $this;
    }

    /**
     * Get pathAdjunto
     *
     * @return string 
     */
    public function getPathAdjunto()
    {
        return $this->pathAdjunto;
    }
}
