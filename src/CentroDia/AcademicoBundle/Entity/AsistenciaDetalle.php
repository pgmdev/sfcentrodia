<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AsistenciaDetalle
 */
class AsistenciaDetalle
{
    /**
     * @var integer
     */
    private $id;

    
    /**
     * @var string
     */
    private $alumno;

    /**
     * @var boolean
     */
    private $asistio;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    


    /**
     * @var \CentroDia\AcademicoBundle\Entity\AsistenciaCabecera
     */
    private $asistenciaCabecera;


    /**
     * Set asistio
     *
     * @param boolean $asistio
     * @return AsistenciaDetalle
     */
    public function setAsistio($asistio)
    {
        $this->asistio = $asistio;

        return $this;
    }

    /**
     * Get asistio
     *
     * @return boolean 
     */
    public function getAsistio()
    {
        return $this->asistio;
    }

    /**
     * Set asistenciaCabecera
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera
     * @return AsistenciaDetalle
     */
    public function setAsistenciaCabecera(\CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera = null)
    {
        $this->asistenciaCabecera = $asistenciaCabecera;

        return $this;
    }

    /**
     * Get asistenciaCabecera
     *
     * @return \CentroDia\AcademicoBundle\Entity\AsistenciaCabecera 
     */
    public function getAsistenciaCabecera()
    {
        return $this->asistenciaCabecera;
    }

    /**
     * Set alumno
     *
     * @param \CentroDia\PersonaBundle\Entity\Alumno $alumno
     * @return AsistenciaDetalle
     */
    public function setAlumno(\CentroDia\PersonaBundle\Entity\Alumno $alumno = null)
    {
        $this->alumno = $alumno;

        return $this;
    }

    /**
     * Get alumno
     *
     * @return \CentroDia\PersonaBundle\Entity\Alumno 
     */
    public function getAlumno()
    {
        return $this->alumno;
    }
}
