<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taller
 */
class Taller
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Taller
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Taller
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @var string
     */
    private $propuestaTerapeutica;


    /**
     * Set propuestaTerapeutica
     *
     * @param string $propuestaTerapeutica
     * @return Taller
     */
    public function setPropuestaTerapeutica($propuestaTerapeutica)
    {
        $this->propuestaTerapeutica = $propuestaTerapeutica;

        return $this;
    }

    /**
     * Get propuestaTerapeutica
     *
     * @return string 
     */
    public function getPropuestaTerapeutica()
    {
        return $this->propuestaTerapeutica;
    }
   

   
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $personales;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->alumnos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add personales
     *
     * @param \CentroDia\AcademicoBundle\Entity\Personal 
     * @return Taller
     */
    public function addPersonal(\CentroDia\AcademicoBundle\Entity\Personal $personal)
    {
        $this->personales[] = $personal;
        
        $personal->addTaller($this);

        return $this;
    }

    /**
     * Remove personales
     *
     * @param \CentroDia\AcademicoBundle\Entity\Personal $personales
     */
    public function removePersonal(\CentroDia\AcademicoBundle\Entity\Personal $personal)
    {
        $this->personales->removeElement($personal);
    }

    /**
     * Get personales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPersonales()
    {
        return $this->personales;
    }
    
    public function __toString() {
        return $this->getNombre();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $horarios;


    /**
     * Add personales
     *
     * @param \CentroDia\PersonaBundle\Entity\Personal $personales
     * @return Taller
     */
    public function addPersonale(\CentroDia\PersonaBundle\Entity\Personal $personales)
    {
        $this->personales[] = $personales;

        return $this;
    }

    /**
     * Remove personales
     *
     * @param \CentroDia\PersonaBundle\Entity\Personal $personales
     */
    public function removePersonale(\CentroDia\PersonaBundle\Entity\Personal $personales)
    {
        $this->personales->removeElement($personales);
    }

    /**
     * Add horarios
     *
     * @param \CentroDia\AcademicoBundle\Entity\Horario $horarios
     * @return Taller
     */
    public function addHorario(\CentroDia\AcademicoBundle\Entity\Horario $horarios)
    {
        $this->horarios[] = $horarios;

        return $this;
    }

    /**
     * Remove horarios
     *
     * @param \CentroDia\AcademicoBundle\Entity\Horario $horarios
     */
    public function removeHorario(\CentroDia\AcademicoBundle\Entity\Horario $horarios)
    {
        $this->horarios->removeElement($horarios);
    }

    /**
     * Get horarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHorarios()
    {
        return $this->horarios;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $alumnos;


    /**
     * Add alumnos
     *
     * @param \CentroDia\PersonaBundle\Entity\Alumno $alumnos
     * @return Taller
     */
    /*public function addAlumno(\CentroDia\PersonaBundle\Entity\Alumno $alumno)
    {
        $this->alumnos[] = $alumno;

        return $this;
    }*/
    
    public function setAlumnos(\Doctrine\Common\Collections\Collection $alumnos)
    {
        $this->alumnos = $alumnos;
        foreach ($alumnos as $alumno) {
            $alumno->setTaller($this);
        }
    }

    /**
     * Remove alumnos
     *
     * @param \CentroDia\PersonaBundle\Entity\Alumno $alumnos
     */
    public function removeAlumno(\CentroDia\PersonaBundle\Entity\Alumno $alumnos)
    {
        $this->alumnos->removeElement($alumnos);
    }

    /**
     * Get alumnos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAlumnos()
    {
        return $this->alumnos;
    }
}
