<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * HorarioRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HorarioRepository extends EntityRepository {

    public function hasHorarioInicial($personal) {
        $qb = $this->createQueryBuilder('h')
                ->select('h')
                ->innerJoin('h.talleres', 't')
                ->innerJoin('t.personales', 'p')
                ->where('p.id = :personal')
                ->setParameter('personal', $personal);

        return $qb->getQuery()->getResult();
    }

    public function getHorarioById($id) {
        $qb = $this->createQueryBuilder('h')
                ->select('h')
                ->where('h.id = :id')
                ->setParameter('id', $id);

        return $qb;
    }

    public function getHorarioSemanalByFechaActual($desde, $hasta) {

        $em = $this->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('fecha', 'fecha');
        $rsm->addScalarResult('nombre', 'nombre');
        $rsm->addScalarResult('inicio', 'inicio');
        $rsm->addScalarResult('fin', 'fin');


        $query = $em->createNativeQuery("
                            SELECT h.id, date_format(h.fecha, '%Y-%m-%d') as fecha, 
                                    concat(fecha, 'T', h.hora_inicial)as inicio,                                      
                                    h.hora_final as fin, 
                                    t.nombre as nombre        
                            FROM cd_academico_horario h
                            INNER JOIN cd_talleres_horarios AS th ON h.id = th.horario_id
                            INNER JOIN cd_taller AS t ON th.taller_id = t.id                                                        
                            WHERE h.fecha BETWEEN '$desde'  AND '$hasta' "
                . "", $rsm);
        return $query->getArrayResult();
    }

    public function getHorarioInicial($horario) {

        $em = $this->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('inicio', 'inicio');
        $rsm->addScalarResult('dia', 'dia');
        $rsm->addScalarResult('fin', 'fin');
        $rsm->addScalarResult('nombre', 'nombre');


        $query = $em->createNativeQuery("
                            SELECT h.id, h.hora_inicial as inicio, 
                                   h.hora_final as fin, 
                                   h.dia as dia,
                                   t.nombre as nombre        
                            FROM cd_academico_horario h                            
                            INNER JOIN cd_talleres_horarios AS th ON h.id = th.horario_id
                            INNER JOIN cd_taller AS t ON th.taller_id = t.id                                                        
                            WHERE h.id = $horario "
                . "", $rsm);
        return $query->getArrayResult();
    }

    public function getHorarioSemanalByPersonal($personal) {
        $em = $this->getEntityManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('nombre', 'nombre');
        $rsm->addScalarResult('inicio', 'inicio');
        $rsm->addScalarResult('fin', 'fin');
        $rsm->addScalarResult('dia', 'dia');


        $query = $em->createNativeQuery("
                            SELECT h.id,                                      
                                   h.hora_final as fin, 
                                   h.hora_inicial as inicio, 
                                   h.dia as dia, 
                                   t.nombre as nombre        
                            FROM cd_academico_horario h
                            INNER JOIN cd_talleres_horarios AS th ON h.id = th.horario_id
                            INNER JOIN cd_talleres_personales AS tp ON th.taller_id = tp.taller_id  
                            INNER JOIN cd_taller AS t ON th.taller_id = t.id
                            WHERE tp.personal_id ='$personal' "
                . "", $rsm);
        return $query->getArrayResult();
    }

    public function getHorariosByFecha($params) {
        $em = $this->getEntityManager();
        $rsm = new ResultSetMapping();
        //$rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('nombre', 'nombre');
        $rsm->addScalarResult('inicio', 'inicio');
        $rsm->addScalarResult('fin', 'fin');
        $rsm->addScalarResult('dia', 'dia');
        $rsm->addScalarResult('personal', 'personal');
        $rsm->addScalarResult('fecha', 'fecha');
        $rsm->addScalarResult('horarioId', 'horarioId');
        $rsm->addScalarResult('tallerId', 'tallerId');
        $rsm->addScalarResult('asistencias', 'asistencias');

        $query = $em->createNativeQuery("
                            select t.id as tallerId, ah.id as horarioId, count(ac.id) as asistencias, 
                                   ah.hora_inicial as inicio,
                                   ah.dia as dia,
                                   ah.fecha as fecha,
                                   t.nombre as nombre,
                                   p.id as personal
                            from cd_taller t 
                            left join cd_talleres_horarios th on t.id =th.taller_id 
                            left join cd_academico_horario ah on th.horario_id = ah.id 
                            left join cd_asistencia_cabecera ac on ac.horario_id = ah.id 
                            left join cd_talleres_personales tp on th.taller_id = tp.taller_id
                            left join cd_personal p on tp.personal_id = p.id
                            where ah.fecha between  '" . $params["desde"] . "'  AND '" . $params["hasta"] . "'
                            group by t.id, ah.id
                ", $rsm);
        return $query->getArrayResult();
    }

}
