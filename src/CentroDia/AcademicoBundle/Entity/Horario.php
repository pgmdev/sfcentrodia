<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Horario
 */
class Horario
{
    /**
     * @var integer
     */
    private $id;   

    /**
     * @var \DateTime
     */
    private $horario;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set horario
     *
     * @param \DateTime $horario
     * @return Horario
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;

        return $this;
    }

    /**
     * Get horario
     *
     * @return \DateTime 
     */
    public function getHorario()
    {
        return $this->horario;
    }
    /**
     * @var \DateTime
     */
    private $hora;

    /**
     * @var integer
     */
    private $dia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $talleres;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->talleres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setInicial(true);
    }
    
    public function __toString() {
        return $this->getId();
        
    }
        

    /**
     * Set hora
     *
     * @param \DateTime $hora
     * @return Horario
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set dia
     *
     * @param integer $dia
     * @return Horario
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return integer 
     */
    public function getDia()
    {
        return $this->dia;
    }   

    
    /**
     * Add horarios
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     * @return Horario
     */
    public function addTaller(\CentroDia\AcademicoBundle\Entity\Taller $taller)
    {
        $this->talleres[] = $taller;

        return $this;
    }
    
    /**
     * Remove talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     */
    public function removeTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres->removeElement($talleres);
    }

    /**
     * Get talleres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTalleres()
    {
        return $this->talleres;
    }
    
   
    /**
     * @var boolean
     */
    private $activo;

    /**
     * @var boolean
     */
    private $inicial;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Horario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set inicial
     *
     * @param boolean $inicial
     * @return Horario
     */
    public function setInicial($inicial)
    {
        $this->inicial = $inicial;

        return $this;
    }

    /**
     * Get inicial
     *
     * @return boolean 
     */
    public function getInicial()
    {
        return $this->inicial;
    }
    /**
     * @var \DateTime
     */
    private $fecha;


    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Horario
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $asistenciaCabecera;


    /**
     * Add asistenciaCabecera
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera
     * @return Horario
     */
    public function addAsistenciaCabecera(\CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera)
    {
        $this->asistenciaCabecera[] = $asistenciaCabecera;

        return $this;
    }

    /**
     * Remove asistenciaCabecera
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera
     */
    public function removeAsistenciaCabecera(\CentroDia\AcademicoBundle\Entity\AsistenciaCabecera $asistenciaCabecera)
    {
        $this->asistenciaCabecera->removeElement($asistenciaCabecera);
    }

    /**
     * Get asistenciaCabecera
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsistenciaCabecera()
    {
        return $this->asistenciaCabecera;
    }
    /**
     * @var \DateTime
     */
    private $hora_inicial;

    /**
     * @var \DateTime
     */
    private $hora_final;


    /**
     * Set hora_inicial
     *
     * @param \DateTime $horaInicial
     * @return Horario
     */
    public function setHoraInicial($horaInicial)
    {
        $this->hora_inicial = $horaInicial;

        return $this;
    }

    /**
     * Get hora_inicial
     *
     * @return \DateTime 
     */
    public function getHoraInicial()
    {
        return $this->hora_inicial;
    }

    /**
     * Set hora_final
     *
     * @param \DateTime $horaFinal
     * @return Horario
     */
    public function setHoraFinal($horaFinal)
    {
        $this->hora_final = $horaFinal;

        return $this;
    }

    /**
     * Get hora_final
     *
     * @return \DateTime 
     */
    public function getHoraFinal()
    {
        return $this->hora_final;
    }

    /**
     * Add talleres
     *
     * @param \CentroDia\AcademicoBundle\Entity\Taller $talleres
     * @return Horario
     */
    public function addTallere(\CentroDia\AcademicoBundle\Entity\Taller $talleres)
    {
        $this->talleres[] = $talleres;

        return $this;
    }
}
