<?php

namespace CentroDia\AcademicoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AsistenciaCabecera
 */
class AsistenciaCabecera
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $horario;

    /**
     * @var string
     */
    private $personal;

    /**
     * @var \DateTime
     */
    private $fecha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set horario
     *
     * @param string $horario
     * @return AsistenciaCabecera
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;

        return $this;
    }

    /**
     * Get horario
     *
     * @return string 
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * Set personal
     *
     * @param string $personal
     * @return AsistenciaCabecera
     */
    public function setPersonal($personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return string 
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return AsistenciaCabecera
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * @var \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle
     */
    private $asistenciaDetalle;


    /**
     * Set asistenciaDetalle
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle
     * @return AsistenciaCabecera
     */
    public function setAsistenciaDetalle(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle = null)
    {
        $this->asistenciaDetalle = $asistenciaDetalle;

        return $this;
    }

    /**
     * Get asistenciaDetalle
     *
     * @return \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle 
     */
    public function getAsistenciaDetalle()
    {
        return $this->asistenciaDetalle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->asistenciaDetalle = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return (string)$this->getId();
    }
    
    /**
     * Add asistenciaDetalle
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle
     * @return AsistenciaCabecera
     */
    public function addAsistenciaDetalle(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle)
    {
        $this->asistenciaDetalle[] = $asistenciaDetalle;

        return $this;
    }

    /**
     * Remove asistenciaDetalle
     *
     * @param \CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle
     */
    public function removeAsistenciaDetalle(\CentroDia\AcademicoBundle\Entity\AsistenciaDetalle $asistenciaDetalle)
    {
        $this->asistenciaDetalle->removeElement($asistenciaDetalle);
    }
}
