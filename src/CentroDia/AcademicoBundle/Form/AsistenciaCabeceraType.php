<?php

namespace CentroDia\AcademicoBundle\Form;

use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsistenciaCabeceraType extends AbstractType
{
    private $arrayParams;
    const   entityPersonal = "AcademicoBundle:Horario"; 
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                
        
            $entityManager = $this->arrayParams['container']->get('doctrine')->getManager();
        
            $personalTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['horario'], self::entityPersonal);                        
        
            $builder->add($builder
                    ->create('horario', 'hidden')
                    ->addModelTransformer($personalTransformer));
                        
            $builder
                    ->add('asistenciaDetalle', 'collection',array(
                    'type'=> new AsistenciaDetalleType($this->arrayParams)                    
            ))
               
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AcademicoBundle\Entity\AsistenciaCabecera',
            'cascade_validation'=> true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asistencia_cabecera';
    }
}
