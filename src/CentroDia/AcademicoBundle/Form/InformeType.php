<?php

namespace CentroDia\AcademicoBundle\Form;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InformeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
              
        $builder
                ->add('fecha', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha:', 'attr' => array('class' => 'form-control')))
                ->add('nombre', 'text', array('label' => 'Nombre'))
                ->add('observaciones', 'textarea', array('label' => 'Observaciones'))
                ->add('archivoAdjunto', 'file', array(
                    'required' => false,
                    'label' => 'Archivo adjunto',
                    'mapped' => false,
                    'attr' => array('class' => 'input-file uniform_on')
                ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AcademicoBundle\Entity\Informe'
        ));
    }

    public function getName() {
        return 'centrodia_academicobundle_informe';
    }

}
