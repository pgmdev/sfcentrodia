<?php

namespace CentroDia\AcademicoBundle\Form;

use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TallerType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder               
            ->add('nombre', 'text', array('label'=> 'Nombre'))            
            ->add('descripcion', 'textarea', array('label'=> 'Descripcion'))
            ->add('alumnos', 'entity', array(
                'required'=> false,
                'empty_value'=> 'Elejir un alumno',
                'multiple' => true,   // Multiple selection allowed            
                //'property' => 'nombre', // Assuming that the entity has a "name" property
                'class'    => 'PersonaBundle:Alumno',
            ))           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AcademicoBundle\Entity\Taller'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'taller';
    }
}
