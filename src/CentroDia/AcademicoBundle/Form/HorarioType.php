<?php

namespace CentroDia\AcademicoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HorarioType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('hora', 'time', array(
                    'input' => 'datetime',
                    'widget' => 'choice',
                ))
                ->add('dia', 'choice', array(
                    'choices' => array('1' => 'Lunes', '2' => 'Martes', '3' => 'Miercoles', '4' => 'Jueves', '5' => 'Viernes'),
                    'required' => false,
                        )
                )
                ->add('inicial')
                ->add('talleres','collection', array(
                    'type' => new TallerType(),                    
                    'prototype' => true,                    
                    
                ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AcademicoBundle\Entity\Horario'
        ));
    }

    public function getName() {
        return 'centrodia_academicobundle_horario';
    }

}
