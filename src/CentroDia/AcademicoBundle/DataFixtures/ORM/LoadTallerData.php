<?php

namespace CentroDia\AcademicoBundle\DataFixtures\ORM;

use CentroDia\AcademicoBundle\Entity\Taller;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTallerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarTaller($manager);
    }
    
                
    public function cargarTaller(ObjectManager $manager){                                
        $i= 0;
        
        $talleres[$i]['nombre'] = 'Golf';
        $talleres[$i]['descripcion'] = 'Escuelita de Golf';                       
        $i++;
        
        $talleres[$i]['nombre'] = 'Informática ';
        $talleres[$i]['descripcion'] = 'Escuelita de informática';                                
        $i++;
                               
        $talleres[$i]['nombre'] = 'Cocina ';
        $talleres[$i]['descripcion'] = 'Aprender cocina';                
        $i++;
        
        $talleres[$i]['nombre'] = 'Jardinería ';
        $talleres[$i]['descripcion'] = 'Diversas dicas de jardines';                
        $i++;
        
        $talleres[$i]['nombre'] = 'Manualidades ';
        $talleres[$i]['descripcion'] = 'Manualidades';      
        $i++;
        
        $talleres[$i]['nombre'] = 'Teatro ';
        $talleres[$i]['descripcion'] = 'teatro';          
        $i++;
        
        $talleres[$i]['nombre'] = 'Carpintería ';
        $talleres[$i]['descripcion'] = 'Carpintería';        
        $i++;
        
        $talleres[$i]['nombre'] = 'Tejido';
        $talleres[$i]['descripcion'] = 'Tejido';
   
                                      
        foreach ($talleres as $taller) {                                   
            $talleresObjeto = new Taller();
            $talleresObjeto->setNombre($taller['nombre']);
            $talleresObjeto->setDescripcion($taller['descripcion']);                                                                  
            $manager->persist($talleresObjeto);
            $manager->flush();                      
        }                                
    }
    
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 12; 
    }
}