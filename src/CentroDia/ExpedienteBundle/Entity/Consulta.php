<?php

namespace CentroDia\ExpedienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consulta
 */
class Consulta
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombreCorto;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $observaciones;

    /**
     * @var integer
     */
    private $periodicidadInforme;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreCorto
     *
     * @param string $nombreCorto
     * @return Consulta
     */
    public function setNombreCorto($nombreCorto)
    {
        $this->nombreCorto = $nombreCorto;

        return $this;
    }

    /**
     * Get nombreCorto
     *
     * @return string 
     */
    public function getNombreCorto()
    {
        return $this->nombreCorto;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Consulta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return Consulta
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set periodicidadInforme
     *
     * @param integer $periodicidadInforme
     * @return Consulta
     */
    public function setPeriodicidadInforme($periodicidadInforme)
    {
        $this->periodicidadInforme = $periodicidadInforme;

        return $this;
    }

    /**
     * Get periodicidadInforme
     *
     * @return integer 
     */
    public function getPeriodicidadInforme()
    {
        return $this->periodicidadInforme;
    }
    /**
     * @var \CentroDia\ExpedienteBundle\Entity\Turno
     */
    private $turno;


    /**
     * Set turno
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Turno $turno
     * @return Consulta
     */
    public function setTurno(\CentroDia\ExpedienteBundle\Entity\Turno $turno = null)
    {
        $this->turno = $turno;

        return $this;
    }

    /**
     * Get turno
     *
     * @return \CentroDia\ExpedienteBundle\Entity\Turno 
     */
    public function getTurno()
    {
        return $this->turno;
    }
}
