<?php

namespace CentroDia\ExpedienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Turno
 */
class Turno
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var \DateTime
     */
    private $hora;

    /**
     * @var boolean
     */
    private $padresAsisten;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Turno
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set hora
     *
     * @param \DateTime $hora
     * @return Turno
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set padresAsisten
     *
     * @param boolean $padresAsisten
     * @return Turno
     */
    public function setPadresAsisten($padresAsisten)
    {
        $this->padresAsisten = $padresAsisten;

        return $this;
    }

    /**
     * Get padresAsisten
     *
     * @return boolean 
     */
    public function getPadresAsisten()
    {
        return $this->padresAsisten;
    }
    /**
     * @var \CentroDia\ExpedienteBundle\Entity\Consulta
     */
    private $consulta;

    /**
     * @var \CentroDia\PersonaBundle\Entity\Personal
     */
    private $personal;


    /**
     * Set consulta
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Consulta $consulta
     * @return Turno
     */
    public function setConsulta(\CentroDia\ExpedienteBundle\Entity\Consulta $consulta = null)
    {
        $this->consulta = $consulta;

        return $this;
    }

    /**
     * Get consulta
     *
     * @return \CentroDia\ExpedienteBundle\Entity\Consulta 
     */
    public function getConsulta()
    {
        return $this->consulta;
    }

    /**
     * Set personal
     *
     * @param \CentroDia\PersonaBundle\Entity\Personal $personal
     * @return Turno
     */
    public function setPersonal(\CentroDia\PersonaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \CentroDia\PersonaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    /**
     * @var \CentroDia\PersonalBundle\Entity\Alumno
     */
    private $alumno;


    /**
     * Set alumno
     *
     * @param \CentroDia\PersonalBundle\Entity\Alumno $alumno
     * @return Turno
     */
    public function setAlumno(\CentroDia\PersonalBundle\Entity\Alumno $alumno = null)
    {
        $this->alumno = $alumno;

        return $this;
    }

    /**
     * Get alumno
     *
     * @return \CentroDia\PersonalBundle\Entity\Alumno 
     */
    public function getAlumno()
    {
        return $this->alumno;
    }
}
