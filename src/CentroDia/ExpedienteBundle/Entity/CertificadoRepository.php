<?php

namespace CentroDia\ExpedienteBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CertificadoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CertificadoRepository extends EntityRepository
{
     public function getListarCertificadosActivos(){
        $qb = $this->createQueryBuilder('ce')
                    ->select('ce')                                                     
                    ->orderBy('ce.id', 'ASC');                              
        
        return $qb;
    }
}
