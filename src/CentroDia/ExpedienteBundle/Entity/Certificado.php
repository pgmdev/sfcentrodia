<?php

namespace CentroDia\ExpedienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Certificado
 */
class Certificado
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaEmision;

    /**
     * @var \DateTime
     */
    private $fechaVencimiento;

    /**
     * @var string
     */
    private $ciudadEmision;

    /**
     * @var string
     */
    private $emitidoPor;

    /**
     * @var boolean
     */
    private $acompanante;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $diagnostico;
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaEmision
     *
     * @param \DateTime $fechaEmision
     * @return Certificado
     */
    public function setFechaEmision($fechaEmision)
    {
        $this->fechaEmision = $fechaEmision;

        return $this;
    }

    /**
     * Get fechaEmision
     *
     * @return \DateTime 
     */
    public function getFechaEmision()
    {
        return $this->fechaEmision;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return Certificado
     */
    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    /**
     * Set ciudadEmision
     *
     * @param string $ciudadEmision
     * @return Certificado
     */
    public function setCiudadEmision($ciudadEmision)
    {
        $this->ciudadEmision = $ciudadEmision;

        return $this;
    }

    /**
     * Get ciudadEmision
     *
     * @return string 
     */
    public function getCiudadEmision()
    {
        return $this->ciudadEmision;
    }

    /**
     * Set emitidoPor
     *
     * @param string $emitidoPor
     * @return Certificado
     */
    public function setEmitidoPor($emitidoPor)
    {
        $this->emitidoPor = $emitidoPor;

        return $this;
    }

    /**
     * Get emitidoPor
     *
     * @return string 
     */
    public function getEmitidoPor()
    {
        return $this->emitidoPor;
    }

    /**
     * Set acompanante
     *
     * @param boolean $acompanante
     * @return Certificado
     */
    public function setAcompanante($acompanante)
    {
        $this->acompanante = $acompanante;

        return $this;
    }

    /**
     * Get acompanante
     *
     * @return boolean 
     */
    public function getAcompanante()
    {
        return $this->acompanante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Certificado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set diagnostico
     *
     * @param string $diagnostico
     * @return Certificado
     */
    public function setDiagnostico($diagnostico)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string 
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }
    


    

    
    /**
     * @var \CentroDia\PersonaBundle\Entity\Alumno
     */
    private $alumno;


    /**
     * Set alumno
     *
     * @param \CentroDia\PersonaBundle\Entity\Alumno $alumno
     * @return Certificado
     */
    public function setAlumno(\CentroDia\PersonaBundle\Entity\Alumno $alumno = null)
    {
        $this->alumno = $alumno;

        return $this;
    }

    /**
     * Get alumno
     *
     * @return \CentroDia\PersonaBundle\Entity\Alumno 
     */
    public function getAlumno()
    {
        return $this->alumno;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $alumnos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->alumnos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add alumnos
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Alumnos $alumnos
     * @return Certificado
     */
    public function addAlumno(\CentroDia\ExpedienteBundle\Entity\Alumnos $alumnos)
    {
        $this->alumnos[] = $alumnos;

        return $this;
    }

    /**
     * Remove alumnos
     *
     * @param \CentroDia\ExpedienteBundle\Entity\Alumnos $alumnos
     */
    public function removeAlumno(\CentroDia\ExpedienteBundle\Entity\Alumnos $alumnos)
    {
        $this->alumnos->removeElement($alumnos);
    }

    /**
     * Get alumnos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAlumnos()
    {
        return $this->alumnos;
    }
}
