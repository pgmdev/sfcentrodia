<?php

namespace CentroDia\ExpedienteBundle\Controller;

use CentroDia\ExpedienteBundle\Entity\Certificado;
use CentroDia\ExpedienteBundle\Form\CertificadoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller {

    public function newCertificadoAction(Request $request, $id) {

        $classAlumno = "PersonaBundle:Alumno";

        $certificado = new Certificado();

        $arrayParams = array('container' => $this->container, 'alumno' => $id, 'class' => $classAlumno);

        $form = $this->createForm(new CertificadoType($arrayParams), $certificado);


        return $this->render('ExpedienteBundle:Default:certificado.html.twig', array(
                    'edit'=> false,
                    'entity' => $certificado,
                    'id' => $id,
                    'form' => $form->createView()
        ));
    }

    public function updateCertificadoAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();       
        
        $arrayParams = array();

        $certificado = $em->getRepository('ExpedienteBundle:Certificado')->findOneByAlumno($id);
        
        if (!$certificado) {
        
            $certificado = new Certificado();
            $arrayParams['alumno']= $id;
        
        }
        
               
        $form = $this->createForm(new CertificadoType($arrayParams), $certificado);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($certificado);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'El certificado se ha guardado correctamente.'
            );

            return $this->redirect($this->generateUrl('certificado_edit', array('id'=> $id)));
        }

        return $this->render('ExpedienteBundle:Default:certificado.html.twig', array( 
                    'edit'=> false,
                    'certificado' => $certificado,
                    'form' => $form->createView(),
        ));
    }
    
    
    
    
    public function createCertificadoAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();       
        
        $arrayParams = array();
                                
        $certificado = new Certificado();
        
        $arrayParams['alumno']= $id;
                              
        $form = $this->createForm(new CertificadoType($arrayParams), $certificado);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($certificado);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'El certificado se ha guardado correctamente.'
            );

            return $this->redirect($this->generateUrl('certificado_edit', array('id'=> $id)));
        }

        return $this->render('ExpedienteBundle:Default:certificado.html.twig', array( 
                    'edit'=> false,
                    'certificado' => $certificado,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Lists all Banco entities.
     *
     */
    public function indexCertificadoAction(Request $request) {
        $administracionManager = $this->get('administracion.manager');
        $certificados = $administracionManager->listarCertificadosActivos();
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
                $certificados, $this->get('request')->query->get('page', 1), 20
        );

        return $this->render('ExpedienteBundle:Certificado:index.html.twig', array('entities' => $entities,
                        //                'formFilter'=>$form->createView()
                        )
        );
    }

    
    public function editCertificadoAction($id) {
        $em = $this->getDoctrine()->getManager();
       
        
        $certificado = $em->getRepository('ExpedienteBundle:Certificado')->findOneByAlumno($id);

        if (!$certificado) {
            throw $this->createNotFoundException('No se puede encontrar el certificado.');
        }
        
        $arrayParams['alumno']= $id;
        
        $classCertificado = "ExpedienteBundle:Certificado";
                
        $form = $this->createForm(new CertificadoType($arrayParams), $certificado);


        return $this->render('ExpedienteBundle:Default:certificado.html.twig', array(
                    'id'=>$id,
                    'edit' => true,
                    'entity' => $certificado,
                    'form' => $form->createView()
        ));
    }

}
