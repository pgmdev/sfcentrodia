<?php

namespace CentroDia\ExpedienteBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CertificadoType extends AbstractType {
    
    private $arrayParams;
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        
        $alumno =   $this->arrayParams['alumno'];
        
        $factory = $builder->getFormFactory();
        
        
        $builder                              
                ->add('alumno', 'entity', array('class'=> 'PersonaBundle:Alumno', 
                                                'label'=> 'Alumno',
                                                'query_builder' => function (EntityRepository $repository) use ($alumno){
                                                    $qb = $repository->getAlumnoById($alumno);                                                                                                                                                                                                      
                                                    return $qb;
                                               }
                    ))
                //->add('alumno')
                ->add('fechaEmision', 'datetime', array(
                    'widget' => 'single_text', 
                    'format' => 'yyyy-MM-dd', 
                    'label' => 'Fecha Emisión:', 
                    'attr' => array('class' => 'form-control date'),
                    'invalid_message' => 'Fecha invalida')                        
                        )
                
                ->add('fechaVencimiento', 'datetime', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha Vencimiento:', 'attr' => array('class' => 'form-control date')))
                ->add('ciudadEmision', 'text', array('label' => 'Ciudad de emisión:', 'attr' => array('class' => 'form-control')))                
                ->add('descripcion', 'textarea', array('label' => 'Descripcion:', 'attr' => array('class' => 'form-control')))                
                ->add('diagnostico', 'textarea', array('label' => 'Diagnostico:', 'attr' => array('class' => 'form-control')))                
                ->add('emitidoPor', 'text', array('label' => 'Emitido por:', 'attr' => array('class' => 'form-control')))                
                ->add('acompanante', 'checkbox', array('label' => 'Acompañante:', 'attr' => array('class' => 'form-control')))                
                ;

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\ExpedienteBundle\Entity\Certificado'
        ));
    }
    
    
    /**
     * @return string
     */
    public function getName() {
        return 'centrodia_expedientebundle_certificado';
    }

}
