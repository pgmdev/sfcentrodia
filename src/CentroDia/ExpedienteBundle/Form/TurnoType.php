<?php

namespace CentroDia\ExpedienteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TurnoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
           ->add()                                                                                                                                                                                     
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\ExpedienteBundle\Entity\Turno'
        ));
    }

    public function getName()
    {
        return 'centrodia_expedientebundle_turno';
    }
}
