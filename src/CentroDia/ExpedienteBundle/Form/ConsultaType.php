<?php

namespace CentroDia\ExpedienteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConsultaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
           
                       
           
            ->add('nombreCorto', 'text', array('label'=> 'nombreCorto'))
            ->add('descripcion', 'textarea', array('label'=> 'Descripcion'))            
            ->add('observaciones', 'textarea', array('label'=> 'observaciones'))
            ->add('periodicidadInforme', 'text', array('label'=> 'periodicidadInforme'))           
           
                       
           
           
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\ExpedienteBundle\Entity\Consulta'
        ));
    }

    public function getName()
    {
        return 'centrodia_expedientebundle_consulta';
    }
}
