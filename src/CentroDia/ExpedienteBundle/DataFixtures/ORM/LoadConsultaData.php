<?php

namespace CentroDia\ExpedienteBundle\DataFixtures\ORM;

use CentroDia\ExpedienteBundle\Entity\Consulta;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use DateTime;

class LoadConsultaData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarConsultas($manager);
    }
    
    public function cargarConsultas(ObjectManager $manager){                                
        $i= 0;
        
        /* ID 0 */
        $consultas[$i]['nombreCorto'] = 'Kisiología';
        $consultas[$i]['descripcion'] = 'Kisiología';
        $consultas[$i]['observacion'] = 'Kisiología';        
        $consultas[$i]['periodicidaInforme'] = 180;
        
        $i++;
        
        /* ID 0 */
        $consultas[$i]['nombreCorto'] = 'psicología';
        $consultas[$i]['descripcion'] = 'psicología';
        $consultas[$i]['observacion'] = 'psicología';        
        $consultas[$i]['periodicidaInforme'] = 200;
        
        $i++;
        
        
        /* ID 2 */
        $consultas[$i]['nombreCorto'] = 'podología';
        $consultas[$i]['descripcion'] = 'podología';
        $consultas[$i]['observacion'] = 'podología';        
        $consultas[$i]['periodicidaInforme'] = 180;
                    
        $i++;
        
        /* ID 3 */                
        $consultas[$i]['nombreCorto'] = 'equino terapia';
        $consultas[$i]['descripcion'] = 'equino terapia';
        $consultas[$i]['observacion'] = 'equino terapia';        
        $consultas[$i]['periodicidaInforme'] = 360;
                                  
        $i++;
        
        
        /* ID 4 */
        $consultas[$i]['nombreCorto'] = 'nado terapia';
        $consultas[$i]['descripcion'] = 'nado terapia';
        $consultas[$i]['observacion'] = 'nado terapia';        
        $consultas[$i]['periodicidaInforme'] = 240;               
         
        
        
                               
        foreach ($consultas as $consulta) {
            $consultasObjeto = new Consulta();                                  
            $consultasObjeto->setNombreCorto($consulta['nombreCorto']);           
            $consultasObjeto->setDescripcion($consulta['descripcion']);
            $consultasObjeto->setObservaciones($consulta['observacion']);
            $consultasObjeto->setPeriodicidadInforme($consulta['periodicidaInforme']);
                                                
            $manager->persist($consultasObjeto);
            $manager->flush();
        }                   
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 22; 
    }
}