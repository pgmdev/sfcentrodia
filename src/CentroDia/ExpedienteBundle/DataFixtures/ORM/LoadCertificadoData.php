<?php

namespace CentroDia\ExpedienteBundle\DataFixtures\ORM;

use CentroDia\ExpedienteBundle\Entity\Certificado;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use DateTime;

class LoadCertificadoData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarCertificados($manager);
    }
    
    public function cargarCertificados(ObjectManager $manager){                                
        $i= 0;
        
        /* ID 0 */
        //$certificados[$i]['alumno'] = $this->getReference('tapia');        
        $certificados[$i]['fechaEmision'] = '1965-01-20';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;
        $i++;
        
        
        /* ID 1 */
        //$certificados[$i]['alumno'] = $this->getReference('tapia1');        
        $certificados[$i]['fechaEmision'] = '1965-01-22';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        /* ID 2 */
        //$certificados[$i]['alumno'] = $this->getReference('cabral');        
        $certificados[$i]['fechaEmision'] = '1965-01-23';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 3 */                
        //$certificados[$i]['alumno'] = $this->getReference('maidana');        
        $certificados[$i]['fechaEmision'] = '1965-01-24';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        /* ID 4 */
        //$certificados[$i]['alumno'] = $this->getReference('parada');        
        $certificados[$i]['fechaEmision'] = '1965-01-25';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        /* ID 5 */
        //$certificados[$i]['alumno'] = $this->getReference('boero');        
        $certificados[$i]['fechaEmision'] = '1965-01-26';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        /* ID 6 */
        //$certificados[$i]['alumno'] = $this->getReference('boero1');        
        $certificados[$i]['fechaEmision'] = '1965-01-27';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 7 */
        //$certificados[$i]['alumno'] = $this->getReference('pro');        
        $certificados[$i]['fechaEmision'] = '1965-01-28';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 8 */
        //$certificados[$i]['alumno'] = $this->getReference('cabral1');        
        $certificados[$i]['fechaEmision'] = '1965-01-29';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        /* ID 9 */
        //$certificados[$i]['alumno'] = $this->getReference('cabral2');        
        $certificados[$i]['fechaEmision'] = '1965-01-30';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 10 Alfredo nuñez */
        
        
        /* ID 11 Roman nuñez */
        //$certificados[$i]['alumno'] = $this->getReference('cabral3');        
        $certificados[$i]['fechaEmision'] = '1965-02-21';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 12 Alfredo nuñez */
        //$certificados[$i]['alumno'] = $this->getReference('cabral4');        
        $certificados[$i]['fechaEmision'] = '1965-02-22';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 13 Lucas Cabral */
        ////$certificados[$i]['alumno'] = $this->getReference('perez');        
        $certificados[$i]['fechaEmision'] = '1965-02-23';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        /* ID 14 Mercedes Cabral */
        //$certificados[$i]['alumno'] = $this->getReference('nunez');        
        $certificados[$i]['fechaEmision'] = '1965-02-24';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 15 Belen Cabral */
        //$certificados[$i]['alumno'] = $this->getReference('nunez1');        
        $certificados[$i]['fechaEmision'] = '1965-03-24';
        $certificados[$i]['fechaVencimiento'] = '1965-02-24';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 16 Soledad Cabral */
        //$certificados[$i]['alumno'] = $this->getReference('cuevas');        
        $certificados[$i]['fechaEmision'] = '1965-04-20';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        /* ID 17  */
        //$certificados[$i]['alumno'] = $this->getReference('cuevas1');        
        $certificados[$i]['fechaEmision'] = '1965-05-20';
        $certificados[$i]['fechaVencimiento'] = '1965-01-20';
        $certificados[$i]['ciudadEmision'] = 'Posadas';        
        $certificados[$i]['emitidoPor'] = 'dffd';
        $certificados[$i]['descripcion'] = 'asdfasdfdsf';
        $certificados[$i]['diagnostico'] = 'asdfasdfasd';
        $certificados[$i]['acompanante'] = true;                
        $i++;
        
        
        
                               
        /*foreach ($certificados as $certificado) {
            $certificadosObjeto = new Certificado();
            $certificadosObjeto->setAlumno($certificado['alumno']);                        
            $fechaEmision = DateTime::createFromFormat('Y-m-d', $certificado['fechaEmision']);
            $fechaVencimiento = DateTime::createFromFormat('Y-m-d', $certificado['fechaVencimiento']);
            $certificadosObjeto->setFechaEmision($fechaEmision);
            $certificadosObjeto->setFechaVencimiento($fechaVencimiento);
            $certificadosObjeto->setDescripcion($certificado['descripcion']);
            $certificadosObjeto->setCiudadEmision($certificado['ciudadEmision']);
            $certificadosObjeto->setDiagnostico($certificado['diagnostico']);
            $certificadosObjeto->setEmitidoPor($certificado['emitidoPor']);            
            $certificadosObjeto->setAcompanante($certificado['acompanante']);
                                    
            $manager->persist($certificadosObjeto);
            $manager->flush();
        } */                  
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 9; 
    }
}