<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FacturaDetalle
 */
class FacturaDetalle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $asistencia;

    /**
     * @var boolean
     */
    private $inasistencia;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set asistencia
     *
     * @param boolean $asistencia
     * @return FacturaDetalle
     */
    public function setAsistencia($asistencia)
    {
        $this->asistencia = $asistencia;

        return $this;
    }

    /**
     * Get asistencia
     *
     * @return boolean 
     */
    public function getAsistencia()
    {
        return $this->asistencia;
    }

    /**
     * Set inasistencia
     *
     * @param boolean $inasistencia
     * @return FacturaDetalle
     */
    public function setInasistencia($inasistencia)
    {
        $this->inasistencia = $inasistencia;

        return $this;
    }

    /**
     * Get inasistencia
     *
     * @return boolean 
     */
    public function getInasistencia()
    {
        return $this->inasistencia;
    }
}
