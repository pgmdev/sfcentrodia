<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonalTaller
 */
class PersonalTaller
{
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
