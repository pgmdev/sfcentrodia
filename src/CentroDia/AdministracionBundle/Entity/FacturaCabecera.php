<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FacturaCabecera
 */
class FacturaCabecera
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var integer
     */
    private $alumno;

    /**
     * @var integer
     */
    private $obraSocial;

    /**
     * @var string
     */
    private $importe;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FacturaCabecera
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set alumno
     *
     * @param integer $alumno
     * @return FacturaCabecera
     */
    public function setAlumno($alumno)
    {
        $this->alumno = $alumno;

        return $this;
    }

    /**
     * Get alumno
     *
     * @return integer 
     */
    public function getAlumno()
    {
        return $this->alumno;
    }

    /**
     * Set obraSocial
     *
     * @param integer $obraSocial
     * @return FacturaCabecera
     */
    public function setObraSocial($obraSocial)
    {
        $this->obraSocial = $obraSocial;

        return $this;
    }

    /**
     * Get obraSocial
     *
     * @return integer 
     */
    public function getObraSocial()
    {
        return $this->obraSocial;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return FacturaCabecera
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte()
    {
        return $this->importe;
    }
    /**
     * @var integer
     */
    private $periodoEmitido;

    /**
     * @var boolean
     */
    private $pendientePago;


    /**
     * Set periodoEmitido
     *
     * @param integer $periodoEmitido
     * @return FacturaCabecera
     */
    public function setPeriodoEmitido($periodoEmitido)
    {
        $this->periodoEmitido = $periodoEmitido;

        return $this;
    }

    /**
     * Get periodoEmitido
     *
     * @return integer 
     */
    public function getPeriodoEmitido()
    {
        return $this->periodoEmitido;
    }

    /**
     * Set pendientePago
     *
     * @param boolean $pendientePago
     * @return FacturaCabecera
     */
    public function setPendientePago($pendientePago)
    {
        $this->pendientePago = $pendientePago;

        return $this;
    }

    /**
     * Get pendientePago
     *
     * @return boolean 
     */
    public function getPendientePago()
    {
        return $this->pendientePago;
    }
}
