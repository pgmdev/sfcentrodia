<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Concepto
 */
class Concepto
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $tipo;


    
    public function __toString() {
        return $this->getNombre()." / ".$this->getTipo();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Concepto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Concepto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Concepto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $conceptoPago;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->conceptoPago = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add conceptoPago
     *
     * @param \CentroDia\AdministracionBundle\Entity\ConceptoPago $conceptoPago
     * @return Concepto
     */
    public function addConceptoPago(\CentroDia\AdministracionBundle\Entity\ConceptoPago $conceptoPago)
    {
        $this->conceptoPago[] = $conceptoPago;

        return $this;
    }

    /**
     * Remove conceptoPago
     *
     * @param \CentroDia\AdministracionBundle\Entity\ConceptoPago $conceptoPago
     */
    public function removeConceptoPago(\CentroDia\AdministracionBundle\Entity\ConceptoPago $conceptoPago)
    {
        $this->conceptoPago->removeElement($conceptoPago);
    }

    /**
     * Get conceptoPago
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConceptoPago()
    {
        return $this->conceptoPago;
    }
}
