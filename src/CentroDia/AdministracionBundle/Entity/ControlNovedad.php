<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlNovedad
 */
class ControlNovedad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaControl;

    /**
     * @var \DateTime
     */
    private $fechaVencimiento;

    /**
     * @var string
     */
    private $observaciones;

    /**
     * @var string
     */
    private $responsable;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaControl
     *
     * @param \DateTime $fechaControl
     * @return ControlNovedad
     */
    public function setFechaControl($fechaControl)
    {
        $this->fechaControl = $fechaControl;

        return $this;
    }

    /**
     * Get fechaControl
     *
     * @return \DateTime 
     */
    public function getFechaControl()
    {
        return $this->fechaControl;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return ControlNovedad
     */
    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return ControlNovedad
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return ControlNovedad
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string 
     */
    public function getResponsable()
    {
        return $this->responsable;
    }
    /**
     * @var \CentroDia\AdministracionBundle\Entity\ControlEdificio
     */
    private $controlEdificio;


    /**
     * Set controlEdificio
     *
     * @param \CentroDia\AdministracionBundle\Entity\ControlEdificio $controlEdificio
     * @return ControlNovedad
     */
    public function setControlEdificio(\CentroDia\AdministracionBundle\Entity\ControlEdificio $controlEdificio = null)
    {
        $this->controlEdificio = $controlEdificio;

        return $this;
    }

    /**
     * Get controlEdificio
     *
     * @return \CentroDia\AdministracionBundle\Entity\ControlEdificio 
     */
    public function getControlEdificio()
    {
        return $this->controlEdificio;
    }
}
