<?php

namespace CentroDia\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Edificio
 */
class ControlEdificio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombreCorto;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $observaciones;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString() {
        return $this->getNombreCorto();
    }

    /**
     * Set nombreCorto
     *
     * @param string $nombreCorto
     * @return Edificio
     */
    public function setNombreCorto($nombreCorto)
    {
        $this->nombreCorto = $nombreCorto;

        return $this;
    }

    /**
     * Get nombreCorto
     *
     * @return string 
     */
    public function getNombreCorto()
    {
        return $this->nombreCorto;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Edificio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return Edificio
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }
    /**
     * @var \DateTime
     */
    private $periodicidad;


    /**
     * Set periodicidad
     *
     * @param \DateTime $periodicidad
     * @return ControlEdificio
     */
    public function setPeriodicidad($periodicidad)
    {
        $this->periodicidad = $periodicidad;

        return $this;
    }

    /**
     * Get periodicidad
     *
     * @return \DateTime 
     */
    public function getPeriodicidad()
    {
        return $this->periodicidad;
    }
    /**
     * @var \CentroDia\AdministracionBundle\Entity\ControlNovedad
     */
    private $novedadEdificio;


    /**
     * Set novedadEdificio
     *
     * @param \CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio
     * @return ControlEdificio
     */
    public function setNovedadEdificio(\CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio = null)
    {
        $this->novedadEdificio = $novedadEdificio;

        return $this;
    }

    /**
     * Get novedadEdificio
     *
     * @return \CentroDia\AdministracionBundle\Entity\ControlNovedad 
     */
    public function getNovedadEdificio()
    {
        return $this->novedadEdificio;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->novedadEdificio = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add novedadEdificio
     *
     * @param \CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio
     * @return ControlEdificio
     */
    public function addNovedadEdificio(\CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio)
    {
        $this->novedadEdificio[] = $novedadEdificio;

        return $this;
    }

    /**
     * Remove novedadEdificio
     *
     * @param \CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio
     */
    public function removeNovedadEdificio(\CentroDia\AdministracionBundle\Entity\ControlNovedad $novedadEdificio)
    {
        $this->novedadEdificio->removeElement($novedadEdificio);
    }
}
