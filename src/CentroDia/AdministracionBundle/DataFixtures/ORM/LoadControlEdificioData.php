<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\AdministracionBundle\Entity\ControlEdificio;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadControlEdificioData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarControlEdificios($manager);
    }
    
    public function cargarControlEdificios(ObjectManager $manager){                                
        $i= 0;
        $edificios[$i]['id'] = 1;
        $edificios[$i]['nombreCorto'] = 'Carga de matafuegos';
        $edificios[$i]['descripcion'] = 'Carga de matafuegos';
        $edificios[$i]['observaciones'] = 'Carga de matafuegos';
        $edificios[$i]['periodicidad'] = 15;
        
        $i++;
        
        $edificios[$i]['id'] = 2;
        $edificios[$i]['nombreCorto'] = 'Fumigaciones';
        $edificios[$i]['descripcion'] = 'Fumigaciones';
        $edificios[$i]['observaciones'] = 'Fumigaciones';
        $edificios[$i]['periodicidad'] = 6;
        
        $i++;
        
        $edificios[$i]['id'] = 3;
        $edificios[$i]['nombreCorto'] = 'Inspección de tuberías de gas';
        $edificios[$i]['descripcion'] = 'Inspección de tuberías de gas';
        $edificios[$i]['observaciones'] = 'Inspección de tuberías de gas';
        $edificios[$i]['periodicidad'] = 24;
        
        $i++;
        
        $edificios[$i]['id'] = 4;                
        $edificios[$i]['nombreCorto'] = 'Inspección de instación eléctrica';
        $edificios[$i]['descripcion'] = 'Inspección de instación eléctrica';
        $edificios[$i]['observaciones'] = 'Inspección de instación eléctrica';
        $edificios[$i]['periodicidad'] = 12;
        
        
        $i++;
        
        $edificios[$i]['id'] = 5;
        $edificios[$i]['nombreCorto'] = 'Inspección de bomberos';
        $edificios[$i]['descripcion'] = 'Inspección de bomberos';
        $edificios[$i]['observaciones'] = 'Inspección de bomberos';
        $edificios[$i]['periodicidad'] = 36;
        
        
        $i++;
        $edificios[$i]['id'] = 6;
        $edificios[$i]['nombreCorto'] = 'Inspección de tanque de agua';
        $edificios[$i]['descripcion'] = 'Inspección de tanque de agua';
        $edificios[$i]['observaciones'] = 'Inspección de tanque de agua';
        $edificios[$i]['periodicidad'] = 12;
        
        $i++;
        
        $edificios[$i]['id'] = 7;
        $edificios[$i]['nombreCorto'] = 'Limpieza de tanque de agua';
        $edificios[$i]['descripcion'] = 'Limpieza de tanque de agua';
        $edificios[$i]['observaciones'] = 'Limpieza de tanque de agua';
        $edificios[$i]['periodicidad'] = 12;
        
           
        
               
        foreach ($edificios as $controledificio) {
            $edificiosObjeto = new ControlEdificio();
            $edificiosObjeto->setNombreCorto($controledificio['nombreCorto']);
            $edificiosObjeto->setDescripcion($controledificio['descripcion']);
            $edificiosObjeto->setObservaciones($controledificio['observaciones']);
            $edificiosObjeto->setPeriodicidad($controledificio['periodicidad']);
            //$this->addReference(strtolower($personal['nombre']), $personalesObjeto);
            $manager->persist($edificiosObjeto);
            
            if($controledificio['id'] == 1){
                $this->addReference('matafuego', $edificiosObjeto);
            }
            
            if($controledificio['id'] == 2){
                $this->addReference('fumigacion', $edificiosObjeto);
            }
            
            if($controledificio['id'] == 3){
                $this->addReference('inspecciongas', $edificiosObjeto);
            }
            
            if($controledificio['id'] == 4){
                $this->addReference('inspeccionelectrica', $edificiosObjeto);
            }
            
            if($controledificio['id'] == 5){
                $this->addReference('inspeccionbomberos', $edificiosObjeto);
            }
            
            if($controledificio['id'] == 6){
                $this->addReference('inspeccionagua', $edificiosObjeto);
            }
            
           
            $manager->flush();
        }                   
        
    }
    
                 
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 6; 
    }
}