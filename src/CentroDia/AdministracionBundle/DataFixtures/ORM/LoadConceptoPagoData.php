<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\AdministracionBundle\Entity\ConceptoPago;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadConceptoPagoData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarPagos($manager);
    }
    
    public function cargarPagos(ObjectManager $manager){                                
        $i= 0;
        
        $pagos[$i]['concepto'] = $this->getReference('agua');
        $pagos[$i]['observaciones'] = 'Gas natural';        
        $pagos[$i]['importe'] = 450;        
        $pagos[$i]['fecha'] = '2012-02-05';                
        $i++;                                    
        
        $pagos[$i]['concepto'] = $this->getReference('luz');
        $pagos[$i]['observaciones'] = 'luz';        
        $pagos[$i]['importe'] = 150;        
        $pagos[$i]['fecha'] = '2012-02-05';                
        $i++;                                    
        
        $pagos[$i]['concepto'] = $this->getReference('vigilancia');
        $pagos[$i]['observaciones'] = 'myTimeAndExpenses';        
        $pagos[$i]['importe'] = 350;        
        $pagos[$i]['fecha'] = '2014-01-05';                
        $i++;                                    
        
        $pagos[$i]['concepto'] = $this->getReference('gas');
        $pagos[$i]['observaciones'] = 'lud';        
        $pagos[$i]['importe'] = 50;        
        $pagos[$i]['fecha'] = '2012-02-05';                
        $i++;                                    
        
        $pagos[$i]['concepto'] = $this->getReference('alquiler');
        $pagos[$i]['observaciones'] = 'Pago a Daniel del depto';        
        $pagos[$i]['importe'] = 3550;        
        $pagos[$i]['fecha'] = '2012-02-05';                
        $i++;         
                               
        foreach ($pagos as $pago) {
            $pagosObjeto = new ConceptoPago();
            $pagosObjeto->setConcepto($pago['concepto']);
            
            $fecha = DateTime::createFromFormat('Y-m-d', $pago['fecha']);
            
            $pagosObjeto->setFecha($fecha);
            $pagosObjeto->setImporte($pago['importe']);
            $pagosObjeto->setObservaciones($pago['observaciones']);
            $manager->persist($pagosObjeto);
            $manager->flush();
        }                   
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 8; 
    }
}