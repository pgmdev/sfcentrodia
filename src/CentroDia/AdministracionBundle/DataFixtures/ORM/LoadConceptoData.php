<?php

namespace CentroDia\AdministracionBundle\DataFixtures\ORM;

use CentroDia\AdministracionBundle\Entity\Concepto;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadConceptoData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarConceptos($manager);
    }
    
                
    public function cargarConceptos(ObjectManager $manager){                                
        $i= 0;
        
        $conceptos[$i]['nombre'] = 'Litoral Gas';
        $conceptos[$i]['descripcion'] = 'Gas natural';        
        $conceptos[$i]['tipo'] = 's';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Aguas Santafesina ';
        $conceptos[$i]['descripcion'] = 'proveedor de agua';
        $conceptos[$i]['tipo'] = 's';  
                        
        $i++;
                               
        $conceptos[$i]['nombre'] = 'EPE Energía Eléctrica ';
        $conceptos[$i]['descripcion'] = 'proveedor de luz';
        $conceptos[$i]['tipo'] = 's';  
        
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Fibertel ';
        $conceptos[$i]['descripcion'] = 'proveedor de internet';
        $conceptos[$i]['tipo'] = 's';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Cable visión ';
        $conceptos[$i]['descripcion'] = 'proveedor de televisión por cable';
        $conceptos[$i]['tipo'] = 's';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Impuestos municipales ';
        $conceptos[$i]['descripcion'] = 'impuestos de servicios';
        $conceptos[$i]['tipo'] = 'i';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'DGR';
        $conceptos[$i]['descripcion'] = 'Rentas';
        $conceptos[$i]['tipo'] = 'i';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Alquiler ';
        $conceptos[$i]['descripcion'] = 'conceptos';
        $conceptos[$i]['tipo'] = 'i';  
        
        $i++;
        
        $conceptos[$i]['nombre'] = 'Vigilancia ';
        $conceptos[$i]['descripcion'] = 'conceptos';
        $conceptos[$i]['tipo'] = 'g';  
        
        $i++;
                
        $conceptos[$i]['nombre'] = 'Otros gastos ';
        $conceptos[$i]['descripcion'] = 'conceptos';
        $conceptos[$i]['tipo'] = 'g';  
            
        
                      
        foreach ($conceptos as $concepto) {                                   
            $conceptosObjeto = new Concepto();
            $conceptosObjeto->setNombre($concepto['nombre']);
            $conceptosObjeto->setDescripcion($concepto['descripcion']);                        
            $conceptosObjeto->setTipo($concepto['tipo']);                        
                                     
            $manager->persist($conceptosObjeto);
            $manager->flush();
            
            if($concepto['nombre']== "Aguas Santafesina "){
                $this->addReference('agua', $conceptosObjeto);
            }
            
            if($concepto['nombre']== "Litoral Gas"){
                $this->addReference('gas', $conceptosObjeto);
            }
                        
            if($concepto['nombre']== "Vigilancia "){
                $this->addReference('vigilancia', $conceptosObjeto);
            }
            
            if($concepto['nombre']== "Alquiler "){
                $this->addReference('alquiler', $conceptosObjeto);
            }
            
            if($concepto['nombre']== "EPE Energía Eléctrica "){
                $this->addReference('luz', $conceptosObjeto);
            }
        }                   
        
        
    }
    
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 5; 
    }
}