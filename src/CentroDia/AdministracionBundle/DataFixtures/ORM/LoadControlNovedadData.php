<?php

namespace CentroDia\PersonaBundle\DataFixtures\ORM;

use CentroDia\AdministracionBundle\Entity\ControlNovedad;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadControlNovedadData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarControlNovedades($manager);
    }
    
    public function cargarControlNovedades(ObjectManager $manager){                                
        $i= 0;
        
        $novedades[$i]['controlEdificio'] = $this->getReference('matafuego');
        $novedades[$i]['fechaControl'] = '2001-05-09';        
        $novedades[$i]['fechaNacimiento'] = '2002-02-05';
        $novedades[$i]['observaciones'] = 'una cagada todo';
        $novedades[$i]['responsable'] = 'Sergio Mendez';        
        $i++;
        
        $novedades[$i]['controlEdificio'] = $this->getReference('inspeccionagua');
        $novedades[$i]['fechaControl'] = '2001-05-09';        
        $novedades[$i]['fechaNacimiento'] = '2002-02-05';
        $novedades[$i]['observaciones'] = 'una cagada todo';
        $novedades[$i]['responsable'] = 'Sergio Mendez';
        
        $i++;
                              
               
        foreach ($novedades as $novedad) {
            $novedadesObjeto = new ControlNovedad();
            $novedadesObjeto->setControlEdificio($novedad['controlEdificio']);
            $fechaControl = \DateTime::createFromFormat('Y-m-d', $novedad['fechaControl']);
            $fechaNacimiento = \DateTime::createFromFormat('Y-m-d', $novedad['fechaNacimiento']);
            $novedadesObjeto->setFechaControl($fechaControl);
            $novedadesObjeto->setFechaVencimiento($fechaNacimiento);
            $novedadesObjeto->setObservaciones($novedad['observaciones']);
            $novedadesObjeto->setResponsable($novedad['responsable']);
            $manager->persist($novedadesObjeto);
            $manager->flush();
        }                   
        
    }
    
    
    
     
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 7; 
    }
}