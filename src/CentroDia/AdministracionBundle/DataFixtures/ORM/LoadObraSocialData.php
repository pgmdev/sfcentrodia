<?php

namespace CentroDia\AdministracionBundle\DataFixtures\ORM;

use CentroDia\AdministracionBundle\Entity\ObraSocial;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadObraSocialData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarObrasSociales($manager);
    }
    
                
    public function cargarObrasSociales(ObjectManager $manager){                                
        $i= 0;
        
        $conceptos[$i]['nombre'] = 'Ase  Nacional';
        $conceptos[$i]['cuit'] = '20-11111-01';
        $conceptos[$i]['descripcion'] = 'Accion Social de Empresarios';        
        $conceptos[$i]['direccion'] = 'Lima 87 , Monserrat - Ciudad de Buenos Aires ';          
        $i++;
        
        $conceptos[$i]['nombre'] = 'UPCN';
        $conceptos[$i]['cuit'] = '20-11111-02';
        $conceptos[$i]['descripcion'] = 'Sindicato Union del Personal Civil Seccional Prov Bs As';
        $conceptos[$i]['direccion'] = 'Avenida 13 901 , La Plata - Buenos Aires';                          
        $i++;
           
        $conceptos[$i]['nombre'] = 'OSPSA';
        $conceptos[$i]['cuit'] = '20-11111-03';
        $conceptos[$i]['descripcion'] = 'OBRA SOCIAL DEL PERSONAL DE LA SANIDAD ARGENTINA EN TODO EL PAÍS';
        $conceptos[$i]['direccion'] = 'Dorrego 1752 , Mar del Plata - Buenos Aires';                          
        $i++;
        
        $conceptos[$i]['nombre'] = 'UOCRA';
        $conceptos[$i]['cuit'] = '20-11111-04';
        $conceptos[$i]['descripcion'] = 'OBRA SOCIAL DEL PERSONAL DE LA CONSTRUCCIÓN';
        $conceptos[$i]['direccion'] = 'Poratti 1152 , 9 de Julio - Buenos Aires';                          
        $i++;
        
        $conceptos[$i]['nombre'] = 'OSECAC';
        $conceptos[$i]['cuit'] = '20-11111-05';
        $conceptos[$i]['descripcion'] = 'OBRA SOCIAL EMPLEADOS DE COMERCIO Y ACTIVIDADES AFINES';
        $conceptos[$i]['direccion'] = 'Diagonal 80 N° 875 , La Plata - Buenos Aires';                          
        $i++;
        
        $conceptos[$i]['nombre'] = 'APOC';
        $conceptos[$i]['cuit'] = '20-11111-07';
        $conceptos[$i]['descripcion'] = 'Seccional Santa Fe';
        $conceptos[$i]['direccion'] = 'San Martín 1615 , Santa Fe - Santa Fe';                          
        $i++;
        
            
        
                      
        foreach ($conceptos as $concepto) {                                   
            $conceptosObjeto = new ObraSocial();
            $conceptosObjeto->setRazonSocial($concepto['nombre']);
            $conceptosObjeto->setCuit($concepto['cuit']);
            $conceptosObjeto->setDescripcion($concepto['descripcion']);                        
            $conceptosObjeto->setDireccion($concepto['direccion']);                        
                                     
            $manager->persist($conceptosObjeto);
            $manager->flush();
          
        }                   
        
        
    }
    
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 16; 
    }
}