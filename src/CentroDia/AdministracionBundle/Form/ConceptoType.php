<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConceptoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text',array('label'=> 'Nombre:'))                
            ->add('descripcion', 'textarea', array('label'=> 'Nombre:'))
            ->add('tipo', 'choice', array(
                                        'label'=> 'Tipo',
                                        'choices'   => array('s'=> 'Servicio', 'i'=> 'Impuesto'),
                                        'required'  => false,
                                        'empty_value'=> 'Seleccionar Periodo'
                                        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\Concepto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_administracionbundle_concepto';
    }
}
