<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EscuelaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cuit', 'text', array('label'=> 'CUIT'))
            ->add('nombreCorto', 'text', array('label'=> 'Nombre Corto:'))
            ->add('numero', 'text', array('label'=> 'Numero'))
            ->add('direccion', 'text', array('label'=> 'Direccion'))
            ->add('telefono', 'text', array('label'=> 'Telefono'))
            ->add('email', 'text', array('label'=> 'E-mail'))
            ->add('categoria', 'text', array('label'=> 'Categoria'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\Escuela'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_administracionbundle_escuela';
    }
}
