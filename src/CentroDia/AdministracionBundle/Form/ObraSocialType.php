<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ObraSocialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('razonSocial', 'text', array('label'=> 'Razon Social'))                                                          
           ->add('direccion', 'text', array('label'=> 'Direccion'))      
           ->add('email', 'text', array('label'=> 'EMail'))      
          ->add('telefono', 'text', array('label'=> 'Telefono'))            
          ->add('responsable', 'text', array('label'=> 'Responsable'))      
            ->add('observaciones', 'textarea', array('label'=> 'Observaciones'))                                  
            ->add('cuit', 'text', array('label'=> 'CUIT')) 
                                                                                                                                                                                     
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\ObraSocial'
        ));
    }

    public function getName()
    {
        return 'centrodia_administracionbundle_obrasocial';
    }
}
