<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InventarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('nombre', 'text', array('label'=> 'Nombre'))                                                          
            ->add('descripcion', 'textarea', array('label'=> 'Descripcion'))                                  
            ->add('cantidad', 'text', array('label'=> 'Cantidad'))         
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\Inventario'
        ));
    }

    public function getName()
    {
        return 'centrodia_administracionbundle_inventario';
    }
}
