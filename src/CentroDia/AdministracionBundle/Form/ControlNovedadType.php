<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ControlNovedadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('controlEdificio', 'entity', array('class'=> 'AdministracionBundle:ControlEdificio',
                                                    'label'=> 'Control'
                ))
            ->add('fechaControl', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha control:'))
            ->add('fechaVencimiento', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha vencimiento:'))            
            ->add('observaciones', 'textarea', array('label'=> 'Observaciones'))
            ->add('responsable', 'text', array('label'=> 'Responsable'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\ControlNovedad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_administracionbundle_controlnovedad';
    }
}
