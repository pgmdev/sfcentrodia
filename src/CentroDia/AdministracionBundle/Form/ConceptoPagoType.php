<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConceptoPagoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('concepto', 'entity', array('class'=> 'AdministracionBundle:Concepto',
                                                    'label'=> 'Concepto'
                ))
            ->add('fecha', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label' => 'Fecha:'))            
            ->add('observaciones', 'textarea', array('label'=> 'Observaciones'))
            ->add('importe', 'money', array('label'=> 'Importe'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\ConceptoPago'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_administracionbundle_conceptopago';
    }
}
