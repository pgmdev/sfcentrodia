<?php

namespace CentroDia\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ControlEdificioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $arrayTiempo = array(
                             '15' => '15 Dias', 
                             '1' => '1 Mes', 
                             '2'=> '2 Meses',
                             '3'=> '3 Meses',
                             '4'=> '4 Meses',
                             '5'=> '5 Meses',
                             '6'=> '6 Meses',
                             '11'=> '1 Año',
                             '12'=> '2 Años',
                            );
        
        $builder
            ->add('nombreCorto', 'text', array('label' => 'Nombre:'))
            ->add('descripcion', 'textarea', array('label' => 'Descripcion:'))
            ->add('observaciones', 'textarea', array('label' => 'Observaciones:'))
            ->add('periodicidad','choice', array(
                                        'label'=> 'Periodicidad',
                                        'choices'   => $arrayTiempo,
                                        'required'  => false,
                                        'empty_value'=> 'Seleccionar Periodo'
                                        )
                 )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\ControlEdificio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'centrodia_administracionbundle_edificio';
    }
}
