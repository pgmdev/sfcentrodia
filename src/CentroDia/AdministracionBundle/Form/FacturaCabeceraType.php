<?php

namespace CentroDia\AdministracionBundle\Form;

use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturaCabeceraType extends AbstractType
{
    private $arrayParams;
    const   entityObraSocial = "AdministracionBundle:ObraSocial"; 
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                
        
            $entityManager = $this->arrayParams['container']->get('doctrine')->getManager();
                    
            $obraSocialTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['obraSocial'], self::entityObraSocial);                        
            
            $obraSocial = $this->arrayParams['obraSocial'];
                        
        
            /*$builder->add($builder
                    ->create('obraSocial', 'hidden')
                    ->addModelTransformer($obraSocialTransformer));*/
            $builder ->add('obraSocial', 'entity', array('class'=> 'AdministracionBundle:ObraSocial', 
                                                'label'=> 'Obra Social',
                                                'query_builder' => function (EntityRepository $repository) use ($obraSocial){
                                                    $qb = $repository->getObraSocialById($obraSocial);                                                                                                                                                                                                      
                                                    return $qb;
                                               }));
            
            $builder ->add('alumno', 'cd_autocompletefill', array(
                            'class'=> 'PersonaBundle:Alumno',
                            'property' => 'nombre',
                            'search_method'=> 'getAlumnoByData',
                            'route_primary' => 'ajax_alumno_by_data',
                            'route_secondary' => 'ajax_asistencias_by_alumno'
                            ))
                           ->add('importe')
                        
                        
            /*$builder
                    ->add('facturaDetalle', 'collection',array(
                    'type'=> new facturaDetalleType($this->arrayParams)                    
            ))*/
               
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AdministracionBundle\Entity\FacturaCabecera',
            'cascade_validation'=> true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'factura_cabecera';
    }
}
