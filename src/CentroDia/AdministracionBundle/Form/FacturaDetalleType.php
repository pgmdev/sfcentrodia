<?php

namespace CentroDia\AdministracionBundle\Form;

use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturaDetalleType extends AbstractType
{

    private $arrayParams;
    const entityAlumno = "PersonaBundle:Alumno"; 
    
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        
        $entityManager = $this->arrayParams['container']->get('doctrine')->getManager();
        
        $personalTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['alumno'], self::entityAlumno);                        
        
        $builder->add($builder
                    ->create('alumno', 'hidden')
                    ->addModelTransformer($personalTransformer));
        
        $builder
            ->add('asistio', 'checkbox', array('required'=> false))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\AcademicoBundle\Entity\AsistenciaDetalle'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asistencia_detalles';
    }
}
