<?php
namespace CentroDia\AdministracionBundle\Services;

class AdministracionManager {

    
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /*
     * Devuelve un listado de  activos 
     */

    public function listarCertificadosActivos() {
        $certificadoRepository = $this->container->get('doctrine')->getRepository('ExpedienteBundle:Certificado');
        $aCertificados = $certificadoRepository->getListarCertificadosActivos();
        return $aCertificados;
    }
    
    
     public function listarObraSociales($filtros) {
        
        $obraSocialRepository = $this->container->get('doctrine')->getRepository('AdministracionBundle:ObraSocial');
        $aObraSocial = $obraSocialRepository->getListarObraSociales($filtros);
        
        return $aObraSocial;
    }
   
    
}
