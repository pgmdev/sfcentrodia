<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use CentroDia\AdministracionBundle\Entity\Inventario;
use CentroDia\AdministracionBundle\Form\InventarioType;

/**
 * Inventario controller.
 *

 */
class InventarioController extends Controller
{

    /**
     * Lists all Inventario entities.
     *

     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdministracionBundle:Inventario')->findAll();
        
        $paginador = $this->get('knp_paginator');
        
        $entities = $paginador->paginate(
                $entities,
                $this->get('request')->query->get('page', 1),
                10
        );
        


        return $this->render('AdministracionBundle:Inventario:index.html.twig', array(
            'entities' => $entities,
        ));

    }
    
 
    public function createAction(Request $request)
    {
        $entity  = new Inventario();
        $form = $this->createForm(new InventarioType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('inventario_show', array('id' => $entity->getId())));
        }


        return $this->render('AdministracionBundle:Inventario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Inventario entity.
     *

     */
    public function newAction()
    {
        $entity = new Inventario();
        $form   = $this->createForm(new InventarioType(), $entity);


        return $this->render('AdministracionBundle:Inventario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

    }

    /**
     * Finds and displays a Inventario entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Inventario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Inventario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:Inventario:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Inventario entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Inventario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Inventario entity.');
        }

        $editForm = $this->createForm(new InventarioType(), $entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render('AdministracionBundle:Inventario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Edits an existing Inventario entity.
     *

     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Inventario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Inventario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new InventarioType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('inventario_edit', array('id' => $id)));
        }


        return $this->render('AdministracionBundle:Inventario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }
    /**
     * Deletes a Inventario entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdministracionBundle:Inventario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Inventario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('inventario'));
    }

    /**
     * Creates a form to delete a Inventario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
