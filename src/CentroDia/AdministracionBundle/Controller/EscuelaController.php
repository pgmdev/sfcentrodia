<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CentroDia\AdministracionBundle\Entity\Escuela;
use CentroDia\AdministracionBundle\Form\EscuelaType;

/**
 * Escuela controller.
 *
 */
class EscuelaController extends Controller
{
        
    /**
     * Finds and displays a Escuela entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Escuela')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escuela entity.');
        }

        
        return $this->render('AdministracionBundle:Escuela:show.html.twig', array(
            'entity'      => $entity        
        ));
    }

    /**
     * Displays a form to edit an existing Escuela entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Escuela')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escuela entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:Escuela:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Escuela entity.
    *
    * @param Escuela $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Escuela $entity)
    {
        $form = $this->createForm(new EscuelaType(), $entity, array(
            'action' => $this->generateUrl('escuela_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Escuela entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Escuela')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escuela entity.');
        }

    
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('escuela_edit', array('id' => $id)));
        }

        return $this->render('AdministracionBundle:Escuela:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView()

        ));
    }
    
}
