<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CentroDia\AdministracionBundle\Entity\ControlEdificio;
use CentroDia\AdministracionBundle\Form\ControlEdificioType;

/**
 * ControlEdificio controller.
 *
 */
class ControlEdificioController extends Controller
{

    /**
     * Lists all ControlEdificio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('AdministracionBundle:ControlEdificio')->findAll();
        
        $paginator = $this->get('knp_paginator');                   
        
        $entities = $paginator->paginate(
                $entities,
                $this->get('request')->query->get('page', 1),
                10
            );                
        

        return $this->render('AdministracionBundle:ControlEdificio:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ControlEdificio entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ControlEdificio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('edificio'));
        }

        return $this->render('AdministracionBundle:ControlEdificio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ControlEdificio entity.
     *
     * @param ControlEdificio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ControlEdificio $entity)
    {
        $form = $this->createForm(new ControlEdificioType(), $entity, array(
            'action' => $this->generateUrl('edificio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new ControlEdificio entity.
     *
     */
    public function newAction()
    {
        $entity = new ControlEdificio();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdministracionBundle:ControlEdificio:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ControlEdificio entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlEdificio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlEdificio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:ControlEdificio:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ControlEdificio entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlEdificio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlEdificio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:ControlEdificio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ControlEdificio entity.
    *
    * @param ControlEdificio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ControlEdificio $entity)
    {
        $form = $this->createForm(new ControlEdificioType(), $entity, array(
            'action' => $this->generateUrl('edificio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ControlEdificio entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlEdificio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlEdificio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('edificio_edit', array('id' => $id)));
        }

        return $this->render('AdministracionBundle:ControlEdificio:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ControlEdificio entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdministracionBundle:ControlEdificio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ControlEdificio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('edificio'));
    }

    /**
     * Creates a form to delete a ControlEdificio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('edificio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
