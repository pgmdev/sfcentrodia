<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CentroDia\AdministracionBundle\Entity\Concepto;
use CentroDia\AdministracionBundle\Form\ConceptoType;

/**
 * Concepto controller.
 *
 */
class ConceptoController extends Controller
{

    /**
     * Lists all Concepto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $conceptos = $em->getRepository('AdministracionBundle:Concepto')->findAll();
        
        $paginator = $this->get('knp_paginator');    
                
        
        $entities = $paginator->paginate(
                $conceptos,
                $this->get('request')->query->get('page', 1),
                20
            );    

        return $this->render('AdministracionBundle:Concepto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Concepto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Concepto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('concepto_show', array('id' => $entity->getId())));
        }

        return $this->render('AdministracionBundle:Concepto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Concepto entity.
     *
     * @param Concepto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Concepto $entity)
    {
        $form = $this->createForm(new ConceptoType(), $entity, array(
            'action' => $this->generateUrl('concepto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Concepto entity.
     *
     */
    public function newAction()
    {
        $entity = new Concepto();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdministracionBundle:Concepto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Concepto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Concepto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Concepto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:Concepto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Concepto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Concepto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Concepto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:Concepto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Concepto entity.
    *
    * @param Concepto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Concepto $entity)
    {
        $form = $this->createForm(new ConceptoType(), $entity, array(
            'action' => $this->generateUrl('concepto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Concepto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:Concepto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Concepto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('concepto_edit', array('id' => $id)));
        }

        return $this->render('AdministracionBundle:Concepto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Concepto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdministracionBundle:Concepto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Concepto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('concepto'));
    }

    /**
     * Creates a form to delete a Concepto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('concepto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
