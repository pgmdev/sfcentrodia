<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CentroDia\AdministracionBundle\Entity\ControlNovedad;
use CentroDia\AdministracionBundle\Form\ControlNovedadType;

/**
 * ControlNovedad controller.
 *
 */
class ControlNovedadController extends Controller
{

    /**
     * Lists all ControlNovedad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdministracionBundle:ControlNovedad')->findAll();

        $paginator = $this->get('knp_paginator');                   
        
        $entities = $paginator->paginate(
                $entities,
                $this->get('request')->query->get('page', 1),
                10
            );                
        
        
        return $this->render('AdministracionBundle:ControlNovedad:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ControlNovedad entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ControlNovedad();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('controlnovedad'));
        }

        return $this->render('AdministracionBundle:ControlNovedad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ControlNovedad entity.
     *
     * @param ControlNovedad $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ControlNovedad $entity)
    {
        $form = $this->createForm(new ControlNovedadType(), $entity, array(
            'action' => $this->generateUrl('controlnovedad_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ControlNovedad entity.
     *
     */
    public function newAction()
    {
        $entity = new ControlNovedad();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdministracionBundle:ControlNovedad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ControlNovedad entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlNovedad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlNovedad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:ControlNovedad:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ControlNovedad entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlNovedad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlNovedad entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:ControlNovedad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ControlNovedad entity.
    *
    * @param ControlNovedad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ControlNovedad $entity)
    {
        $form = $this->createForm(new ControlNovedadType(), $entity, array(
            'action' => $this->generateUrl('controlnovedad_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ControlNovedad entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ControlNovedad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ControlNovedad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('controlnovedad'));
        }

        return $this->render('AdministracionBundle:ControlNovedad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    
}
