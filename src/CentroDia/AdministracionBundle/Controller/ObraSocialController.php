<?php

namespace CentroDia\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use CentroDia\AdministracionBundle\Entity\ObraSocial;
use CentroDia\AdministracionBundle\Form\ObraSocialType;

/**
 * ObraSocial controller.
 *

 */
class ObraSocialController extends Controller
{

    /**
     * Lists all ObraSocial entities.
     *

     */
    public function indexAction()
    {
        $administracionManager = $this->get('administracion.manager');
        
        $aObraSocial = $administracionManager->listarObraSociales(array());     
        
        
        
        return $this->render('AdministracionBundle:ObraSocial:index.html.twig', array(
            'entities' => $aObraSocial,
        ));

    }
    /**
     * Creates a new ObraSocial entity.
 
    public function createAction(Request $request)
    {
        $entity  = new ObraSocial();
        $form = $this->createForm(new ObraSocialType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('obrasocial_show', array('id' => $entity->getId())));
        }


        return $this->render('AdministracionBundle:ObraSocial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new ObraSocial entity.
     *

     */
    public function newAction()
    {
        $entity = new ObraSocial();
        $form   = $this->createForm(new ObraSocialType(), $entity);


        return $this->render('AdministracionBundle:ObraSocial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

    }

    /**
     * Finds and displays a ObraSocial entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ObraSocial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ObraSocial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdministracionBundle:ObraSocial:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ObraSocial entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ObraSocial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ObraSocial entity.');
        }

        $editForm = $this->createForm(new ObraSocialType(), $entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render('AdministracionBundle:ObraSocial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Edits an existing ObraSocial entity.
     *

     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdministracionBundle:ObraSocial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ObraSocial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ObraSocialType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('obrasocial_edit', array('id' => $id)));
        }


        return $this->render('AdministracionBundle:ObraSocial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }
    /**
     * Deletes a ObraSocial entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdministracionBundle:ObraSocial')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ObraSocial entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('obrasocial'));
    }

    /**
     * Creates a form to delete a ObraSocial entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
