<?php

namespace CentroDia\AdministracionBundle\Controller;

use CentroDia\AdministracionBundle\Entity\Concepto;
use CentroDia\AdministracionBundle\Entity\FacturaCabecera;
use CentroDia\AdministracionBundle\Form\ConceptoType;
use CentroDia\AdministracionBundle\Form\FacturaCabeceraType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Concepto controller.
 *
 */
class FacturacionController extends Controller
{


    public function createAction(Request $request)
    {
        $entity = new Concepto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('concepto_show', array('id' => $entity->getId())));
        }

        return $this->render('AdministracionBundle:Concepto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Concepto entity.
     *
     * @param Concepto $entity The entity
     *
     * @return Form The form
     */
    private function createCreateForm(Concepto $entity)
    {
        $form = $this->createForm(new ConceptoType(), $entity, array(
            'action' => $this->generateUrl('concepto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Concepto entity.
     *
     */
    public function newAction($id)
    {
        $ruta = $this->get('kernel')->getRootDir() . '/../web/arch.pdf';
        
        $pdf = $this->container->get('knp_snappy.pdf')->generate('http://www.google.fr', $ruta);
        
        
        $em = $this->getDoctrine()->getManager();
        
        $facturaCabecera = new FacturaCabecera();
        
        $obraSocial = $em->getRepository('AdministracionBundle:ObraSocial')->find($id);
        
        $facturaCabecera->setObraSocial($obraSocial);
        
        $params = array('container' => $this->container, 'obraSocial' => $id);
        
        $form = $this->createForm(new FacturaCabeceraType($params), $facturaCabecera);
                        
        //$form   = $this->createCreateForm($facturaCabecera);

        return $this->render('AdministracionBundle:Facturacion:factura.html.twig', array(
            'entity' => $facturaCabecera,
            'obraSocial'=> $obraSocial,
            'form'   => $form->createView(),
        ));
    }



}
