<?php

namespace Rismi\UtilBundle\lib;

use \DateTime;
use nusoap_client;
use Rismi\AdministracionBundle\Entity\H2007ReporteNovedades;
use Rismi\PersonaBundle\Entity\Persona;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Serializer\Exception\Exception;

/**
 * Description of H2007Integracion
 *
 * @author santiago.semhan
 */
class H2007Integracion {

    private static $metodo = "HL7_SendMessage";
    
    private $container;
    private $dominioInterno;

    public function __construct(Container $container) {
        $this->container = $container;

        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $this->dominioInterno = $em->getRepository('AdministracionBundle:DominioInterno')->find(Tools::getSessionDominioId());
    }

    public function createPersonaMessage($persona, $mode) {

        switch ($mode) {
            case 'ADT004':
                $message = $this->setADT004($persona);
                break;
            case 'ADT008':
                $message = $this->setADT008($persona);
                break;
        }

        return $message;
    }

    private function reportNews($mensajeHL7, $url) {
        try {
            
            $client = new nusoap_client($url, 'wsdl', '', '', '', '');
            $param = array('msg' => $mensajeHL7);
            $webServiceResponse = $client->call(self::$metodo, array('parameters' => $param));
            if (!$client->fault)
                return $webServiceResponse;
            else
                return $client->getError();
        } catch (Exception $excep) {
            throw $excep;
        }
    }

    private function saveWebServiceExcecution($excecRes, $mensajeHL7) {
        try {

            $h2007ReporteNovedades = new H2007ReporteNovedades();

            $h2007ReporteNovedades->setDominioInterno($this->dominioInterno);

            $h2007ReporteNovedades->setFechaHora(new DateTime('NOW'));

            $h2007ReporteNovedades->setResultadoEjecucion($excecRes);

            $h2007ReporteNovedades->setMensaje($mensajeHL7);
            
            return true;
            
        } catch (Exception $excep) {
            throw $excep;
        }
    }

    public function sendNews($persona,$mode) {
        try {

                $ipServerH2007 = $this->dominioInterno->getIpServerH2007();

               
                if ($ipServerH2007) {
                    $mensajeHL7 = $this->createPersonaMessage($persona, $mode);
                    
                    //TODO quitar líneas para enviar mensaje HL2007
//                    echo '<pre>';
//                    echo $mensajeHL7;
//                    exit;
//                    
//                    $excecRes = $this->reportNews($mensajeHL7, $ipServerH2007);
//                    return $this->saveWebServiceExcecution($excecRes, $mensajeHL7);
                }

            return false;
        } catch (Exception $excep) {
            return false;
        }
    }

    /*
     * Genera un mensaje de alta de pacientes ADT 004
     */
    
    private function setADT004(Persona $persona) {

        $appOrigen = 'Empadronador';
        $appDestino = 'H2007';
        
        /* Se obtienen los datos del paciente */
        $idPaciente               = $persona->getId();
        $nombrePaciente           = $persona->getNombre();
        $otroNombrePaciente       = $persona->getOtroNombre();
        $apellidoPaciente         = $persona->getApellido();
        $otroApellidoPaciente     = $persona->getOtroApellido();
        $fechaNacimientoPaciente  = $persona->getFechaNacimiento()->format('Ymd');
        $sexoPaciente             = $persona->getParamSexo()->getNombre();
        
        /* Se obtienen los datos de documento de paciente */
        $documento                = $persona->getDocumentoPrincipal();
        $numeroDocumento          = $documento->getNumero();
        $letraDocumento           = $documento->getLetra();
        $tipoDocumento            = $documento->getParamTipoDocumento()->getNombre();
        
        /* Se obtienen los datos de domicilio de paciente */
        $domicilioPaciente        = $persona->getDomicilioPrincipal();
        $direccionPaciente        = sprintf('%s %s %s',$domicilioPaciente->getCalle(),$domicilioPaciente->getPuerta(),$domicilioPaciente->getPuerta());
        $localidadPaciente        = $domicilioPaciente->getLocalidad()->getNombre();
        $provinciaPaciente        = $domicilioPaciente->getProvincia()->getNombre();
        $codigoPostalPaciente     = $domicilioPaciente->getLocalidad()->getCodigoPostal();
        $telefonoContactoPaciente = $persona->getContacto()->getTelContacto();
        
        
        /* Se obtienen los datos de financiador de paciente */
        $idPlanMedicoPaciente         = '';
        $idProgramaMedicoPaciente     = '';
        $nombreProgramaMedicoPaciente = '';                
        $numeroBeneficiarioPaciente   = '';
        
        if ($persona->getPlanMedicoPersonaPrincipal()){
            
            $idPlanMedicoPaciente          = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getId();
            $idProgramaMedicoPaciente      = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getProgramaMedico()->getId();
            $nombreProgramaMedicoPaciente  = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getProgramaMedico()->getNombre();
            $numeroBeneficiarioPaciente    = $persona->getPlanMedicoPersonaPrincipal()->getNumeroBeneficiario();
        }
        
        
        $messageMSH = "MSH|^~\&|" . $appOrigen . "|" . $appDestino . "|||" . date('YmdHis') . "||ADT^A04|" . $idPaciente . "-" . date('YmdHis') . "|P|2.3|";
        $messageEVN = "EVN|A04|" . date('YmdHis') . "|||";
        $messagePID = "PID|1|" . $idPaciente . "||" . $numeroDocumento . "^^" . $letraDocumento . "^^" . $tipoDocumento . "|" . $apellidoPaciente . "^" . $otroApellidoPaciente . "^" . $nombrePaciente . "^" . $otroNombrePaciente . "||" . $fechaNacimientoPaciente . "|" . $sexoPaciente . "|||" . $direccionPaciente . "^^" . $localidadPaciente . "^" . $provinciaPaciente . "^" . $codigoPostalPaciente . "||" . $telefonoContactoPaciente . "||||||||||||||||||";
        $messagePV1 = "PV1||||||||||||||||||||||||||||||||||||||||||||||||||";
        $messageIN1 = "IN1|1|" . $idPlanMedicoPaciente . "|" . $idProgramaMedicoPaciente . "|" . $nombreProgramaMedicoPaciente . "||||" . $numeroBeneficiarioPaciente . "|||||||||||||||||||||||||||||";

        $message = $messageMSH . $messageEVN . $messagePID . $messagePV1 . $messageIN1;

        return $message;
    }

    /*
     * Genera un mensaje de modificacion de pacientes ADT 008
     */

    private function setADT008($persona) {

        $appOrigen = 'Empadronador';
        $appDestino = 'H2007';
        
        /* Se obtienen los datos del paciente */
        $idPaciente               = $persona->getId();
        $nombrePaciente           = $persona->getNombre();
        $otroNombrePaciente       = $persona->getOtroNombre();
        $apellidoPaciente         = $persona->getApellido();
        $otroApellidoPaciente     = $persona->getOtroApellido();
        $fechaNacimientoPaciente  = $persona->getFechaNacimiento()->format('Ymd');
        $sexoPaciente             = $persona->getParamSexo()->getNombre();
        
        /* Se obtienen los datos de documento de paciente */
        $documento                = $persona->getDocumentoPrincipal();
        $numeroDocumento          = $documento->getNumero();
        $letraDocumento           = $documento->getLetra();
        $tipoDocumento            = $documento->getParamTipoDocumento()->getNombre();
        
        /* Se obtienen los datos de domicilio de paciente */
        $domicilioPaciente        = $persona->getDomicilioPrincipal();
        $direccionPaciente        = sprintf('%s %s %s',$domicilioPaciente->getCalle(),$domicilioPaciente->getPuerta(),$domicilioPaciente->getPuerta());
        $localidadPaciente        = $domicilioPaciente->getLocalidad()->getNombre();
        $provinciaPaciente        = $domicilioPaciente->getProvincia()->getNombre();
        $codigoPostalPaciente     = $domicilioPaciente->getLocalidad()->getCodigoPostal();
        $telefonoContactoPaciente = $persona->getContacto()->getTelContacto();
        
        
        /* Se obtienen los datos de financiador de paciente */
        $idPlanMedicoPaciente         = '';
        $idProgramaMedicoPaciente     = '';
        $nombreProgramaMedicoPaciente = '';                
        $numeroBeneficiarioPaciente   = '';
        
        if ($persona->getPlanMedicoPersonaPrincipal()){
            
            $idPlanMedicoPaciente          = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getId();
            $idProgramaMedicoPaciente      = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getProgramaMedico()->getId();
            $nombreProgramaMedicoPaciente  = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getProgramaMedico()->getNombre();
            $numeroBeneficiarioPaciente    = $persona->getPlanMedicoPersonaPrincipal()->getNumeroBeneficiario();
        }
        
        $messageMSH = "MSH|^~\&|" . $appOrigen . "|" . $appDestino . "|||" . date('YmdHis') . "||ADT^A08|" . $idPaciente . "-" . date('YmdHis') . "|P|2.3|";
        $messageEVN = "EVN|A08|" . date('YmdHis') . "|||";
        $messagePID = "PID|1|" . $idPaciente . "||" . $numeroDocumento . "^^" . $letraDocumento . "^^" . $tipoDocumento . "|" . $apellidoPaciente . "^" . $otroApellidoPaciente . "^" . $nombrePaciente . "^" . $otroNombrePaciente . "||" . $fechaNacimientoPaciente . "|" . $sexoPaciente . "|||" . $direccionPaciente . "^^" . $localidadPaciente . "^" . $provinciaPaciente . "^" . $codigoPostalPaciente . "||" . $telefonoContactoPaciente . "||||||||||||||||||";
        $messagePV1 = "PV1||||||||||||||||||||||||||||||||||||||||||||||||||";
        $messageIN1 = "IN1|1|" . $idPlanMedicoPaciente . "|" . $idProgramaMedicoPaciente . "|" . $nombreProgramaMedicoPaciente . "||||" . $numeroBeneficiarioPaciente . "|||||||||||||||||||||||||||||";

        $message = $messageMSH . $messageEVN . $messagePID . $messagePV1 . $messageIN1;

        return $message;
    }

}
