<?php

namespace Rismi\UtilBundle\lib;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\Container;

/**
 * PucoManager Service
 *
 * Servicio destinado a la administración de todas las operaciones con el puco.
 * 
 * @author santiago.semhan
 */
class PucoManager {
    
    private $container;
    
    private $em;
    
    
    public function getContainer(){
        return $this->container;
    }
 
    public function __construct(Container $container) {

        $this->container = $container;
        $this->em        = $container->get('doctrine')->getManager('puco');;

    }
    
    
    public function getCodigoObraSocialByNumeroDocumento($nroDoc) {
        
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('codeos', 'codeos');
        
        $query = "SELECT codeos FROM puco p
                  WHERE  p.nrodoc = '$nroDoc' 
                  LIMIT 1";
        
        $nativeQuery = $this->em->createNativeQuery($query, $rsm);
        
        $result = $nativeQuery->getOneOrNullResult();
        
        return $result ? $result['codeos'] : null;
    }
    
    /*
     * Busca un conjunto de  datos del puco (codigo de obra social, fecha de inscripcion,nro de beneficiario 'nroafil', fecha baja y si esta embarazada)
     * asociados al numero de documento principal. Se usa en el widget de buscar persona
     * 
     *@return array
     */
    
    public function getADatosPUCOByNumeroDocumento($nroDoc) {
        
        $rsm = new ResultSetMapping();

        $rsm->addScalarResult('codeos', 'codeos');
        $rsm->addScalarResult('nroafil', 'nroafil');
        $rsm->addScalarResult('fechainscripcion', 'fechainscripcion');
        $rsm->addScalarResult('fechabaja', 'fechabaja');
        $rsm->addScalarResult('embarazo_actual', 'embarazo_actual');

        $query = "SELECT * FROM puco p
                  WHERE  p.nrodoc = '$nroDoc' 
                  LIMIT 1";
        
        $nativeQuery = $this->em->createNativeQuery($query, $rsm);
        
        $result = $nativeQuery->getOneOrNullResult();
        
        return $result;
    }
    
    
}
