<?php

namespace Rismi\UtilBundle\lib;

use Symfony\Component\DependencyInjection\Container;

/**
 * Description of RisIntegracion
 *
 * @author juan.cabral
 */
class RisIntegracion {

    private $container;
    
    private $ip;
    
    private $port;
    
    private $timeout;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->ip = $container->getParameter('ris_ip');
        $this->port = $container->getParameter('ris_port');
        $this->timeout = $container->getParameter('ris_timeout');
    }

    public function createPersonaXML($persona) {

        
        
        //Genero los datos del domicilio
        $domicilioPrincipal = $persona->getDomicilioPrincipal();
        
        //valores por defecto para domicilio y departamento
        $domicilioPrincipalDesc = 'NO DECLARADO';
        $departamento = 'POSADAS';
        
        //tiene domicilio
        if ($domicilioPrincipal != null){
            $domicilioPrincipalDesc = sprintf('%s %s %s %s', $domicilioPrincipal->getCalle(), $domicilioPrincipal->getNumero(), $domicilioPrincipal->getPiso(), $domicilioPrincipal->getPuerta());
            //tiene domicilio pero no departamento
            if ($domicilioPrincipal->getDepartamento() != null){
                $departamento = $domicilioPrincipal->getDepartamento()->getNombre();
            }
        }
       
        //Genero el plan y el financiador
        if($persona->getPlanMedicoPersonaPrincipal()){
            $planMedico = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getNombre();
            $financiador = $persona->getPlanMedicoPersonaPrincipal()->getPlanMedico()->getProgramaMedico()->getNombre();
        }else{
            $planMedico = 'PLAN UNICO';
            $financiador = 'PARTICULAR SIN CAPACIDAD DE PAGO';
        }
        
        if ($persona->getContacto()){
            $telContacto = $persona->getContacto()->getTelContacto();
        }else{
            $telContacto = 111111;
        }
        
        //Genero el documento
        $documento = $persona->getDocumentoPrincipal();
        
        $documentoTipo = 'DNI';
        $documentoNumero = '111111';
        
        if ($documento != null){
            $documentoTipo  = $documento->getParamTipoDocumento()->getNombre();
            $documentoNumero = $documento->getNumero();
        }
        
        
        //Genero el sexo
        $sexo  = 'M';
        
        if ($persona->getParamSexo() != null){
            $sexo = $persona->getParamSexo()->getDescripcion();
        }

        //genero el XML
        $xml = chr(11);
        $xml .= "<HisRisPaciente>" . chr(13) . chr(10);
        $xml .= "<Paciente>" . chr(13) . chr(10);
        $xml .= "<NroFcr>" . $persona->getId() . "</NroFcr>" . chr(13) . chr(10);
        $xml .= "<Sexo>" . $sexo . "</Sexo>" . chr(13) . chr(10);
        $xml .= "<Nombres>" . sprintf('%s %s', $persona->getNombre(), $persona->getOtroNombre()) . "</Nombres>" . chr(13) . chr(10);
        $xml .= "<ApellidoPaterno>" . sprintf('%s %s', $persona->getApellido(), $persona->getOtroApellido()) . "</ApellidoPaterno>" . chr(13) . chr(10);
        $xml .= "<FechaNacimiento>" . $persona->getFechaNacimiento()->format('Y-m-d')  . "</FechaNacimiento>" . chr(13) . chr(10);
        $xml .= "<Protesisortesis></Protesisortesis>" . chr(13) . chr(10);
        $xml .= "<Alergias></Alergias>" . chr(13) . chr(10);
        $xml .= "<Domicilio>" . substr($domicilioPrincipalDesc, 0, 49) . "</Domicilio>" . chr(13) . chr(10);
        $xml .= "<Telefono>" . $telContacto . "</Telefono>" . chr(13) . chr(10);
        $xml .= "<Departamento>" . $departamento . "</Departamento>" . chr(13) . chr(10);
        $xml .= "<EstadoCivil></EstadoCivil>" . chr(13) . chr(10);
        $xml .= "<TipoDocumento>" . $documentoTipo . "</TipoDocumento>" . chr(13) . chr(10);
        $xml .= "<NroDocumento>" . $documentoNumero . "</NroDocumento>" . chr(13) . chr(10);
        $xml .= "<FinanciadorPlan>" . chr(13) . chr(10);
        $xml .= " <Financiador>" . $financiador . "</Financiador>" . chr(13) . chr(10);
        $xml .= " <Plan>" . $planMedico . "</Plan>" . chr(13) . chr(10);
        $xml .= " <FechaInicio></FechaInicio>" . chr(13) . chr(10);
        $xml .= "</FinanciadorPlan>" . chr(13) . chr(10);
        $xml .= "</Paciente>" . chr(13) . chr(10);
        $xml .= "</HisRisPaciente>" . chr(13) . chr(10);
        $xml .= chr(28);

        return $xml;
    }

    public function sendXml($xml) {
        $ris_host = $this->ip;
        $ris_port = $this->port;
        $ris_timeout = $this->timeout;
        $respuesta = "";
        
        //TODO descomentar estas líneas para enviar integración al RIS
//       
//        echo '<pre>';
//        echo $xml;
//        exit;
//        $sk = fsockopen($ris_host, $ris_port, $errnum, $errstr, $ris_timeout);
//        
//        if (!is_resource($sk)) {
//            exit("Fallo en la conexi&oacute;n: " . $errnum . " " . $errstr);
//        } else {
//            // se envia el mensaje por socket
//            // la respuesta del servidor se almacena en
//            // el string $respuesta
//            fwrite($sk, $xml);
//            $respuesta = "";
//            while (!feof($sk)) {
//                $respuesta .= fgets($sk, 1024);
//            }
//        }
//        fclose($sk);
        return $respuesta;
    }

}
