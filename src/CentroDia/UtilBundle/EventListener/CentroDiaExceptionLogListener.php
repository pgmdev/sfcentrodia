<?php

namespace CentroDia\UtilBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Exception;
use CentroDia\AdministracionBundle\Entity\CentroDiaExceptionLog;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Description of CentroDiaLogExceptionListener
 *
 * @author juan.cabral
 */
class CentroDiaExceptionLogListener {

    /**
     * @var 
     */
    protected $container;

    /**
     * @var TwigEngine
     */
    protected $templating;

    /**
     * @var Kernel
     */
    protected $kernel;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, TwigEngine $templating, $kernel) {
        // assign value(s)
        $this->container = $container;
        $this->templating = $templating;
        $this->kernel = $kernel;
    }

    /**
     * Intersepta el evento *onKernelException* para guardar los datos del LOG de Excepciones
     *
     *
     * @param GetResponseForExceptionEvent $event 
     *
     * @return Render
     */
    public function onKernelException(GetResponseForExceptionEvent $event) {

        $entorno = $this->kernel->getEnvironment();
        $exception = $event->getException();
        $request = $event->getRequest();
        $code = 0;
        if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
        } else {
            $code = 500;
        }

        
        if ('dev' == $this->kernel->getEnvironment()) {
            $response = new Response($this->templating->render(
                            'TwigBundle:Exception:error.html.twig', array(
                        'status_code' => $code,
                        'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                        'exception' => $exception,
                            //'logger'         => $logger,
                            //'currentContent' => $currentContent,
                            )
            ));

            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(500);
            }

            $event->setResponse($response);
        }
    }

}
