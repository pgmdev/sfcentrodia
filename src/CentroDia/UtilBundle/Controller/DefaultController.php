<?php

namespace GestionHogar\UtilBundle\Controller;

use GestionHogar\UtilBundle\lib\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DefaultController extends Controller {
    

    /**
     * @Route("/ajax_default", name="ajax_default")
     */
    public function getAjaxDefaultAction(Request $request) {
        $value = strtoupper($request->get('term'));
        
        $class = $request->get('class');
        $property = $request->get('property');
        $searchMethod = $request->get('search_method');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository($class)->$searchMethod($value);

        $json = array();

        if (!count($entities)) {
            $json[] = array(
                'label' => 'No se encontraron coincidencias',
                'value' => ''
            );
        } else {

            foreach ($entities as $entity) {
                $json[] = array(
                    'id' => $entity['id'],
                    //'label' => $entity[$property],
                    'value' => $entity[$property]
                );
            }
        }

        $response = new Response();
        $response->setContent(json_encode($json));

        return $response;
    }

    

}
