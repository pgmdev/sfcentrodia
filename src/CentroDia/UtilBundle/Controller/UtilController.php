<?php

namespace CentroDia\UtilBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UtilController extends Controller {

    /**
     * @author juan.cabral
     */
    public function getDataByInputAction(Request $request) {

        $parentID = $request->get('term');
        $em = $this->getDoctrine()->getManager();
        $rClase = $em->getRepository($request->get('class'));
        $searchMethod = $request->get('search_method');
        $oEntity = $rClase->$searchMethod($parentID);       
                
        if (count($oEntity) > 0) {
            foreach ($oEntity as $entity) {
                //var_dump($entity);
                $retorno[] = array(
                    'id'=> $entity['id'],
                    'value'=> $entity['apellido'],
                    'label'=> $entity['apellido']." - ".$entity['nombre'],
                    //$key => $value,                    
                );
            }
        } else {
            $retorno[] = array(
                'id' => '',
                'nombre' => 'Sin resultados'
            );
        }        

        $retorno = json_encode($retorno);
        
        return new Response($retorno, 200, array('Content-Type' => 'application/json'));
    }
    
    /**
     * @author juan.cabral
     */
    public function getDataByParentAction(Request $request) {

        $parentID = $request->get('parentID');
        $em = $this->getDoctrine()->getManager();
        $rClase = $em->getRepository($request->get('class'));
        $oEntity = $rClase->find($parentID);       
                        
        if (is_object($oEntity)) {            
                //var_dump($entity);                
                    $retorno = array(
                        'id' => $parentID,
                        'apellido' => $oEntity->getApellido(),
                        'nombre' => $oEntity->getNombre(),
                        'dni' => $oEntity->getDni(),
                        'direccion' => $oEntity->getDireccion(),
                    //$key => $value,                    
                    );                
                
        } else {
            $retorno[] = array(
                'id' => '',
                'nombre' => 'Sin resultados'
            );
        }        

        $retorno = json_encode($retorno);
        
        return new Response($retorno, 200, array('Content-Type' => 'application/json'));
    }

    

}
