<?php

namespace CentroDia\UsuarioBundle\Form;

use CentroDia\UsuarioBundle\Entity\Usuario;
use CentroDia\UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UsuarioType extends AbstractType {
    
    private $arrayParams;
    const   entityPersonal = "PersonaBundle:Personal"; 
    
    public function __construct($arrayParams = null) {
        $this->arrayParams = $arrayParams;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        //$personal = $this->arrayParams['personal'];
        
        $entityManager = $this->arrayParams['container']->get('doctrine')->getManager();
        
        $personalTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['personal'], self::entityPersonal);
        
        $accept = 'image/*';
        
        $assert = new Assert\File(array(
            'maxSize' => '5120k',
            'mimeTypes' => array(
                'image/png',
                'image/jpeg',
                'image/gif'
            ),
            'mimeTypesMessage' => 'Por favor seleccione una imágen con formato válido',
        ));
       
        $builder->add($builder
                    ->create('personal', 'hidden')
                    ->addModelTransformer($personalTransformer))
                ->add('username', 'text', array('label' => 'Nombre', 'attr' => array('class' => 'form-control')))
                ->add('email', 'text', array('label' => 'E-Mail', 'attr' => array('class' => 'form-control')))
                
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'Los campos de la contreña deben coincidir.',
                    'options' => array('attr' => array('class' => 'password-field')),
                    'required' => true,
                    'first_options' => array('label' => 'Contraseña'),
                    'second_options' => array('label' => 'Repertir Contraseña'),
        ))
                ->add('file', 'file', array(
            'required' => false,
            'label' => 'Foto',            
            'mapped'=>false,
            'attr' => array('accept' => $accept),
            'constraints' => array($assert),
            'auto_initialize' => false,
            'attr'=>array('class'=> 'input-file uniform_on')          
        ))
            /*->add('roles', 'choice', array('label' => 'Rol', 
                                           'required' => true, 
                                           'choices' => array( 
                                               1 => 'ROLE_ADMIN', 
                                               2 => 'ROLE_USER', 
                                               3 => 'ROLE_PROFESOR', 
                                               4 => 'ROLE_SECRETARIO'
                                            ), 
                                            'multiple' => true))*/;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CentroDia\UsuarioBundle\Entity\Usuario',
            'cascade_validation' => true
        ));
        
    }
    
    
    public static function processImage(UploadedFile $uploaded_file, Usuario $usuario)
    {
        $path = 'foto/';
     
        $uploaded_file_info = pathinfo($uploaded_file->getClientOriginalName());
        $file_name =            
            $usuario->getId() ."_".md5(rand()).
            "." .
            $uploaded_file_info['extension']
            ;

        $uploaded_file->move($path, $file_name);

        return $file_name;
    }

    public function getName() {
        return 'centrodia_usuariobundle_usuariotype';
    }

}
