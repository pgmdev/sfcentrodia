<?php

namespace CentroDia\UsuarioBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('nombre', 'text', array('label'=>'Nombre de usuario:', 'required'=> false))
                     
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array(     
            'cascade_validation' => true,
            'csrf_protection'   => false                      
        ));
    }

    public function getName()
    {
        return 'centrodia_usuariobundle_usuario_filter';
    }
}
