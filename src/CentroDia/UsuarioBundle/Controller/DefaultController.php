<?php

namespace CentroDia\UsuarioBundle\Controller;

use CentroDia\PersonaBundle\lib\HandlerUser;
use CentroDia\UsuarioBundle\Form\filters\UsuarioFilterType;
use CentroDia\UsuarioBundle\Form\UsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        
        $personaManager = $this->get('persona.manager');
        
        $usuarioFiltro = new UsuarioFilterType();  
        
        $aUsuarios = $personaManager->listarUsuarios(array());                                                
        
        $formFiltro = $this->createForm($usuarioFiltro);  
        
        $formFiltro->handleRequest($request);
        
        if ($formFiltro->isValid()) {

            $filtros = $formFiltro->getData();            
            
            $aUsuarios = $personaManager->listarUsuarios($filtros);
            
        }  

        return $this->render('UsuarioBundle:Default:indexUsuario.html.twig', array(
            'entities' => $aUsuarios,
            'formFilter'=>$formFiltro->createView()
        ));        
    }
    
    public function showDashboardAction(){
                
        if ($this->get('security.context')->isGranted('ROLE_USER')) {
            
            $usuario = $this->getUser();
            
            $academicoManager = $this->get('academico.manager');                        
            
            //$aTalleres = $academicoManager->getTalleresByPersonal($usuario->getPersonal());
            
            return $this->render('UsuarioBundle:Home:usuario.html.twig', array(
            'entities' => $usuario,
            ));
        }
        
        
                
    }
    
      
     public function createUsuarioAction($id){
        $em = $this->getDoctrine()->getManager();
        $usuarioHandler = new HandlerUser($this->container);
        $personal = $em->getRepository('PersonaBundle:Personal')->find($id);
        
        $usuario = $usuarioHandler->checkUser($personal);
                
        $arrayParams = array('personal'=> $id, 'container'=> $this->container);
        
        $usuarioForm = new UsuarioType($arrayParams);
        
        $form = $this->createForm($usuarioForm, $usuario);                
        
        return $this->render('PersonaBundle:Persona:usuario.html.twig', array(
                    'entity' => $usuario,      
                    'personal'=> $personal,
                    'form' => $form->createView()
        ));
    }
    
    public function updateUsuarioAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $usuarioHandler = new HandlerUser($this->container);
        $personal = $em->getRepository('PersonaBundle:Personal')->find($id);
               
        $usuario = $em->getRepository('UsuarioBundle:Usuario')->findOneByPersonal($id);
        if (!$usuario) {
            $usuario = $usuarioHandler->crearNuevoUsuario($personal);
            
        }

        $arrayParams = array('personal'=> $id,'container'=> $this->container);
        
        $usuarioForm = new UsuarioType($arrayParams);
        
        
        $form = $this->createForm($usuarioForm, $usuario);
        
        if ($request->isMethod('POST')) {
            $form->submit($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) {
                $uploaded_file = $form['file']->getData();
                
                if ($uploaded_file) {
                    //Sube la imagen
                    $picture = UsuarioType::processImage($uploaded_file, $usuario);

                    //setea el tamaño del recorte
                    $targ_w = $request->request->get('w');
                    $targ_h = $request->request->get('h');

                    if ($targ_h && $targ_w) {
                        if (null !== $usuario->getFoto()) {
                            $file = $usuario->getFoto();
                            @unlink($file);
                        } else {
                            $file = $picture;
                        }
                        $file = $this->get('kernel')->getRootDir() . '/../web/foto/' . $picture;

                        $img_r = imagecreatefromjpeg($file);
                        $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                        imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                        imagejpeg($dst_r, $file, 90);

                        
                    }
                    

                    $usuario->setFoto($picture);
                }
                
                
                $usuarioHandler = new HandlerUser($this->container);
                $usuarioManager = $this->container->get('fos_user.user_manager');                
                $usuarioManager->updateUser($usuario, false);
                $em->persist($usuario);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha creado el usuario .'.$usuario->getUsername()
                );

            
                return $this->redirect($this->generateUrl('administracion_usuarios'));
            }
        
        }
        
        
        
        return $this->render('PersonaBundle:Persona:usuario.html.twig', array(
                    'entity' => $usuario, 
                    'personal' => $personal, 
                    'form' => $form->createView()
        ));
    }
    
    public function perfilUsuarioAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('UsuarioBundle:Usuario')->find($id);

        if (!$usuario) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }       
        

        return $this->render('AdministracionBundle:Usuario:view.html.twig', array(            
            'usuario'      => $usuario,
            
        ));
    }
    
    public function editUsuarioAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('UsuarioBundle:Usuario')->find($id);

        if (!$usuario) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }       
        
        $arrayParams = array('personal'=> $id, 'container'=> $this->container);
        
        $form = $this->createForm(new UsuarioType($arrayParams), $usuario);
        

        return $this->render('AdministracionBundle:Usuario:usuario.html.twig', array(
            'modo_edicion'=> true,
            'usuario'      => $usuario,
            'form'   => $form->createView()            
        ));
        
    }
    
    
}
