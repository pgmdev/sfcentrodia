<?php  

namespace CentroDia\UsuarioBundle\Entity; // Reemplazar ruta por nuestro bundle.

use FOS\UserBundle\Model\User as BaseUser;  
use Doctrine\ORM\Mapping as ORM;

/** 
 * @ORM\Table(name="cd_usuario")
 * @ORM\Entity(repositoryClass="CentroDia\UsuarioBundle\Entity\UsuarioRepository")
 */
class Usuario extends BaseUser  
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;	          
    
    /**
     * @var cuenta
     * @ORM\OneToOne(targetEntity="CentroDia\PersonaBundle\Entity\Personal", mappedBy="creadoPor",  cascade={"persist", "remove"})
     *
     */
    
    protected $personal;	

    public function __construct()
    {
        // Mantener esta línea para llamar al constructor
        // de la clase padre
        parent::__construct();
        //$this->roles = array('ROLE_PROFESOR');

        // Aquí podremos añadir el código necesario.
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   

   

    /**
     * Set personal
     *
     * @param \CentroDia\PersonaBundle\Entity\Personal $personal
     * @return Usuario
     */
    public function setPersonal(\CentroDia\PersonaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \CentroDia\PersonaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    /**
     * @var string
     */
    private $foto;


    /**
     * Set foto
     *
     * @param string $foto
     * @return Usuario
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }
}
