<?php

namespace CentroDia\ThemeBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilder extends ContainerAware {

    protected $user;

    /**
     *
     * Función que Arma la estructura principal de menu
     *
     * @param FactoryInterface $menu
     * @param array $options
     */
    public function mainMenu(FactoryInterface $factory, array $options) {


        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-menu');

        $menu->addChild('Dashboard', array('route' => 'personas'))
                ->setAttribute('class', 'active')
                ->setAttribute('icon', 'fa fa-dashboard');
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_ADMIN'))) {
            $menu = $this->roleSuperAdmin($menu);
        }
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_SECRETARIO'))) {
            $menu = $this->roleSecretario($menu);
        }
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_PROFESOR'))) {
            $menu = $this->roleProfesor($menu);
        }

        return $menu;
    }
    
    public function createBreadcrumbsMenu(Request $request) {
    	//
        $bcmenu = $this->createMainMenu($request);
        return $this->getCurrentMenuItem($bcmenu);
    }
    
    public function getCurrentMenuItem($menu)
    {
        $voter = $this->container->get('centrodia.theme.menu.request');
		
        foreach ($menu as $item) {
            if ($voter->matchItem($item)) {
                return $item;
            }
			
            if ($item->getChildren() && $currentChild = $this->getCurrentMenuItem($item)) {
                return $currentChild;
            }
        }
		
        return null;
    }

    public function roleSuperAdmin($menu) {
        $menu = $this->empadronadorAdminMenu($menu);
        $menu = $this->administracionMenu($menu);
        $menu = $this->academicoMenu($menu);
        $menu = $this->facturacionMenu($menu);
        
        
                
        return $menu;
    }
    
    public function roleSecretario($menu) {
        $menu = $this->empadronadorMenu($menu);                
                
        return $menu;
    }
    
    public function roleProfesor($menu) {
        $menu = $this->academicoMenu($menu);                
                
        return $menu;
    }
    
    public function empadronadorAdminMenu($menu){
        $menu->addChild('empadronador', array('uri' => '#', 'label' => 'Empadronador'))                
                ->setAttribute('class', 'treeview');

        $menu['empadronador']->setChildrenAttribute('class', 'treeview-menu');

        $menu['empadronador']->setAttribute('icon', 'fa fa-users');
        
        $menu['empadronador']->addChild('alumnos', array('uri' => '#', 'route' => 'alumnos', 'label' => 'Alumnos'));
                                               
        $menu['empadronador']->addChild('padres', array('route' => 'padres', 'label' => 'Padres'));                

        $menu['empadronador']->addChild('personal', array('route' => 'personal', 'label' => 'Personal'));                
        
        $menu['empadronador']->addChild('alumnos', array('route' => 'alumnos', 'label' => 'Alumnos'));                                      
        
        return $menu;
    }
    
    public function empadronadorMenu($menu){
        $menu->addChild('empadronador', array('uri' => '#', 'label' => 'Empadronador'));                

        $menu['empadronador']->setChildrenAttribute('class', 'treeview-menu');

        $menu['empadronador']->setAttribute('icon', 'fa fa-users');
        
        $menu['empadronador']->addChild('alumnos', array('uri' => '#', 'route' => 'alumnos', 'label' => 'Alumnos'));                
                       
                                     
        return $menu;
    }
    
    
    
    
    public function administracionMenu($menu){
        $menu->addChild('admin', array('uri' => '#', 'label' => 'Administracion'))                
                ->setAttribute('class', 'treeview');
        
        $menu['admin']->setAttribute('icon', 'fa fa-users');
        
        $menu['admin']->setChildrenAttribute('class', 'treeview-menu');                
        
        $menu['admin']->addChild('usuario', array('route' => 'administracion_usuarios', 'label' => 'Usuarios'));
                
        
        $menu['admin']->addChild('controledificio', array('route' => 'edificio', 'label' => 'Edificio'));
                
        
        $menu['admin']->addChild('concepto', array('route' => 'concepto', 'label' => 'Concepto'));
                
        
        $menu['admin']->addChild('taller', array('route' => 'taller', 'label' => 'Talleres'));
                
        
        $menu['admin']->addChild('informe', array('route' => 'informe', 'label' => 'Informes'));
                
        
        $menu['admin']->addChild('certificado', array('route' => 'certificados', 'label' => 'Certificados'));
        
        $menu['admin']->addChild('inventario', array('route' => 'inventario', 'label' => 'Inventario'));
        
        $menu['admin']->addChild('obrasocial', array('route' => 'obrasocial', 'label' => 'Obra Social'));
                
        
        return $menu;
    }
    
    public function academicoMenu($menu){
        
        $menu->addChild('academico', array('uri' => '#', 'label' => 'Academico'))                
                ->setAttribute('class', 'treeview');
        
        $menu['academico']->setChildrenAttribute('class', 'treeview-menu');
        
        
        $menu['academico']->setAttribute('icon', 'fa fa-mortar-board');
                                
        
        $menu['academico']->addChild('horario', array('route' => 'horario_test', 'label' => 'Horario'));
                

        $menu['academico']->addChild('asistencia', array('route' => 'personas', 'label' => 'Asistencia'));
                
        
        return $menu;
    }
    
   
    
    public function expedienteMenu($menu){
        $menu->addChild('expediente', array('uri' => '#', 'label' => 'Expediente'))
                ->setAttribute('icon', 'fa fa-angle-left pull-right')
                ->setAttribute('class', 'treeview');

        $menu['expediente']->setChildrenAttribute('class', 'treeview-menu');

        $menu['expediente']->setAttribute('icon', 'fa fa-gears');

        $menu['expediente']->addChild('alumnos', array('route' => 'alumnos', 'label' => 'Certificado'))
                ->setAttribute('icon', 'fa fa-angle-double-right');


        $menu['empadronador']->addChild('padres', array('route' => 'padres', 'label' => 'Padres'))
                ->setAttribute('icon', 'fa fa-angle-double-right');       
        
        return $menu;
    }
    
    
    public function facturacionMenu($menu){
        $menu->addChild('facturacion', array('uri' => '#', 'label' => 'Facturacion'));                

        $menu['facturacion']->setChildrenAttribute('class', 'treeview-menu');

        $menu['facturacion']->setAttribute('icon', 'fa fa-users');
        
        $menu['facturacion']->addChild('alumnos', array('uri' => '#', 'route' => 'alumnos', 'label' => 'Alumnos'));                
                       
                                     
        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav pull-right');

        $menu->addChild('User', array('label' => 'Hi visitor'))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'icon-user');

        $menu['User']->addChild('Edit profile', array('route' => 'acme_hello_profile'))
                ->setAttribute('icon', 'icon-edit');

        return $menu;
    }

}
